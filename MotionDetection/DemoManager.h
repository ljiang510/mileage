//
//  DemoManager.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/12/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DemoManager : NSObject
+(instancetype)sharedManager;

-(void)registerNSUserDefault;
-(BOOL)isDemo;
-(void)setShouldDemo:(BOOL)shouldDemo;

-(NSArray *)cars;
-(NSArray *)tags;
-(NSArray *)projects;
-(NSArray *)trips;
-(NSArray *)tripSummary;
-(NSArray *)expenses;

@end
