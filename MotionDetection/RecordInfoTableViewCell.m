
//
//  RecordInfoTableViewCell.m
//  MotionDetection
//
//  Created by Liangjun Jiang on 5/23/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "RecordInfoTableViewCell.h"
#import "Trip.h"
#import "TripSummary.h"
#import <GoogleMaps/GoogleMaps.h>
#import "AAPLStyleUtilities.h"

@interface RecordInfoTableViewCell()<GMSMapViewDelegate>
@property (nonatomic, weak) IBOutlet UILabel *startLabel;
@property (nonatomic, weak) IBOutlet UILabel *finishLabel;
@property (nonatomic, weak) IBOutlet UILabel *distanceLabel;
@property (nonatomic, weak) IBOutlet UILabel *dayOfWeekLabel;
@property (nonatomic, weak) IBOutlet UILabel *dollarLabel;
@property (nonatomic, weak) IBOutlet UIView *startMapView;
@property (nonatomic, weak) IBOutlet UIView *finishMapView;

@end

@implementation RecordInfoTableViewCell

+ (GMSCameraPosition *)defaultCamera {
    return [GMSCameraPosition cameraWithLatitude:37.7847
                                       longitude:-122.41
                                            zoom:15];
}

-(void)layoutSubviews
{
    self.startLabel.font = [AAPLStyleUtilities smallFont];
    self.finishLabel.font = [AAPLStyleUtilities smallFont];
    self.distanceLabel.font = [AAPLStyleUtilities smallFont];
    self.dayOfWeekLabel.font = [AAPLStyleUtilities smallFont];
    self.dollarLabel.font = [AAPLStyleUtilities smallFont];
    
//    self.startLabel.textColor = [AAPLStyleUtilities foregroundColor];
//    self.finishLabel.textColor = [AAPLStyleUtilities foregroundColor];
//    self.distanceLabel.textColor = [AAPLStyleUtilities foregroundColor];
//    self.dayOfWeekLabel.textColor = [AAPLStyleUtilities foregroundColor];
//    self.dollarLabel.textColor = [AAPLStyleUtilities foregroundColor];
    
    self.startLabel.backgroundColor = [AAPLStyleUtilities transparentBlackColor];
    self.finishLabel.backgroundColor = [AAPLStyleUtilities transparentBlackColor];
    self.distanceLabel.backgroundColor = [AAPLStyleUtilities transparentBlackColor];
    self.dayOfWeekLabel.backgroundColor = [AAPLStyleUtilities transparentBlackColor];
    self.dollarLabel.backgroundColor = [AAPLStyleUtilities transparentBlackColor];
//    self.trashButton.tintColor = [UIColor whiteColor];
    [self.contentView bringSubviewToFront:self.trashButton];
    
}


//Should I use this:
//https://developers.google.com/maps/documentation/staticmaps/
-(void)setSummary:(TripSummary *)summary{
    if (summary) {
        if (![_summary.objectId isEqualToString:summary.objectId]) {
            _summary = summary;
            [_summary tripDetailWithBlock:^(TripSummary *tSummary, NSError *error) {
            if (!error){
                self.dayOfWeekLabel.text = [tSummary dayOfWeek];
                self.distanceLabel.text = [tSummary friendlyDistance];
                self.startLabel.text = [tSummary friendlyStartAddress];
                self.dollarLabel.text = [tSummary friendlyDollars];
                Trip *startRecord = tSummary.startRecord;
                if (startRecord) {
                    GMSCameraPosition *startCamera=  [GMSCameraPosition cameraWithLatitude:startRecord.location.latitude
                                                                                 longitude:startRecord.location.longitude
                                                                                      zoom:15];
                    if (startCamera) {
                        if ([self viewWithTag:121]) {
                            GMSMapView *targetView = (GMSMapView *)[self viewWithTag:121];
                            [targetView removeFromSuperview];
                        }
                        GMSMapView *startView = [GMSMapView mapWithFrame:self.startMapView.frame camera:startCamera];
                        startView.tag = 121;
                        startView.settings.zoomGestures = NO;
                        startView.settings.scrollGestures = NO;
                        startView.settings.rotateGestures = NO;
                        startView.settings.tiltGestures = NO;
                        
                        startView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
                        UIViewAutoresizingFlexibleHeight |
                        UIViewAutoresizingFlexibleBottomMargin;
                        GMSMarker *dMarker = [[GMSMarker alloc] init];
                        dMarker.icon = [UIImage imageNamed:@"origin_icon"];
                        dMarker.position = CLLocationCoordinate2DMake(startRecord.location.latitude, startRecord.location.longitude);
                        dMarker.map = startView;
                        [self addSubview:startView];
                    }
                }
                
                Trip *endRecord = tSummary.endRecord;
                self.finishLabel.text = [tSummary friendlyEndAddress];
                [self.contentView bringSubviewToFront:self.trashButton];
                if (endRecord) {
                    GMSCameraPosition *endPosition =  [GMSCameraPosition cameraWithLatitude:endRecord.location.latitude
                                                                                  longitude:endRecord.location.longitude
                                                                                       zoom:15];
                    if (endPosition) {
                        if ([self viewWithTag:122]) {
                            GMSMapView *targetView = (GMSMapView *)[self viewWithTag:122];
                            [targetView removeFromSuperview];
                        }
                        GMSMapView *endView = [GMSMapView mapWithFrame:self.finishMapView.frame camera:endPosition];
                        endView.tag = 122;
                        endView.settings.zoomGestures = NO;
                        endView.settings.scrollGestures = NO;
                        endView.settings.rotateGestures = NO;
                        endView.settings.tiltGestures = NO;
                        endView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
                        UIViewAutoresizingFlexibleHeight |
                        UIViewAutoresizingFlexibleBottomMargin;
                        
                        GMSMarker *dMarker = [[GMSMarker alloc] init];
                        dMarker.icon = [UIImage imageNamed:@"destination_icon"];
                        dMarker.position = CLLocationCoordinate2DMake(endRecord.location.latitude, endRecord.location.longitude);
                        dMarker.map = endView;
                        [self addSubview:endView];
                    }
                    
                }
                [self layoutSubviews];
            }
            }];
        }
    }
        
}



@end
