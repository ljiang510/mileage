//
//  PageContentViewController.m
//  PageViewDemo
//
//  Created by Simon on 24/11/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "PageContentViewController.h"
#import "AAPLStyleUtilities.h"
@interface PageContentViewController ()

@end

@implementation PageContentViewController
@synthesize titleText, imageFile;

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.backgroundImageView.image = [UIImage imageNamed:
                                      [NSString stringWithFormat:@"%@", self.imageFile]];
    self.titleLabel.text =  self.titleText;
    
    self.titleLabel.textColor = [AAPLStyleUtilities foregroundColor];
    self.titleLabel.font = [AAPLStyleUtilities largeFont];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
