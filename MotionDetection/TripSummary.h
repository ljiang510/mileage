//
//  TripSummary.h
//  MotionDetection
//
//  Created by Liangjun Jiang on 5/23/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>
#import <Parse/Parse.h>
typedef NS_OPTIONS(NSInteger, TRIP_TYPE){
    TRIP_UNCLASSIFIED = 0,
    TRIP_PERSONAL = 1,
    TRIP_BUSINESS
};

@class Trip;
@class Car;
@class Project;
@interface TripSummary : PFObject<PFSubclassing>
@property (nonatomic, assign) double distance;
@property (nonatomic, strong) Trip *startRecord;
@property (nonatomic, strong) Trip *endRecord;
@property (nonatomic, strong) NSString *startAddress;
@property (nonatomic, strong) NSString *endAdress;
@property (nonatomic, assign) TRIP_TYPE tripType;
@property (nonatomic, strong) PFUser *driver;
@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;
@property (nonatomic, strong) Car *car;
@property (nonatomic, strong) Project *project;


-(void)initWithDemoData:(NSDictionary *)demo;
-(void)initWithStartRecord:(Trip *)start end:(Trip *)end;

-(NSString *)friendlyStartTime;
-(NSString *)friendlyEndTime;

-(NSString *)friendlyDistance;
-(NSString *)friendlyStartAddress;
-(NSString *)friendlyEndAddress;
-(NSString *)friendlyDollars;
-(NSString *)dayOfWeek;
-(NSURLSessionDataTask *)tripDetailWithBlock:(void (^)(TripSummary *summary, NSError *error))block;

+(PFQuery *)basicQuery;
+(PFQuery *)queryForTripType:(TRIP_TYPE)type;
+(PFQuery *)queryForTripReview;
+(PFQuery *)queryForThisWeek;
+(PFQuery *)queryForThisMonth;
+(PFQuery *)queryForThisYear;
-(NSString *)reportString;
-(NSString *)friendlyCarProject;
-(NSString *)friendlyTripType;
-(NSString *)friendlyStartInfo;
-(NSString *)friendlyEndInfo;

-(NSString *)reportDateFormattedString:(NSDate *)date;
@end
