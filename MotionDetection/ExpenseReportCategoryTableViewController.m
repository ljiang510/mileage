//
//  ExpenseReportCategoryTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/30/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "ExpenseReportCategoryTableViewController.h"
#import "DataProcessingManager.h"
#import "Tag.h"
#import "Project.h"
#import "Expense.h"
#import "ExpenseTableViewCell.h"
#import "AAPLStyleUtilities.h"
#import "NSNumberFormatter+ThreadSafe.h"
@interface ExpenseReportCategoryTableViewController ()
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic, strong) NSDictionary *expensesProjects;
@property (nonatomic, strong) NSDictionary *expensesCategories;

@end

@implementation ExpenseReportCategoryTableViewController


-(IBAction)onSegmentControl:(id)sender {
    [self.tableView reloadData];
}

-(void)setExpenses:(NSArray *)expenses
{
    if (_expenses != expenses) {
        
        _expenses = expenses;
        
        self.expensesCategories = [[DataProcessingManager sharedManager] expensesByCategories:_expenses];
        self.expensesProjects = [[DataProcessingManager sharedManager] expensesByProjects:_expenses];
        
        [self.tableView reloadData];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger categoryCount = [[self.expensesCategories allKeys] count];
    NSInteger projectCount = [[self.expensesProjects allKeys] count];
    return (self.segmentControl.selectedSegmentIndex == 0)?projectCount:categoryCount;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSString *name;
    NSArray *values;
    if (self.segmentControl.selectedSegmentIndex == 0) {
        name = [self.expensesProjects allKeys][section];
        values = self.expensesProjects[name];
    } else {
        name = [self.expensesCategories allKeys][section];
        values = self.expensesCategories[name];
    }
    return [values count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *name = @"";
    NSArray *values;
    double total = 0.0;
    NSString *formattedTotal = @"";
    if (self.segmentControl.selectedSegmentIndex == 0) {
        name = [self.expensesProjects allKeys][section];
        values = self.expensesProjects[name];

        for (Expense *e in values) {
            total += e.total;
        }
        
    } else {
        name = [self.expensesCategories allKeys][section];
        values = self.expensesCategories[name];
        
        for (Expense *e in values) {
            total += e.total;
        }
    }
    formattedTotal = [[NSNumberFormatter currencyFormatter] stringFromNumber:[NSNumber numberWithDouble:total]];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 1.0, 320.0, 28.0)];
    view.backgroundColor = [AAPLStyleUtilities foregroundColor];
    
    UILabel *titleLabel= [[UILabel alloc] initWithFrame:CGRectMake(15.0, 0.0, 200.0, 28.0)];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [AAPLStyleUtilities standardFont];
    
    titleLabel.text = name;
    [view addSubview:titleLabel];
    
    UILabel *totalLabel= [[UILabel alloc] initWithFrame:CGRectMake(215.0, 0.0, 310.0, 28.0)];
    totalLabel.textColor = [UIColor whiteColor];
    totalLabel.backgroundColor = [UIColor clearColor];
    totalLabel.font = [AAPLStyleUtilities standardFont];
    totalLabel.text = formattedTotal;
    [view addSubview:totalLabel];
    
    return view;
 
}

- (ExpenseTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ExpenseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReportCell" forIndexPath:indexPath];
    
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    NSString *name = @"";
    NSArray *value;
    if (self.segmentControl.selectedSegmentIndex == 0) {
        name = [self.expensesProjects allKeys][section];
        value = self.expensesProjects[name];
    } else {
        name = [self.expensesCategories allKeys][section];
        value = self.expensesCategories[name];
  }
    Expense *expense = value[row];
    [cell setItem:expense];
    return cell;
}



@end
