//
//  EMABm
//  Chapter11
//
//  Created by Liangjun Jiang on 4/21/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "EMABEmailTextField.h"
#import "AAPLStyleUtilities.h"
@implementation EMABEmailTextField

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.borderStyle = UITextBorderStyleNone;
        self.font = [AAPLStyleUtilities standardFont];
        self.textColor = [AAPLStyleUtilities foregroundColor];
        self.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email",@"") attributes:@{NSFontAttributeName:[AAPLStyleUtilities standardFont],NSForegroundColorAttributeName:[UIColor whiteColor]}];
        self.backgroundColor = [UIColor clearColor];
        self.autocorrectionType = UITextAutocorrectionTypeNo;	// no auto correction support
        
        self.keyboardType = UIKeyboardTypeEmailAddress;	// use the default type input method (entire keyboard)
        self.returnKeyType = UIReturnKeyDone;
        
        self.clearButtonMode = UITextFieldViewModeWhileEditing;	// has a clear 'x' button to the right
        
        // Add an accessibility label that describes what the text field is for.
        [self setAccessibilityLabel:NSLocalizedString(@"Email", @"")];

    }
    
    return self;
    
}

@end
