

#import "APLEditableTableViewCell.h"
#import "AAPLStyleUtilities.h"
@interface APLEditableTableViewCell ()

@property (nonatomic, weak) IBOutlet UITextField *textField;
@end

@implementation APLEditableTableViewCell

-(void)layoutSubviews
{
    self.textField.font = [AAPLStyleUtilities standardFont];
    self.textField.textColor = [AAPLStyleUtilities foregroundColor];
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];

    // Edit the text field should only be enabled when in editing mode.
    self.textField.enabled = editing;
}
@end
