

#import "Trip.h"
#import "NSDateFormatter+ThreadSafe.h"
#import  <Parse/PFObject+Subclass.h>
#import "Constants.h"
@implementation Trip
@dynamic location, timestamp,status;
+(NSString *)parseClassName
{
    return kTrip;
}

-(void)initWithDemoData:(NSDictionary *)demo{
    if (demo){
        double latitude = [demo[@"latitude"] doubleValue];
        double longtitude = [demo[@"longtitude"] doubleValue];
        self.location = [PFGeoPoint geoPointWithLatitude:latitude longitude:longtitude];
        self.status = [demo[@"status"] integerValue];
    }
}
-(void)initWithAttributes:(NSArray *)attributes{
   
    if (attributes) {
        double latitude = [attributes[0] doubleValue];
        double longtitude = [attributes[1] doubleValue];
        self.location = [PFGeoPoint geoPointWithLatitude:latitude longitude:longtitude];
        NSDateFormatter *dateFormatter = [NSDateFormatter dateReader];
        self.timestamp = [dateFormatter dateFromString:attributes[2]];
        self.status = [attributes[3] integerValue];
    }
}

-(NSString *)readableDayOfWeek{
    NSString *dateTime = @"";
    if (self.timestamp) {
        NSDateFormatter *dateFormatter = [NSDateFormatter dateWriter];
        NSString *result = [dateFormatter weekday:self.timestamp];
        return result;
    }
    
    return dateTime;
}

-(NSString *)readableStatus:(DrivingStatus)status {
    NSString *title = @"";
    switch (status) {
        case DRIVING_STARTED:
            title = @"Started";
            break;
        case DRIVING_ONGOING:
            title = @"Ongoing";
            break;
        case DRIVING_IDEL:
            title = @"Idel";
            break;
        case DRIVING_STOPPED:
            title = @"Stopped";
            break;
        default:
            break;
    }
    
    return title;
    
}
@end
