//
//  Car.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/8/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "Car.h"
#import "Driver.h"
#import "Constants.h"
@implementation Car
@dynamic name,driver, isDefault;

+(NSString *)parseClassName
{
    return kCar;
}

+(PFQuery *)basicQuery{
    PFQuery *query = [PFQuery queryWithClassName:[self parseClassName]];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    
    [query whereKey:@"driver" equalTo:[Driver currentUser]];
    [query orderByAscending:@"name"];
    [query orderByDescending:@"createdAt"];
    return query;
    
}

-(void)initWithDemoData:(NSDictionary *)demo{
    if (demo){
        self.name = demo[@"name"];
    }
}

@end
