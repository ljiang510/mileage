//
//  EMABUserProfileTableViewCell.m
//  Chapter20
//
//  Created by Liangjun Jiang on 4/23/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "ManualMileageTableViewCell.h"
#import "Constants.h"
#import "AAPLStyleUtilities.h"
@implementation ManualMileageTableViewCell

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.textLabel.font = [AAPLStyleUtilities standardFont];
        self.textLabel.textColor = [AAPLStyleUtilities foregroundColor];
        self.textLabel.numberOfLines = 2;
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        CGRect frame = CGRectMake(150, kTopMargin, 150.0, kTextFieldHeight);
        self.textField = [[UITextField alloc] initWithFrame:frame];
        self.textField.borderStyle = UITextBorderStyleNone;
        self.textField.font = [AAPLStyleUtilities standardFont];
        self.textField.textColor = [AAPLStyleUtilities foregroundColor];
        self.textField.textAlignment = NSTextAlignmentRight;
        self.textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.textField.enabled = NO;
        self.textField.backgroundColor = [UIColor clearColor];
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;	// no auto correction support
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        self.textField.returnKeyType = UIReturnKeyDone;
        self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;	// has a clear 'x' button to the right
        
        self.accessoryView = self.textField;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setContentForTableCellLabel:(NSString*)title placeHolder:(NSString *)placeHolder text:(NSString *)text keyBoardType:(NSNumber *)type enabled:(BOOL)enabled
{
    self.textLabel.text = title;
    self.textField.text = text;
    self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolder attributes:@{NSFontAttributeName:[AAPLStyleUtilities standardFont]}];
    self.textField.keyboardType = [type intValue];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.textField.frame.size.height - 1, self.textField.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor grayColor].CGColor;
    [self.textField.layer addSublayer:bottomBorder];

    self.textField.enabled = enabled;
    
}


@end
