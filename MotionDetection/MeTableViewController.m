//
//  MeTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/8/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "MeTableViewController.h"
#import "Driver.h"
#import "Car.h"
#import "Project.h"
#import "AAPLStyleUtilities.h"
typedef NS_OPTIONS(NSInteger, SECION_NAME){
    SECTION_CAR = 0,
    SECTION_PROJECT
};

@interface MeTableViewController ()
@property (nonatomic,strong) Driver *driver;
@property (nonatomic, strong) NSArray *cars;
@property (nonatomic, strong) NSArray *projects;
@property (nonatomic, strong) Car *selectedCar;
@property (nonatomic, strong) Project *selectedProject;
@property (nonatomic, strong) NSIndexPath *carIndexPath;
@property (nonatomic, strong) NSIndexPath *projectIndexPath;



@end

@implementation MeTableViewController

-(void)setTSummary:(TripSummary *)tSummary
{
    _tSummary = tSummary;
    _selectedCar = _tSummary.car;
    _selectedProject = _tSummary.project;
    [self.tableView reloadData];
}


-(void)loadCars:(id)sender
{
    __weak typeof(self)weakSelf = self;
    PFQuery *carQuery = [Car basicQuery];
    [carQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if(!error) {
            weakSelf.cars = objects;
            if ([objects count]>0) {
                [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:SECTION_CAR] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }
    }];
    
}

-(void)loadProjects:(id)sender
{
    __weak typeof(self)weakSelf = self;
    PFQuery *projectQuery = [Project basicQuery];
    [projectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if(!error) {
            weakSelf.projects = objects;
            if ([objects count] > 0) {
                [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:SECTION_PROJECT] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
           
        }
    }];
    
}

-(IBAction)onCancel:(id)sender
{
//    self.cancelBlock(self);
    [self.navigationController popViewControllerAnimated:YES];
 
}

-(IBAction)onDone:(id)sender
{
    self.finishBlock(self, self.selectedCar, self.selectedProject);
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = YES;
    [self loadCars:nil];
    [self loadProjects:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return (section == SECTION_CAR)?[self.cars count]:[self.projects count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DriverCell" forIndexPath:indexPath];
    
    if (indexPath.section == SECTION_CAR) {
        Car *car = self.cars[indexPath.row];
        cell.textLabel.text = car.name;
        
        if (self.selectedCar) {
            if ([car.objectId isEqualToString:self.selectedCar.objectId]) {
                self.carIndexPath = indexPath;
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }
        
    } else {
        Project *project = self.projects[indexPath.row];
        cell.textLabel.text = project.name;
        if (self.selectedProject) {
            if ([project.objectId isEqualToString:self.selectedProject.objectId]) {
                self.projectIndexPath = indexPath;
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }
        
    }
    cell.textLabel.textColor = [AAPLStyleUtilities foregroundColor];
    cell.textLabel.font = [AAPLStyleUtilities standardFont];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    UITableViewCell *oldCell;
    if (indexPath.section == SECTION_CAR) {
        oldCell = [tableView cellForRowAtIndexPath:self.carIndexPath];
            } else {
        oldCell = [tableView cellForRowAtIndexPath:self.projectIndexPath];
    }
    
    
    oldCell.accessoryType = UITableViewCellAccessoryNone;


    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    if (indexPath.section == SECTION_PROJECT) {
        self.selectedProject = self.projects[indexPath.row];
        self.projectIndexPath = indexPath;
    } else {
        self.selectedCar = self.cars[indexPath.row];
        self.carIndexPath = indexPath;
        
    }
}


@end
