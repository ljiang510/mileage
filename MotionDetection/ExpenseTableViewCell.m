//
//  ExpenseTableViewCell.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/3/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "ExpenseTableViewCell.h"
#import "Expense.h"
#import "Tag.h"
#import "NSDateFormatter+ThreadSafe.h"
#import "AAPLStyleUtilities.h"
@interface ExpenseTableViewCell()
@property (nonatomic, weak) IBOutlet UILabel *businessLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalLabel;
@property (nonatomic, weak) IBOutlet UILabel *whenLabel;
@property (nonatomic, weak) IBOutlet UILabel *categoryLabel;

@end

@implementation ExpenseTableViewCell

-(void)setItem:(Expense *)expense{
    if (expense) {
        self.businessLabel.textColor = [AAPLStyleUtilities foregroundColor];
        self.totalLabel.textColor = [AAPLStyleUtilities foregroundColor];
        self.whenLabel.textColor = [AAPLStyleUtilities foregroundColor];
        self.categoryLabel.textColor = [AAPLStyleUtilities foregroundColor];
        self.businessLabel.font = [AAPLStyleUtilities standardFont];
        self.totalLabel.font = [AAPLStyleUtilities standardFont];
        self.whenLabel.font = [AAPLStyleUtilities standardFont];
        self.categoryLabel.font = [AAPLStyleUtilities standardFont];
        
        self.businessLabel.text = expense.businessName;
        self.totalLabel.text = [expense friendlyTotal];
        NSDateFormatter *dateFormatter = [NSDateFormatter dateWriter];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
        self.whenLabel.text = [dateFormatter stringFromDate:expense.when];
        self.categoryLabel.text = expense.tag.name;
    }
}
@end
