
#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
typedef NS_OPTIONS(NSInteger, DrivingStatus){
    DRIVING_STARTED = 0,
    DRIVING_ONGOING = 1,
    DRIVING_IDEL = 2,
    DRIVING_STOPPED = 3
};


@interface Trip : PFObject<PFSubclassing>
@property (nonatomic, strong) NSDate *timestamp;
@property (nonatomic, strong) PFGeoPoint *location;
@property (nonatomic, assign) DrivingStatus status;

-(void)initWithDemoData:(NSDictionary *)demo;
-(void)initWithAttributes:(NSArray *)attributes;
-(NSString *)readableStatus:(DrivingStatus)status;
-(NSString *)readableDayOfWeek;
@end
