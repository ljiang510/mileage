//
//  TripSummary.m
//  MotionDetection
//
//  Created by Liangjun Jiang on 5/23/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "TripSummary.h"
#import "Constants.h"
#import "Trip.h"
#import "APIClient.h"
#import "NSDateFormatter+ThreadSafe.h"
#import "NSCalendar+ThreadSafe.h"
#import <Parse/PFObject+Subclass.h>
#import "Project.h"
#import "Car.h"

@implementation TripSummary
@dynamic distance, startAddress, endAdress, startRecord, endRecord, tripType, driver,startTime,endTime,project, car;


+(NSString *)parseClassName
{
    return kTripSummary;
}


-(void)initWithStartRecord:(Trip *)start end:(Trip *)end {
    self.distance = 0.0;
    self.startAddress = @"";
    self.endAdress = @"";
    self.startRecord = start;
    self.endRecord = end;
    self.tripType = TRIP_UNCLASSIFIED;
    self.driver = [PFUser currentUser];
}

+(PFQuery *)basicQuery{
    PFQuery *query = [PFQuery queryWithClassName:[self parseClassName]];
    query.cachePolicy = kPFCachePolicyIgnoreCache;
    [query includeKey:@"startRecord"];
    [query includeKey:@"endRecord"];
    [query includeKey:@"project"];
    [query includeKey:@"car"];
    [query orderByDescending:@"createdAt"];
    [query whereKey:@"driver" equalTo:[PFUser currentUser]];
    return query;
}

+(PFQuery *)queryForTripType:(TRIP_TYPE)type{
    PFQuery *query = [self basicQuery];
    [query whereKey:@"tripType" equalTo:@(type)];
    return query;
}

+(PFQuery *)queryForTripReview{
    PFQuery *query = [self basicQuery];
    [query whereKey:@"tripType" notEqualTo:@(TRIP_UNCLASSIFIED)];
    return query;
    
}

+(PFQuery *)queryForThisWeek{
    PFQuery *query = [self queryForTripType:TRIP_BUSINESS];
    NSCalendar *calendar = [NSCalendar safeCalendar];
    NSDate *today = [NSDate date];
    NSDate *startDateOfThisWeek = [calendar startOfDayThisWeek:[NSDate date]];
    [query whereKey:@"createdAt" greaterThanOrEqualTo:startDateOfThisWeek];
    [query whereKey:@"createdAt" lessThanOrEqualTo:today];
    
    return query;
}

+(PFQuery *)queryForThisMonth{
    PFQuery *query = [self queryForTripType:TRIP_BUSINESS];
    
    NSCalendar *calendar = [NSCalendar safeCalendar];
    NSDate *today = [NSDate date];
    NSDate *startDateOfThisMonth = [calendar startOfDayThisMonth];
    [query whereKey:@"createdAt" greaterThanOrEqualTo:startDateOfThisMonth];
    [query whereKey:@"createdAt" lessThanOrEqualTo:today];
    
    return query;
}

+(PFQuery *)queryForThisYear{
    PFQuery *query = [self queryForTripType:TRIP_BUSINESS];
    
    NSCalendar *calendar = [NSCalendar safeCalendar];
    NSDate *today = [NSDate date];
    NSDate *startDateOfThisYear = [calendar firstDayOfThisYear];
    [query whereKey:@"createdAt" greaterThanOrEqualTo:startDateOfThisYear];
    [query whereKey:@"createdAt" lessThanOrEqualTo:today];
    
    return query;
}


-(void)handleAttributes:(NSDictionary *)attributes{
    if (attributes && [[attributes allKeys] count] > 0) {
        self.startAddress = ([attributes[@"origin_addresses"] count] >=1)?attributes[@"origin_addresses"][0]:@"";
        self.endAdress = ([attributes[@"destination_addresses"] count] >=1)?attributes[@"destination_addresses"][0]:@"";
        NSArray *rows = attributes[@"rows"];
        if ([rows count] >=1) {
            NSArray *elements = rows[0][@"elements"];
            if ([elements count]>=1) {
                self.distance = [elements[0][@"distance"][@"value"] doubleValue] /1609;  //in miles
            }
        }
    }
}

-(NSString *)friendlyStartTime{
    if (self.startTime) {
        return [self reportDateFormattedString:self.startTime];
        
    }
    return @"";
}

-(NSString *)friendlyEndTime{
    if (self.endTime) {
        return [self reportDateFormattedString:self.endTime];
        
    }
    return @"";
}

-(NSString *)dayOfWeek
{
    NSString *dateTime = @"";
    if (self.startRecord) {
        NSDateFormatter *dateFormatter = [NSDateFormatter dateWriter];
        NSString *result = [dateFormatter weekday:self.createdAt];
        return result;
    }
    
    return dateTime;
}

-(NSString *)friendlyTripType{
    return (self.tripType == TRIP_PERSONAL)?NSLocalizedString(@"Personal", @""):NSLocalizedString(@"Business", @"");
    
}

-(NSString *)friendlyDistance{
    return [NSString stringWithFormat:@"%.1f miles",self.distance];
}

-(NSString *)friendlyDollars{
    return [NSString stringWithFormat:@"$%.2f",self.distance*DOLLAR_PER_MILEAGE];
}

-(NSString *)shortTimeStyle:(NSDate *)date{
    NSDateFormatter *formatter = [NSDateFormatter shortStyle];
    return [formatter stringFromDate:date];
    
}

-(NSString *)friendlyStartInfo{
    return [NSString stringWithFormat:@"%@ %@", [self friendlyStartTime],[self startAddress]];
}
-(NSString *)friendlyEndInfo{
    return [NSString stringWithFormat:@"%@ %@", [self friendlyEndTime],[self endAdress]];
}

//some duplicatetion
-(NSString *)friendlyStartAddress{
    NSString *shortTime = (self.startRecord.timestamp)?[self shortTimeStyle:self.startRecord.timestamp]:@"16:56pm";
    return [NSString stringWithFormat:@"%@, %@",shortTime,[self friendlyAddress:self.startAddress]];
}

-(NSString *)friendlyEndAddress{
    NSString *shortTime = (self.endRecord.timestamp)?[self shortTimeStyle:self.endRecord.timestamp]:@"18:21pm";
    return [NSString stringWithFormat:@"%@, %@",shortTime,[self friendlyAddress:self.endAdress]];
}

-(NSURLSessionDataTask *)tripDetailWithBlock:(void (^)(TripSummary *detail, NSError *error))block{
    NSString *originStr = [NSString stringWithFormat:@"origins=%f,%f",self.startRecord.location.latitude,self.startRecord.location.longitude];
    NSString *destinationStr = [NSString stringWithFormat:@"&destinations=%f,%f",self.endRecord.location.latitude,self.endRecord.location.longitude];
    NSString *callURL = @"maps/api/distancematrix/json?";
    NSString *urlString = [callURL stringByAppendingFormat:@"%@%@&sensor=false",originStr,destinationStr];
    return [[APIClient sharedClient] POST:urlString parameters:@{@"key":kAPIKey} success:^(NSURLSessionDataTask * __unused task, id JSON) {
//        NSLog(@"request result:%@",JSON);
        NSDictionary *attributes = (NSDictionary *)JSON;
        [self handleAttributes:attributes];
        if (block) {
            block(self, nil);
        }
    } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
        if (block) {
            block(self, error);
        }
    }];
}

-(NSString *)friendlyAddress:(NSString *)fullAddress{
    // I don't want to make another api call
    if (fullAddress) {
        NSString *address = @"";
        NSArray *addresses =  [fullAddress componentsSeparatedByCharactersInSet:
                               [NSCharacterSet characterSetWithCharactersInString:@","]];
        if ([addresses count]>0) {
            NSString *street = addresses[0];
            NSArray *streets = [street componentsSeparatedByCharactersInSet:
                                [NSCharacterSet characterSetWithCharactersInString:@" "]];
            if ([streets count] > 1) {
                NSString *first = streets[0];
                NSCharacterSet *digitalSet = [NSCharacterSet decimalDigitCharacterSet];
                NSArray *result;
                if ([first rangeOfCharacterFromSet:digitalSet].location != NSNotFound) {
                     result = [streets subarrayWithRange:NSMakeRange(1, [streets count] - 1)];
                    return [[result valueForKey:@"description"] componentsJoinedByString:@" "];
                } else
                    return addresses[0];
                
            }
            return address;
        }
        return address;
    } else
        return @"";
    
}
-(NSString *)friendlyCarProject{
    return [NSString stringWithFormat:@"%@ %@",self.car?self.car.name:@"", self.project?self.project.name:@""];
}

-(NSString *)reportString{
    NSString *str=@"";
    NSString *recordDate =  [self reportDateFormattedString:self.createdAt];
    NSString *project = self.project?self.project.name:@"";
    NSString *car = self.car?self.car.name:@"";
    str = [NSString stringWithFormat:@"%@,%.1f,%@,%@,%@,%@\n",recordDate,self.distance,car,project,self.startAddress, self.endAdress];
    return str;
}

-(NSString *)reportDateFormattedString:(NSDate *)date{
    NSDateFormatter *dateFormmater = [NSDateFormatter dateWriter];
    [dateFormmater setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *str =  [dateFormmater stringFromDate:date];
    return str;
}

-(void)initWithDemoData:(NSDictionary *)demo{
    if (demo){
        self.distance = [demo[@"distance"] doubleValue];
        self.startAddress = demo[@"startAddress"];
        self.endAdress = demo[@"endAddress"];
    }
}

@end
