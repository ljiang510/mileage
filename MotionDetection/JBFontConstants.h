//
//  JBFontConstants.h
//  JBChartViewDemo
//
//  Created by Terry Worona on 11/7/13.
//  Copyright (c) 2013 Jawbone. All rights reserved.
//

#pragma mark - Footers

#define kJBFontFooterLabel [UIFont fontWithName:@"Avenir-Light" size:12.0]
#define kJBFontFooterSubLabel [UIFont fontWithName:@"Avenir" size:10.0]

#pragma mark - Headers

#define kJBFontHeaderTitle [UIFont fontWithName:@"Avenir-Bold" size:24]
#define kJBFontHeaderSubtitle [UIFont fontWithName:@"Avenir-Light" size:14]

#pragma mark - Information

#define kJBFontInformationTitle [UIFont fontWithName:@"Avenir-Light" size:20]
#define kJBFontInformationValue [UIFont fontWithName:@"AvenirNext-Bold" size:80]
//@"Avenir-Light"
#define kJBFontInformationUnit [UIFont fontWithName:@"Avenir-Medium" size:60]

#pragma mark - Tooltip

#define kJBFontTooltipText [UIFont fontWithName:@"AvenirNext-Bold" size:14]
