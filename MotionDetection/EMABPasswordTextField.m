//
//  EMABPasswordTextField.m
//  Chapter11
//
//  Created by Liangjun Jiang on 4/21/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "EMABPasswordTextField.h"
#import "AAPLStyleUtilities.h"
@implementation EMABPasswordTextField

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.borderStyle = UITextBorderStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.font = [AAPLStyleUtilities standardFont];
        self.textColor = [AAPLStyleUtilities foregroundColor];
        self.keyboardType = UIKeyboardTypeDefault;
        self.returnKeyType = UIReturnKeyDone;
        self.secureTextEntry = YES;	// make the text entry secure (bullets)
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Password",@"") attributes:@{NSFontAttributeName:[AAPLStyleUtilities standardFont],NSForegroundColorAttributeName:[UIColor whiteColor]}];
        self.clearButtonMode = UITextFieldViewModeWhileEditing;	// has a clear 'x' button to the right
        [self setAccessibilityLabel:NSLocalizedString(@"Password", @"")];
    }
    
    return self;
    
}

@end
