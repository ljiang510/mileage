//
//  Project.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/8/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "Project.h"
#import "Constants.h"
#import "Driver.h"
@implementation Project
@dynamic name,driver, isDefault;

+(NSString *)parseClassName
{
    return kProject;
}

+(PFQuery *)basicQuery{
    PFQuery *query = [PFQuery queryWithClassName:[self parseClassName]];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query whereKey:@"driver" equalTo:[Driver currentUser]];
    [query orderByAscending:@"name"];
    return query;
    
}

-(void)initWithDemoData:(NSDictionary *)demo{
    if (demo){
        self.name = demo[@"name"];
    }
}
@end
