//
//  AppManager.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/12/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "AppManager.h"
#import "Constants.h"
#import <Parse/Parse.h>
#import "UIImageEffects.h"
@implementation AppManager
@synthesize purchase;

+(instancetype)sharedManager{
    static AppManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        manager = [[self alloc] init];
    });
    
    return manager;
}

-(void)registerNSUserDefault{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{kShouldShowTutorial:@(YES)}];
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{kIsValidPlan:@(NO)}];
}

-(void)setShouldShowTutorial:(BOOL)shouldShow{
    [[NSUserDefaults standardUserDefaults] setBool:shouldShow forKey:kShouldShowTutorial];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL)getShouldShowTutorial
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kShouldShowTutorial];
}

-(void)setAppBadgeNumber:(NSInteger)num{
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = num;
}

-(BOOL)shouldShowDemoData{
    return [PFAnonymousUtils isLinkedWithUser:[PFUser currentUser]];
}

-(UIImage *)effectImage{
    UIImage *backgroundImage = [UIImage imageNamed:kBackgroundImageName];
    return backgroundImage;
//    return  [UIImageEffects imageByApplyingLightEffectToImage:backgroundImage];
}

-(void)setIsValidPlan:(BOOL)isValidPlan{
    [[NSUserDefaults standardUserDefaults] setBool:isValidPlan forKey:kIsValidPlan];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(BOOL)getIsValidPlan{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kIsValidPlan];
}

-(void)setPlanName:(NSString*)planName{
    [[NSUserDefaults standardUserDefaults] setObject:planName forKey:kPlanName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(NSString *)getPlanName{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kPlanName];
}

@end
