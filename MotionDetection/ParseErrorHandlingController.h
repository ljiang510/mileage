//
//  ParseErrorHandlingController.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/1/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParseErrorHandlingController : NSObject

+ (void)handleParseError:(NSError *)error;

@end
