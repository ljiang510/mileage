//
//  Constants.m
//  MotionDetection
//
//  Created by Liangjun Jiang on 5/23/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "Constants.h"

NSString *const kAPIURL = @"http://maps.googleapis.com/maps/api/distancematrix/json?";
NSString *const kAPIKey = @"AIzaSyBwyiMK1JjSFLrWi5AuniydHYv2H84JVKs";
NSString *const kBaseURL =@"https://maps.googleapis.com/";
NSString *const testAPIURL = @"http://maps.googleapis.com/maps/api/distancematrix/json?sensor=false&origins";
NSString *const kMotionStatusChanged = @"kMotionStatusChanged";
NSString *const kFinishLoading = @"kFinishLoading";
NSString *const kParseApplicationId = @"XgSXL5LqIdxgJDTUHiSNRqNugA0mhx8DibmYGd7t";
NSString *const KParseClientKey = @"hs6EeHoodDjUWvRRCAP598H0EXWYEtKn1QL8nH2z";
NSString *const kTrip = @"Trip";
NSString *const kTripSummary = @"TripSummary";
NSString *const kExpense = @"Expense";
NSString *const kTag = @"Tag";
NSString *const kProject = @"Project";
NSString *const kCar = @"Car";
NSString *const kPurchase = @"Purchase";
NSString *const kBackgroundImageName = @"blur";
NSString *const kShouldShowDemo = @"kShouldShowDemo";
NSString *const kShouldShowTutorial = @"kShouldShowTutorial";
NSString *const kIsValidPlan = @"kIsValidPlan";
NSString *const kPlanName = @"kPlanName";


NSString *const kMonthlySubscriptionProductId = @"com.getdeductibleapp.monthly";
NSString *const kAnnualSubscriptionProductId = @"com.getdeductibleapp.annual";

int const DISTANCE_THRESHOLD = 100; //in meter
double const DOLLAR_PER_MILEAGE = 0.575;
int const kMinTextLength = 6;
float const kLeftMargin = 20.0;
float const kTextFieldWidth = 260.0;
float const kTextFieldHeight = 30.0;
float const kTopMargin = 7.0;


@implementation Constants

+(BOOL)isValidEmail:(NSString *)emailAdress{
    //https://github.com/benmcredmond/DHValidation
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailAdress];
}
@end
