

#import "RecordTableViewController.h"
#import "Trip.h"
#import "AppDelegate.h"
#import "TripSummary.h"
#import "RecordInfoTableViewCell.h"
#import "Constants.h"
#import "DGActivityIndicatorView.h"
#import "SVProgressHud.h"
#import "ParseErrorHandlingController.h"
#import "AAPLStyleUtilities.h"
#import "Car.h"
#import "Project.h"
#import "MeTableViewController.h"
#import "DemoManager.h"
#import "InfoPanelOverlayView.h"
#import "AppManager.h"
#import "NSCalendar+ThreadSafe.h"
@interface RecordTableViewController ()<UIGestureRecognizerDelegate>{
    
}
@property (nonatomic, strong) NSMutableArray *trips;
@property (nonatomic, weak) IBOutlet UILabel *instructionLabel;
@property (nonatomic, strong) NSArray *cars;
@property (nonatomic, strong) NSArray *projects;
@property (nonatomic, strong) MeTableViewController *viewController;
//@property (nonatomic, strong) DGActivityIndicatorView *activityView;

@end

@implementation RecordTableViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([[DemoManager sharedManager] isDemo]) {
        [self loadDemoData];
        self.navigationItem.title = NSLocalizedString(@"Demo Trips", @"");
    } else {
        [self loadData:nil];
        self.navigationItem.title = NSLocalizedString(@"Trips", @"");
    }
}

-(void)loadDemoData {
    [self removeInfoPanel];
    self.trips = [NSMutableArray arrayWithArray:[[DemoManager sharedManager] tripSummary]];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //add tableview gesture
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onLeftSwipe:)];
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    leftSwipe.delegate = self;
    [self.tableView addGestureRecognizer:leftSwipe];
    
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onRightSwipe:)];
    [rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
    rightSwipe.delegate = self;
    [self.tableView addGestureRecognizer:rightSwipe];
    
//    self.activityView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor redColor] size:20.0f];
//    self.activityView.frame = CGRectMake(0.0f, 0.0f, 50.0f, 50.0f);
//    self.activityView.center = self.tableView.center;

    self.instructionLabel.font = [AAPLStyleUtilities largeFont];
    self.instructionLabel.textColor = [AAPLStyleUtilities foregroundColor];
    
    UIStoryboard *storyboard = [self storyboard];
    self.viewController = [storyboard instantiateViewControllerWithIdentifier:@"MeTableViewController"];
    
//    NSLog(@"first date of this year: %@",[[[NSCalendar safeCalendar] firstDayOfThisYear] description]);
    
    
}

-(IBAction)loadData:(id)sender {
    [self removeInfoPanel];
    self.trips = [NSMutableArray array];
    [self.tableView reloadData];
//    [self.view addSubview:self.activityView];
//    [self.activityView startAnimating];
    PFQuery *query = [TripSummary queryForTripType:TRIP_UNCLASSIFIED];
    
    __weak typeof(self) weakSelf = self;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
//        [self.activityView stopAnimating];
//        [self.activityView removeFromSuperview];
        if ([weakSelf.refreshControl isRefreshing]) {
            [weakSelf.refreshControl endRefreshing];
        }
        if (!error) {
            if ([objects count] == 0) {
                [weakSelf showInfoPanel];
                weakSelf.navigationItem.title = NSLocalizedString(@"Trips", @"");
            } else {
                [weakSelf removeInfoPanel];
                weakSelf.trips = [NSMutableArray arrayWithArray:objects];
                [weakSelf.tableView reloadData];
                
                [weakSelf customTitleBar];
            }
        } else {
            [ParseErrorHandlingController handleParseError:error];
        }
        
    }];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([[DemoManager sharedManager] isDemo]) {
        [self demoAlertController];
    }
}



#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 180.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    return [self.trips count];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSInteger lastSectionIndex = [tableView numberOfSections] - 1;
//    NSInteger lastRowIndex = [tableView numberOfRowsInSection:lastSectionIndex] - 1;
//    if ((indexPath.section == lastSectionIndex) && (indexPath.row == lastRowIndex)) {
//        [self loadNextPage];
//    }
}


- (RecordInfoTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        RecordInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RecordCell" forIndexPath:indexPath];
        if (self.trips && [self.trips count] > 0) {
            TripSummary *summary = (TripSummary *)(self.trips[indexPath.row]);
            [cell setSummary:summary];
            cell.trashButton.tag = indexPath.row + 100;
        }
        
        return cell;
    
}

#pragma mark - swipe gesture
-(void)onLeftSwipe:(UIGestureRecognizer *)gestureRecognizer
{
    CGPoint point = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    if (indexPath) {
        
        RecordInfoTableViewCell *cell = (RecordInfoTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        __block TripSummary *tripSummary = cell.summary;
        
        self.viewController.cancelBlock = ^(MeTableViewController *viewController){
            
        };
        
        __weak typeof(self) weakSelf = self;
        self.viewController.finishBlock = ^(MeTableViewController *viewController, Car *car, Project *project){
            tripSummary.startTime = tripSummary.startRecord.createdAt;
            tripSummary.endTime = tripSummary.endRecord.createdAt;
            tripSummary.car = car;
            tripSummary.project = project;
            [tripSummary setTripType:TRIP_BUSINESS];
            
            [weakSelf.trips removeObjectAtIndex:indexPath.row];
            [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            if ([[DemoManager sharedManager] isDemo]) {
                
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Demo data won't be saved.", @"")];
                if ([weakSelf.trips count] == 0) {
                    [weakSelf showInfoPanel];
                }

            } else {
                [tripSummary saveInBackgroundWithBlock:^(BOOL success, NSError *error){
                    if (!error) {
                    
                        
                    }
                }];
              
                [weakSelf reduceBadgeCount];
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Saved", @"")];
                if ([weakSelf.trips count] == 0) {
                    [weakSelf showInfoPanel];
                }

            }
            
        };
        
        [self.navigationController pushViewController:self.viewController animated:YES];
    }
}

-(void)onRightSwipe:(UIGestureRecognizer *)gestureRecognizer
{
    CGPoint point = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    if (indexPath) {
        RecordInfoTableViewCell *cell = (RecordInfoTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        TripSummary *tripSummary = cell.summary;
        tripSummary.startTime = tripSummary.startRecord.createdAt;
        tripSummary.endTime = tripSummary.endRecord.createdAt;
        [tripSummary setTripType:TRIP_PERSONAL];
        [self.trips removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self reduceBadgeCount];
        if ([self.trips count] == 0) {
            [self showInfoPanel];
        }
        if (![[DemoManager sharedManager] isDemo]) {
            [tripSummary saveInBackgroundWithBlock:^(BOOL success, NSError *error){
                if (!error) {
                    
                   
                }
            }];
        }
    }
}

-(void)reduceBadgeCount
{
    NSInteger  currentCount = [UIApplication sharedApplication].applicationIconBadgeNumber;
    if (currentCount >0) {
         [UIApplication sharedApplication].applicationIconBadgeNumber = currentCount-1;
    }
   
    
}

-(IBAction)onTrashCan:(id)sender{
    UIView *contentView = (UIView *)[sender superview];
    UITableViewCell *cell = (UITableViewCell *)[contentView superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    //this doesn't matter
    if (indexPath) {
        TripSummary *deleted = self.trips[indexPath.row];
        if (deleted) {
            [self.trips removeObject:deleted];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            if ([self.trips count] == 0) {
                [self showInfoPanel];
            }
            if (![[DemoManager sharedManager] isDemo]) {
                __weak typeof(self) weakSelf = self;
                [deleted deleteInBackgroundWithBlock:^(BOOL success, NSError *error){
                    if (!error) {
                        [weakSelf reduceBadgeCount];
                        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Deleted", @"")];
                    }
                }];
                
            }
            
        }
    }
}

#pragma amrk - 
-(void)showInfoPanel{
    InfoPanelOverlayView *overlayView = [[InfoPanelOverlayView alloc] initWithFrame:CGRectMake(10.0, 0.0, self.tableView.frame.size.width-20, 80)];
    overlayView.tag = 101;
    overlayView.center = CGPointMake(self.tableView.center.x, self.tableView.center.y - 32.0);
    overlayView.alpha = 0.0;
    [UIView animateWithDuration:0.3 animations:^{
        overlayView.alpha = 1.0;
    }];
    [self.tableView addSubview:overlayView];
    [self.tableView bringSubviewToFront:overlayView];
}

-(void)removeInfoPanel
{
    InfoPanelOverlayView *overlayView = (InfoPanelOverlayView *)[self.tableView viewWithTag:101];
    if (overlayView) {
        [overlayView removeFromSuperview];
    }
}

#pragma mark -
-(void)customTitleBar
{
    
    NSArray *segmentTextContent = @[
                                    NSLocalizedString(@"All Business", @""),
                                    NSLocalizedString(@"Delete all", @""),
                                    NSLocalizedString(@"All Personal", @""),
                                    ];
    
    // Segmented control as the custom title view
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:segmentTextContent];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    segmentedControl.frame = CGRectMake(0, 0, 400.0f, 30.0f);
    [segmentedControl addTarget:self action:@selector(onSegmentControl:) forControlEvents:UIControlEventValueChanged];
    
    self.navigationItem.titleView = segmentedControl;
    
}

-(void)onSegmentControl:(id)sender{
    UISegmentedControl *control = (UISegmentedControl *)sender;
    if  ([self.trips count] > 0){
       
        self.navigationItem.titleView = nil;
        self.navigationItem.title = NSLocalizedString(@"Trips", @"");
        
        NSInteger  currentCount = [UIApplication sharedApplication].applicationIconBadgeNumber;
        if (currentCount >0) {
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        }
        
        [self showInfoPanel];
        
        __weak typeof(self) weakSelf = self;
        if (control.selectedSegmentIndex == 1) {
            [TripSummary deleteAllInBackground:self.trips block:^(BOOL success, NSError *error){
                if (!error) {
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Deleted", @"Deleted")];
                    [weakSelf.trips removeAllObjects];
                    [weakSelf.tableView reloadData];
                }
            }];
        } else {
            NSMutableArray *results = [NSMutableArray arrayWithCapacity:[self.trips count]];
            NSArray *allCells = [self.tableView visibleCells];
            for (RecordInfoTableViewCell *cell in allCells) {
               TripSummary *summary = cell.summary;
                if (control.selectedSegmentIndex == 0) {
                    [summary setTripType:TRIP_BUSINESS];
                } else if (control.selectedSegmentIndex == 2){
                    [summary setTripType:TRIP_PERSONAL];
                }
                summary.startTime = summary.startRecord.createdAt;
                summary.endTime = summary.endRecord.createdAt;
                [results addObject:summary];
            }

            [TripSummary saveAllInBackground:results block:^(BOOL success, NSError *error){
                if (!error) {
                    [weakSelf.trips removeAllObjects];
                    [weakSelf.tableView reloadData];

                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Saved", @"Saved")];
                }
            }];
        }
        
        
        
    }
}

-(void)demoAlertController {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Demo Data", @"")
                                                                   message:NSLocalizedString(@"We are showing some demo data. Log In or Sign Up to log your mileage and expense", @"")
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Sign Up", @"") style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [self showLogin];
                                                          }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Later", @"") style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {}];
    
    [alert addAction:cancelAction];
    [alert addAction:defaultAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showLogin{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate presentLoginSignUp];
}
@end
