//
//  EMABcustomerProfileTableViewController.m
//  Chapter7
//
//  Created by Liangjun Jiang on 4/19/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "ManualMileageTableViewController.h"
#import "ManualMileageTableViewCell.h"
#import "Driver.h"
#import "TripSummary.h"
#import "SVProgressHud.h"
#import "NSDateFormatter+ThreadSafe.h"
#import "ParseErrorHandlingController.h"
#import "MeTableViewController.h"
#import "DemoManager.h"
static NSString *kTitleKey = @"titleKey";
static NSString *kPlaceholderKey = @"placeholderKey";
static NSString *kKeyboardKey = @"keyboardTypeKey";
#import "AAPLStyleUtilities.h"
typedef NS_OPTIONS(NSInteger, CELL_NAME){
    CELL_DATE = 0,
    CELL_ORIGIN = 1,
    CELL_DESTINATION = 2,
    CELL_START_ODOMETER = 3,
    CELL_END_ODOMETER = 4,
    CELL_CAR_PROJECT
};


@interface ManualMileageTableViewController ()<UITextFieldDelegate>{

}
@property (nonatomic, strong) NSArray *dataSourceArray;
@property (nonatomic, strong) Driver *customer;
@property (nonatomic, strong) TripSummary *tripSummary;
@property (nonatomic, weak) IBOutlet UILabel *instructionLabel;
@end

@implementation ManualMileageTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.instructionLabel.font = [AAPLStyleUtilities largeFont];
    self.instructionLabel.textColor = [AAPLStyleUtilities foregroundColor];
    
    self.tripSummary = [TripSummary object];
    [self.tripSummary initWithStartRecord:nil end:nil];
    [self.tripSummary setTripType:TRIP_BUSINESS];
    [self.tripSummary setDriver:[Driver currentUser]];
    self.dataSourceArray = @[
                             @{
                                 kTitleKey:NSLocalizedString(@"Origin Time*", @"Origin Time"),
                                 kPlaceholderKey:NSLocalizedString(@"Origin Time", @"Origin Time"),
                                 kKeyboardKey:@(UIKeyboardTypeNamePhonePad)},
                             @{
                                 kTitleKey:NSLocalizedString(@"Origin*",@"Origin"),
                                 kPlaceholderKey:NSLocalizedString(@"Origin",@""),
                                 kKeyboardKey:@(UIKeyboardTypeNamePhonePad)},
                             @{
                                 kTitleKey:NSLocalizedString(@"End Time*", @"End Time"),
                                 kPlaceholderKey:NSLocalizedString(@"End Time", @"End Time"),
                                 kKeyboardKey:@(UIKeyboardTypeNamePhonePad)},
                             
                             @{
                                 kTitleKey:NSLocalizedString(@"Destination*",@"Destination"),
                                 kPlaceholderKey:@"Destination",
                                 kKeyboardKey:@(UIKeyboardTypeDefault)},
                             
                             @{
                                 kTitleKey:NSLocalizedString(@"Total Miles*",@""),
                                 kPlaceholderKey:@"Total Miles",
                                 kKeyboardKey:@(UIKeyboardTypeDecimalPad)},
                             ];
    
    
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    if (!editing) {
        
    }
}

-(IBAction)onCancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onDone:(id)sender{
    if ([self isValidInput]) {
        if ([[DemoManager sharedManager] isDemo]) {
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Please sign up or log in to save your data", @"Saved")];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            __weak typeof(self) weakSelf = self;
            [self.tripSummary saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Saved", @"Saved")];
                     [weakSelf.navigationController popViewControllerAnimated:YES];
                } else {
                    NSLog(@"error :%@",[error localizedDescription]);
                    [ParseErrorHandlingController handleParseError:error];
                }
            }];
        }
    } else {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please complete the required information denoted by *",@"")];
        
    }
   
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (section == 0)?[self.dataSourceArray count]:1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ManualMileageTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:@"ManualMileageCell" forIndexPath:indexPath];
        
        NSInteger row = indexPath.row;
        manualCell.textField.delegate = self;
        manualCell.textField.tag = 100 + row;
        
        NSString *text = @"";
        
        NSString *title = self.dataSourceArray[row][kTitleKey];
        NSString *placeholder = self.dataSourceArray[row][kPlaceholderKey];
        NSNumber *keyboardType = self.dataSourceArray[row][kKeyboardKey];
        
        [manualCell setContentForTableCellLabel:title placeHolder:placeholder text:text keyBoardType:keyboardType enabled:YES];
        return manualCell;
    }
        UITableViewCell *carCell = [tableView dequeueReusableCellWithIdentifier:@"CarProjectCell"];
        carCell.textLabel.text = NSLocalizedString(@"Car, Project", @"");
        carCell.detailTextLabel.text = [self.tripSummary friendlyCarProject];;
        carCell.textLabel.font = [AAPLStyleUtilities standardFont];
        carCell.textLabel.textColor = [AAPLStyleUtilities foregroundColor];
        carCell.detailTextLabel.textColor  = [AAPLStyleUtilities foregroundColor];
        carCell.detailTextLabel.font = [AAPLStyleUtilities smallFont];
        return carCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        [tableView endEditing:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    } else {
        UIStoryboard *storyboard = [self storyboard];
        MeTableViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MeTableViewController"];
        __weak typeof(self) weakSelf = self;
        viewController.finishBlock = ^(MeTableViewController *viewController, Car *car, Project *project){
            weakSelf.tripSummary.car = car;
            weakSelf.tripSummary.project = project;
            [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
        };
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}

#pragma mark - UITextField Delegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if (textField.tag == 100 || textField.tag == 102) {
        UIDatePicker *datePickerView = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        datePickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        datePickerView.datePickerMode = UIDatePickerModeDateAndTime;
        datePickerView.tag = textField.tag + 1000;
        textField.inputView = datePickerView;
        [datePickerView addTarget:self
                           action:@selector(onDatePicker:)
                 forControlEvents:UIControlEventValueChanged];
        
        // this animiation was from Apple Sample Code: DateCell
        CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
        CGSize pickerSize = [datePickerView sizeThatFits:CGSizeZero];
        CGRect startRect = CGRectMake(0.0,
                                      screenRect.origin.y + screenRect.size.height,
                                      pickerSize.width, pickerSize.height);
        datePickerView.frame = startRect;
        
        // compute the end frame
        CGRect pickerRect = CGRectMake(0.0,
                                       screenRect.origin.y + screenRect.size.height - pickerSize.height,
                                       pickerSize.width,
                                       pickerSize.height);
        
        datePickerView.frame = pickerRect;
    } 
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == 104)
        self.tripSummary.distance = [textField.text doubleValue];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case 101:
            self.tripSummary.startAddress = textField.text;
            break;
        case 103:
            self.tripSummary.endAdress = textField.text;
            break;
        case 104:
            self.tripSummary.distance = [textField.text doubleValue];
            break;
            
        default:
            break;
    }
    [textField resignFirstResponder];
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)isValidInput
{
    return  self.tripSummary.startTime && self.tripSummary.endTime && self.tripSummary.startAddress && self.tripSummary.endAdress && (self.tripSummary.distance > 0);
}


-(void)onDatePicker:(id)sender {
    
    UIDatePicker *datePicker = (UIDatePicker *)sender;
    NSIndexPath *indexPath = nil;
    NSDateFormatter *dateFormmater = [NSDateFormatter dateWriter];
    
    if (datePicker.tag == 1100) {
        self.tripSummary.startTime = datePicker.date;
        indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    } else {
        self.tripSummary.endTime = datePicker.date;
        indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    }
    ManualMileageTableViewCell *cell = (ManualMileageTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    [dateFormmater setDateFormat:@"yyyy-MM-dd HH:mm"];
    cell.textField.text = [dateFormmater stringFromDate:datePicker.date];
}


@end
