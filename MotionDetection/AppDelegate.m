
#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Constants.h"
#import <Parse/Parse.h>
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import <FBSDKCoreKit/FBSDKApplicationDelegate.h>
#import "Trip.h"
#import "TripSummary.h"
#import "Driver.h"
#import "Expense.h"
#import "Tag.h"
#import "SOMotionDetector.h"
#import "SOStepDetector.h"
#import "LocationShareModel.h"
#import "ConfigureManager.h"
#import "AAPLStyleUtilities.h"
#import "Car.h"
#import "Project.h"
#import "AppManager.h"
#import "NSDateFormatter+ThreadSafe.h"
#import "DemoManager.h"
#import "StoreObserver.h"
#import "Purchase.h"
#import "UVConfig.h"
#import "UserVoice.h"
#import "TutorialViewController.h"
@interface AppDelegate()<UITabBarControllerDelegate,SOMotionDetectorDelegate, CLLocationManagerDelegate>{
    BOOL shouldTracking;
    BOOL _isBackgroundMode;
    BOOL _deferringUpdates;
 }
@property (nonatomic, strong) Trip *record;
@property (nonatomic, strong) PFGeoPoint *globalLocation;
@property (nonatomic, strong)CLLocation *userLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong)CMMotionActivityManager  *motionActivitiyManager;
@property (nonatomic, strong) UIAlertView *alertView;

@end


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Trip registerSubclass];
    [TripSummary registerSubclass];
    [Driver registerSubclass];
    [Expense registerSubclass];
    [Tag registerSubclass];
    [Car registerSubclass];
    [Project registerSubclass];
    [Purchase registerSubclass];
    
    [GMSServices provideAPIKey:kAPIKey];
    [Parse setApplicationId:kParseApplicationId clientKey:KParseClientKey];
    
    
    [PFFacebookUtils initializeFacebookWithApplicationLaunchOptions:launchOptions];
    
    [PFUser enableRevocableSessionInBackground];
    
    if (![PFUser currentUser]) {
        [PFUser enableAutomaticUser];
        [[PFUser currentUser] saveInBackground];
    }
    PFACL *defaultACL = [PFACL ACL];
    
    // If you would like all objects to be private by default, remove this line.
    [defaultACL setPublicReadAccess:YES];
    
    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
    
    [[ConfigureManager sharedManager] fetchConfigIfNeeded];
    
    [self setupAppearance];
    
    [[AppManager sharedManager] registerNSUserDefault];
    
    if ([[AppManager sharedManager] getShouldShowTutorial]){
        [self presentTutorial];
    } else {
        [self presentMainViewController];
    }
    
    return YES;
}

//http://stackoverflow.com/questions/19042894/periodic-ios-background-location-updates/19085518#19085518
- (void)applicationWillResignActive:(UIApplication *)application
{
    _isBackgroundMode = YES;
    
  [_locationManager stopUpdatingLocation];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    _locationManager.pausesLocationUpdatesAutomatically = NO;
    if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        [_locationManager requestAlwaysAuthorization];
    [_locationManager startUpdatingLocation];
    
    [[SOMotionDetector sharedInstance] startDetection];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [SOMotionDetector sharedInstance].useM7IfAvailable = YES;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
    [self presentAlertController:NSLocalizedString(@"Warning", @"") message:NSLocalizedString(@"We won't be able to track your mileage if this app is killed", @"")];
}

-(void)presentMainViewController {
    [[DemoManager sharedManager] registerNSUserDefault];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.tabBarController = (UITabBarController *)[mainStoryboard instantiateInitialViewController];
    self.tabBarController.delegate = self;
    self.window.rootViewController = self.tabBarController;
    //We have to make sure that the Background App Refresh is enable for the Location updates to work in the background.
    if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied){
        
        [self presentAlertController:NSLocalizedString(@"Warning", @"") message:NSLocalizedString(@"The app doesn't work without the Background App Refresh enabled. To turn it on, go to Settings > General > Background App Refresh", @"")];
        
    }else if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted){
        
        [self presentAlertController:NSLocalizedString(@"Warning", @"") message:NSLocalizedString(@"The functions of this app are limited because the Background App Refresh is disable.", @"")];
    } else{
        
        [self initLocationManager];
        
        [[SOMotionDetector sharedInstance] startDetection];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            [SOMotionDetector sharedInstance].useM7IfAvailable = YES; //Use M7 chip if available, otherwise use lib's algorithm
        }
    }
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:[StoreObserver sharedInstance]];
    
    [self checkForPlan];
    
    // Set this up once when your application launches
    UVConfig *config = [UVConfig configWithSite:@"getdeductibleapp.uservoice.com"];
    config.forumId = 304068;
    [UserVoice initialize:config];
    
//    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
//        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
//                                                        UIUserNotificationTypeBadge |
//                                                        UIUserNotificationTypeSound);
//        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
//                                                                                 categories:nil];
//        [application registerUserNotificationSettings:settings];
//        [application registerForRemoteNotifications];
//    }
    
}


#pragma mark Push Notifications

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
    
    [PFPush subscribeToChannelInBackground:@"" block:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
//            NSLog(@"ParseStarterProject successfully subscribed to push notifications on the broadcast channel.");
        } else {
//            NSLog(@"ParseStarterProject failed to subscribe to push notifications on the broadcast channel.");
        }
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code == 3010) {
//        NSLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
//        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
    
    if (application.applicationState == UIApplicationStateInactive) {
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    }
}

///////////////////////////////////////////////////////////
// Uncomment this method if you want to use Push Notifications with Background App Refresh
///////////////////////////////////////////////////////////
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    if (application.applicationState == UIApplicationStateInactive) {
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    }
}

#pragma mark Facebook SDK Integration

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}


#pragma mark - TabBarController Delegate
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    UINavigationController *navViewController = (UINavigationController *)viewController;
    if ([navViewController.title isEqualToString:@"Account"]) {
        if (([PFAnonymousUtils isLinkedWithUser:[PFUser currentUser]])) {
            [self presentLoginSignUp];
        }
    }
}

-(void)presentLoginSignUp
{
    UIStoryboard *dispatchStoryboard = [UIStoryboard storyboardWithName:@"LoginSignup" bundle:nil];
    UINavigationController *navController = (UINavigationController *)[dispatchStoryboard instantiateInitialViewController];
    [self.window.rootViewController presentViewController:navController animated:YES completion:nil];
    
}

-(void)trackLocation:(CLLocation *)location
{
   
    if (location) {
        self.globalLocation = [PFGeoPoint geoPointWithLocation:location];
        if (shouldTracking) {
            if (!self.record) {
                self.record = [Trip object];
                self.record.location = self.globalLocation;
                self.record.timestamp = [NSDate date];
                self.record.status = DRIVING_STARTED;
//                __weak typeof(self) weakSelf = self;
                [self.record saveInBackgroundWithBlock:^(BOOL success, NSError *error){
                    if (!error) {
//                        [weakSelf scheduleLocanotification:@"Trip started."];
                    }
                }];
            }
        }
    } else {
        //todo: we need to get the first valid location
    }
    
}

#pragma mark - helper
-(void)postMotionChanged {
    if ([self shouldRecordAsStopped]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kMotionStatusChanged object:nil];
        [self shouldRecordAsStopped];
    }
}


-(BOOL)shouldRecordAsStopped {
    if (self.record) {
        self.globalLocation = [PFGeoPoint geoPointWithLocation:self.userLocation];
        double distance = [self.record.location distanceInKilometersTo:self.globalLocation]*1000.0;
        return (distance > DISTANCE_THRESHOLD);
    }
    return NO;
}



-(void)onMotionStatusChanged:(NSNotification *)notif {
    shouldTracking = NO;
    if (self.record) {
        if (self.record.status == DRIVING_STARTED) {
            __block Trip *endPoint = [Trip object];
            endPoint.status = DRIVING_STOPPED;
            endPoint.timestamp = [NSDate date];
            endPoint.location = self.globalLocation;
            __weak typeof(self) weakSelf = self;
            [endPoint saveInBackgroundWithBlock:^(BOOL success, NSError *error){
                if (!error) {
                    TripSummary *tripSummary = [TripSummary object];
                    [tripSummary initWithStartRecord:weakSelf.record end:endPoint];
                    [tripSummary saveInBackgroundWithBlock:^(BOOL success, NSError *error){
                        if (!error) {
                            weakSelf.record = nil;
                            [weakSelf.self scheduleLocanotification:@"Trip finished."];
                        }
                    }];
                    
                }
            }];
            
        }
    }
}

#pragma mark - Notification
-(void)scheduleLocanotification:(NSString *)message{
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    localNotif.alertAction = NSLocalizedString(@"View Details", nil);
    localNotif.alertTitle = @"message";
    localNotif.alertBody = message;
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    NSInteger  currentCount = [UIApplication sharedApplication].applicationIconBadgeNumber;
    localNotif.applicationIconBadgeNumber = currentCount+1;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
    
}

#pragma mark - check location & background status
-(void) initLocationManager
{
    // Create the manager object
    self.locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        [_locationManager requestAlwaysAuthorization];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    [_locationManager startUpdatingLocation];
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //  store data
    CLLocation* location = [locations lastObject];
//    self.userLocation = location;
    if (location) {
        NSDate* eventDate = location.timestamp;
        NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
        if (fabs(howRecent) < 15.0) {
            self.userLocation = location;
        }

    }
    //tell the centralManager that you want to deferred this updatedLocation
    [self trackTrip];
    
    if (_isBackgroundMode && !_deferringUpdates)
    {
        _deferringUpdates = YES;
        [self.locationManager allowDeferredLocationUpdatesUntilTraveled:CLLocationDistanceMax timeout:10];
    }
}

-(void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager
{
    
}

-(void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    
}

- (void) locationManager:(CLLocationManager *)manager didFinishDeferredUpdatesWithError:(NSError *)error {
    
    _deferringUpdates = NO;
    
    //do something
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
    
    
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusAuthorizedAlways:
            if ([manager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [manager requestAlwaysAuthorization];
            }
            [manager startUpdatingLocation];
            break;
        case kCLAuthorizationStatusDenied:
        {{
            [self presentAlertController:NSLocalizedString(@"Warning", @"") message:NSLocalizedString(@"We can’t access your current location.\n\nTo view nearby farmers' markets at your current location, turn on access in the Settings app under Location Services.",@"")];
        }}
            break;
        case kCLAuthorizationStatusNotDetermined:
            break;
        case kCLAuthorizationStatusRestricted:
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            break;
            
    }
}

-(void)setupAppearance
{
    [[UINavigationBar appearance] setTintColor:[AAPLStyleUtilities foregroundColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [AAPLStyleUtilities foregroundColor],NSFontAttributeName:[AAPLStyleUtilities largeFont]}];
  
    [[UIBarButtonItem appearance] setTintColor:[AAPLStyleUtilities foregroundColor]];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [AAPLStyleUtilities foregroundColor],NSFontAttributeName:[AAPLStyleUtilities standardFont]} forState:UIControlStateNormal];
    
    [[UITabBar appearance] setTintColor:[AAPLStyleUtilities foregroundColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName:[AAPLStyleUtilities smallFont]} forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[AAPLStyleUtilities foregroundColor],NSFontAttributeName:[AAPLStyleUtilities smallFont]} forState:UIControlStateHighlighted];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName:[AAPLStyleUtilities standardFont]} forState:UIControlStateNormal];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[AAPLStyleUtilities foregroundColor],NSFontAttributeName:[AAPLStyleUtilities standardFont]} forState:UIControlStateHighlighted];
    
    [[UISegmentedControl appearance] setTintColor:[AAPLStyleUtilities foregroundColor]];
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControl.backgroundColor = [UIColor clearColor];
    
}

-(void)presentAlertController:(NSString *)title message:(NSString *)msg{
    if (!_alertView) {
        _alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil, nil];
        [_alertView show];
    }
}



#pragma mark - unused

-(NSString *)filePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentTXTPath = [documentsDirectory stringByAppendingPathComponent:@"Notes.txt"];
    return documentTXTPath;
}
//
-(NSString *)activityPath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentTXTPath = [documentsDirectory stringByAppendingPathComponent:@"Activities.txt"];
    return documentTXTPath;
}

-(void)saveActivity:(NSString*) activity{
    NSString *documentTXTPath = [self activityPath];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error;
    NSDateFormatter *dateformatter = [NSDateFormatter dateWriter];
    NSString *content = [NSString stringWithFormat:@"driving type: %@ %@",activity, [dateformatter stringFromDate:[NSDate date]]];
    
    if(![fileManager fileExistsAtPath:documentTXTPath])
    {
        [content writeToFile:documentTXTPath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    }
    else
    {
        NSFileHandle *myHandle = [NSFileHandle fileHandleForWritingAtPath:documentTXTPath];
        [myHandle seekToEndOfFile];
        [myHandle writeData:[content dataUsingEncoding:NSUTF8StringEncoding]];
    }
}
-(void)trackAsStopped{
    if ([self shouldRecordAsStopped]) {
        [self onMotionStatusChanged:nil];
    }
}


#pragma mark - track trip
-(void)trackTrip {
    [SOMotionDetector sharedInstance].motionTypeChangedBlock = ^(SOMotionType motionType) {
        if (motionType == MotionTypeAutomotive) {
            shouldTracking = YES;
            [self trackLocation:self.userLocation];
        } else if ((motionType == MotionTypeRunning || motionType ==MotionTypeWalking) && (motionType!=MotionTypeAutomotive)){
            // if the device with M7 chip, we should be able to handle playing while driving
            [self trackAsStopped];
        } else if (motionType == MotionTypeAutomotive && motionType == MotionTypeNotMoving){
            // we are in traffic light
            
        }
    };
}


#pragma mark - check for plan
-(void)checkForPlan {
    if (![PFAnonymousUtils isLinkedWithUser:[Driver currentUser]]){
        PFQuery *query = [Purchase basicQuery];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error){
            if (!error) {
                if (object) {
                    Purchase *lastPurchase = (Purchase *)object;
                    [[AppManager sharedManager] setPurchase:lastPurchase];
                    
                    if ([lastPurchase isPlanExpired] ) {
                        [[AppManager sharedManager] setIsValidPlan:NO];
                    } else {
                        [[AppManager sharedManager] setIsValidPlan:YES];
                    }
                } else {
                   //need to handle others
                }
            } else {
                NSLog(@"error :%@",[error localizedDescription]);
            }
        }];
    }
}

#pragma makr - tutorial
-(void)presentTutorial{
    if ([[AppManager sharedManager] getShouldShowTutorial]){
        UIStoryboard *demoStoryboard = [UIStoryboard storyboardWithName:@"Tutorial" bundle:nil];
        TutorialViewController *viewController = [demoStoryboard instantiateInitialViewController];
        __weak typeof(self) weakSelf = self;
        viewController.shoulFinishBlock = ^(TutorialViewController *viewController){
            [[AppManager sharedManager] setShouldShowTutorial:NO];
            [weakSelf presentMainViewController];
        };
        self.window.rootViewController = viewController;
    }
}

@end
