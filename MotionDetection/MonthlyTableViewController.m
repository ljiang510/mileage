//
//  MonthlyTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 7/2/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "MonthlyTableViewController.h"
#import "NSCalendar+ThreadSafe.h"
#import "AAPLStyleUtilities.h"
#import "JBLineChartViewController.h"
#import "DataProcessingManager.h"
@interface MonthlyTableViewController ()
@property (nonatomic, strong) NSArray *dataByMonth;
@property (nonatomic, strong) NSArray *effetiveMonths;
@end

@implementation MonthlyTableViewController

-(void)setData:(NSArray *)data
{
    if (_data != data) {
        _data = data;
    }
}

-(void)setIsForMileage:(BOOL)isForMileage
{
    _isForMileage = isForMileage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateUI];
}

-(void)updateUI{
    NSInteger currentMonthInNumber = [[NSCalendar safeCalendar] monthForToday];
    if (self.isForMileage) {
        NSArray *milesPerMonth = [[DataProcessingManager sharedManager] milesPerMonth:self.data];
        self.dataByMonth = [milesPerMonth subarrayWithRange:NSMakeRange(0, currentMonthInNumber)];
    } else {
        NSArray *expensePerMonth = [[DataProcessingManager sharedManager] expensesPerMonth:self.data];
        self.dataByMonth = [expensePerMonth subarrayWithRange:NSMakeRange(0, currentMonthInNumber)];
    }
    
    
    NSArray *allMonths = @[@"January",@"Feburary",@"March",@"April",@"May",@"June",@"July",@"August",@"September", @"Octomber",@"Novemember",@"December"];
    self.effetiveMonths = [allMonths subarrayWithRange:NSMakeRange(0, currentMonthInNumber)];
    
    self.clearsSelectionOnViewWillAppear = YES;
    
    self.title = self.isForMileage?NSLocalizedString(@"Mileage", @""):NSLocalizedString(@"Expense", @"");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.effetiveMonths count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MonthCell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = self.effetiveMonths[[self.effetiveMonths count]-1- indexPath.row];
    
    cell.textLabel.textColor = [AAPLStyleUtilities foregroundColor];
    cell.textLabel.font = [AAPLStyleUtilities standardFont];

    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *data = self.dataByMonth[[self.dataByMonth count]-1- indexPath.row];
    JBLineChartViewController *lineChartController = [[JBLineChartViewController alloc] initWithRawData:data isForMileage:self.isForMileage month:self.effetiveMonths[[self.effetiveMonths count]-1- indexPath.row]];
    [self.navigationController pushViewController:lineChartController animated:YES];
}
@end
