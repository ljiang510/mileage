//
//  ConfigureManager.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/2/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "ConfigureManager.h"
#import <Parse/PFConfig.h>
#import "Expense.h"
#import "TripSummary.h"
#import "NSCalendar+ThreadSafe.h"
@interface ConfigureManager ()

@property (nonatomic, strong) PFConfig *config;
@property (nonatomic, strong) NSDate *configLastFetchedDate;

@end
@implementation ConfigureManager
@synthesize config, configLastFetchedDate;


+ (instancetype)sharedManager{
    
    static ConfigureManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(fetchConfigIfNeeded)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:nil];
    }
    return self;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)fetchConfigIfNeeded{
    static const NSTimeInterval configRefreshInterval = 7*24*60.0 * 60.0; // 1 hour
    
    if (self.config == nil ||
        self.configLastFetchedDate == nil ||
        [self.configLastFetchedDate timeIntervalSinceNow] * -1.0 > configRefreshInterval) {
        // Set the config to the cached version and start fetching new config
        self.config = [PFConfig currentConfig];
        
        // Set the date to current and use it as a flag that config fetch is in progress
        self.configLastFetchedDate = [NSDate date];
        
        [PFConfig getConfigInBackgroundWithBlock:^(PFConfig *mConfig, NSError *error) {
            if (error == nil) {
                // Yay, config was fetched
                self.config = mConfig;
                self.configLastFetchedDate = [NSDate date];
            } else {
                // Remove the flag to indicate that we should refetch the config once again
                self.configLastFetchedDate = nil;
            }
        }];
    }
}

-(NSString *)appStoreLink{
    return self.config[@"appStoreLink"]?self.config[@"appStoreLink"]:@"";
}
-(NSString *)website{
    return self.config[@"website"]?self.config[@"website"]:@"";
}
-(NSString *)helpLink{
    return self.config[@"helpLink"]?self.config[@"helpLink"]:@"";
}

-(NSString *)faqLink{
    return self.config[@"faqLink"]?self.config[@"faqLink"]:@"";
}
-(NSString *)contactEmail{
    return self.config[@"contactEmail"]?self.config[@"contactEmail"]:@"";
}

-(NSString *)emailNote{
    return self.config[@"emailNote"]?self.config[@"emailNote"]:@"";

}



@end
