//
//  MeTableViewController.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/8/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripSummary.h"
@class Project;
@class Car;
@class MeTableViewController;

typedef void (^MeViewControllerDidFinish)(MeTableViewController *viewController, Car *car, Project *project);
typedef void (^MeViewControllerDidCancel)(MeTableViewController *viewController);

@interface MeTableViewController : UITableViewController
@property (nonatomic, copy) MeViewControllerDidCancel cancelBlock;
@property (nonatomic, copy) MeViewControllerDidFinish finishBlock;
@property (nonatomic, strong) TripSummary *tSummary;
@end
