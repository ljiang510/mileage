//
//  NSNumberFormatter+ThreadSafe.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/2/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumberFormatter (ThreadSafe)

+(NSNumberFormatter *)currencyFormatter;
@end
