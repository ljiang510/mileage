//
//  OverviewTableViewCell.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/3/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OverviewTableViewCell : UITableViewCell

-(void)setContent:(NSString *)content num:(NSString *)numString unit:(NSString *)unitString;


@end
