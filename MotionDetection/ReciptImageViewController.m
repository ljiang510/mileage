//
//  ReciptImageViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/2/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "ReciptImageViewController.h"
#import <ParseUI/PFImageView.h>
#import "Expense.h"
@interface ReciptImageViewController ()
@property(nonatomic, weak) IBOutlet PFImageView *imageView;
@end

@implementation ReciptImageViewController

-(void)setExpense:(Expense *)expense
{
    if (_expense != expense) {
        _expense = expense;
        [self updateUI];
    }
    
}

-(void)updateUI
{
    self.imageView.frame =  [UIScreen mainScreen].applicationFrame;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.backgroundColor = [UIColor blackColor];
    self.imageView.file = self.expense.fullsizeImage;
    [self.imageView loadInBackground];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateUI];
    self.title = NSLocalizedString(@"Receipt", @"Receipt");
}


-(void)onDone:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
