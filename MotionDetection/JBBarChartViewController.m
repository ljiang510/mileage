//
//  JBBarChartViewController.m
//  JBChartViewDemo
//
//  Created by Terry Worona on 11/5/13.
//  Copyright (c) 2013 Jawbone. All rights reserved.
//

#import "JBBarChartViewController.h"

// Views
#import "JBBarChartView.h"
#import "JBChartHeaderView.h"
#import "JBBarChartFooterView.h"
#import "JBChartInformationView.h"
#import "TripSummary.h"
#import "Expense.h"
#import "NSDateFormatter+ThreadSafe.h"
#import "NSCalendar+ThreadSafe.h"
#import "DataProcessingManager.h"
#import "NSNumberFormatter+ThreadSafe.h"
#import "SVProgressHud.h"
#import "ConfigureManager.h"
#import "Driver.h"
@import MessageUI.MFMailComposeViewController;

// Numerics
CGFloat const kJBBarChartViewControllerChartHeight = 250.0f;
CGFloat const kJBBarChartViewControllerChartPadding = 10.0f;
CGFloat const kJBBarChartViewControllerChartHeaderHeight = 80.0f;
CGFloat const kJBBarChartViewControllerChartHeaderPadding = 20.0f;
CGFloat const kJBBarChartViewControllerChartFooterHeight = 25.0f;
CGFloat const kJBBarChartViewControllerChartFooterPadding = 5.0f;
CGFloat const kJBBarChartViewControllerBarPadding = 1.0f;
NSInteger const kJBBarChartViewControllerNumBars = 7;
NSInteger const kJBBarChartViewControllerMaxBarHeight = 10;
NSInteger const kJBBarChartViewControllerMinBarHeight = 5;

// Strings
NSString * const kJBBarChartViewControllerNavButtonViewKey = @"view";

@interface JBBarChartViewController () <JBBarChartViewDelegate, JBBarChartViewDataSource, MFMailComposeViewControllerDelegate>
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, assign) BOOL isForMileage;

@property (nonatomic, strong) JBBarChartView *barChartView;
@property (nonatomic, strong) JBChartInformationView *informationView;
@property (nonatomic, strong) NSArray *chartData;
//@property (nonatomic, strong) NSArray *monthlySymbols;
@property (nonatomic, strong) NSArray *weeklySymbols;


// Buttons
//- (void)chartToggleButtonPressed:(id)sender;

// Data
//- (void)initFakeData;

@end

@implementation JBBarChartViewController

#pragma mark - Alloc/Init
-(instancetype)initWithRawData:(NSArray *)data isFor:(BOOL)isForMileage{
    
    self = [super init];
    if (self)
    {
        _data = data;
        _isForMileage = isForMileage;
        
        [self initData];
    }
    return self;
}


- (void)dealloc
{
    _barChartView.delegate = nil;
    _barChartView.dataSource = nil;
}

#pragma mark - Date
-(void)initData {
    if (self.isForMileage) {
        _chartData = [[DataProcessingManager sharedManager] processWeeklyMileageData:self.data];
        self.title = @"Weekly Mileage";
    } else {
        _chartData = [[DataProcessingManager sharedManager] processWeeklyExpenseData:self.data];
        self.title = @"Weekly Expense";
    }
    _weeklySymbols = [[NSDateFormatter dateWriter] shortWeekdaySymbols];
}

- (void)initFakeData
{
    NSMutableArray *mutableChartData = [NSMutableArray array];
    for (int i=0; i<kJBBarChartViewControllerNumBars; i++)
    {
        NSInteger delta = (kJBBarChartViewControllerNumBars - labs((kJBBarChartViewControllerNumBars - i) - i)) + 2;
        [mutableChartData addObject:[NSNumber numberWithFloat:MAX((delta * kJBBarChartViewControllerMinBarHeight), arc4random() % (delta * kJBBarChartViewControllerMaxBarHeight))]];

    }
    _chartData = [NSArray arrayWithArray:mutableChartData];
    
//    _weeklySymbols = [[[NSDateFormatter alloc] init] shortWeekdaySymbols];
}

#pragma mark - View Lifecycle



- (void)loadView
{
    [super loadView];
    
    
    self.view.backgroundColor = kJBColorBarChartControllerBackground;
    self.navigationItem.rightBarButtonItem = [self chartToggleButtonWithTarget:self action:@selector(chartToggleButtonPressed:)];

    self.barChartView = [[JBBarChartView alloc] init];
    self.barChartView.frame = CGRectMake(kJBBarChartViewControllerChartPadding, kJBBarChartViewControllerChartPadding, self.view.bounds.size.width - (kJBBarChartViewControllerChartPadding * 2), kJBBarChartViewControllerChartHeight);
    self.barChartView.delegate = self;
    self.barChartView.dataSource = self;
    self.barChartView.headerPadding = kJBBarChartViewControllerChartHeaderPadding;
    self.barChartView.minimumValue = 0.0f;
    self.barChartView.inverted = NO;
    self.barChartView.backgroundColor = kJBColorBarChartBackground;
    
    JBChartHeaderView *headerView = [[JBChartHeaderView alloc] initWithFrame:CGRectMake(kJBBarChartViewControllerChartPadding, ceil(self.view.bounds.size.height * 0.5) - ceil(kJBBarChartViewControllerChartHeaderHeight * 0.5), self.view.bounds.size.width - (kJBBarChartViewControllerChartPadding * 2), kJBBarChartViewControllerChartHeaderHeight)];
    headerView.titleLabel.text = [@"Daily mileage" uppercaseString];
    if (self.isForMileage) {
        headerView.titleLabel.text = [@"Daily expense" uppercaseString];
    }
    headerView.subtitleLabel.text = @"This Week";
    headerView.separatorColor = kJBColorBarChartHeaderSeparatorColor;
    self.barChartView.headerView = headerView;
    
    JBBarChartFooterView *footerView = [[JBBarChartFooterView alloc] initWithFrame:CGRectMake(kJBBarChartViewControllerChartPadding, ceil(self.view.bounds.size.height * 0.5) - ceil(kJBBarChartViewControllerChartFooterHeight * 0.5), self.view.bounds.size.width - (kJBBarChartViewControllerChartPadding * 2), kJBBarChartViewControllerChartFooterHeight)];
    footerView.padding = kJBBarChartViewControllerChartFooterPadding;
    footerView.leftLabel.text = [[self.weeklySymbols firstObject] uppercaseString];
    footerView.leftLabel.textColor = [UIColor whiteColor];
    footerView.rightLabel.text = [[self.weeklySymbols lastObject] uppercaseString];
    footerView.rightLabel.textColor = [UIColor whiteColor];
    self.barChartView.footerView = footerView;
    
    self.informationView = [[JBChartInformationView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, CGRectGetMaxY(self.barChartView.frame), self.view.bounds.size.width, self.view.bounds.size.height - CGRectGetMaxY(self.barChartView.frame) - CGRectGetMaxY(self.navigationController.navigationBar.frame))];
    [self.view addSubview:self.informationView];

    [self.view addSubview:self.barChartView];
    [self.barChartView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.barChartView setState:JBChartViewStateExpanded];
}

#pragma mark - JBChartViewDataSource

- (BOOL)shouldExtendSelectionViewIntoHeaderPaddingForChartView:(JBChartView *)chartView
{
    return YES;
}

- (BOOL)shouldExtendSelectionViewIntoFooterPaddingForChartView:(JBChartView *)chartView
{
    return NO;
}

#pragma mark - JBBarChartViewDataSource

- (NSUInteger)numberOfBarsInBarChartView:(JBBarChartView *)barChartView
{
    return kJBBarChartViewControllerNumBars;
}

- (void)barChartView:(JBBarChartView *)barChartView didSelectBarAtIndex:(NSUInteger)index touchPoint:(CGPoint)touchPoint
{
    NSNumber *valueNumber = [self.chartData objectAtIndex:index];
    if (self.isForMileage) {
        [self.informationView setValueText:[NSString stringWithFormat:@"%.1f",[valueNumber doubleValue]] unitText:nil];
        [self.informationView setTitleText:self.weeklySymbols[index]];//NSLocalizedString(@"Total miles on this day",@"")];
    } else {
        [self.informationView setValueText:[[NSNumberFormatter currencyFormatter] stringFromNumber:valueNumber] unitText:nil];
        [self.informationView setTitleText:self.weeklySymbols[index]];//NSLocalizedString(@"Total expense on this day",@"")];
    }
   
    [self.informationView setHidden:NO animated:YES];
    [self setTooltipVisible:YES animated:YES atTouchPoint:touchPoint];
    [self.tooltipView setText:[self.weeklySymbols[index] uppercaseString]];
}

- (void)didDeselectBarChartView:(JBBarChartView *)barChartView
{
    [self.informationView setHidden:YES animated:YES];
    [self setTooltipVisible:NO animated:YES];
}

#pragma mark - JBBarChartViewDelegate

- (CGFloat)barChartView:(JBBarChartView *)barChartView heightForBarViewAtIndex:(NSUInteger)index
{
    return [[self.chartData objectAtIndex:index] floatValue];
}

- (UIColor *)barChartView:(JBBarChartView *)barChartView colorForBarViewAtIndex:(NSUInteger)index
{
    return (index % 2 == 0) ? kJBColorBarChartBarBlue : kJBColorBarChartBarGreen;
}

- (UIColor *)barSelectionColorForBarChartView:(JBBarChartView *)barChartView
{
    return [UIColor whiteColor];
}

- (CGFloat)barPaddingForBarChartView:(JBBarChartView *)barChartView
{
    return kJBBarChartViewControllerBarPadding;
}

#pragma mark - Buttons

- (void)chartToggleButtonPressed:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Export Data"
                                                                   message:@"Do you want to export this week's data"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", @"") style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    
    [alert addAction:noAction];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", @"") style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [self presentMailController];
                                                          }];
    
    [alert addAction:defaultAction];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Overrides

- (JBChartView *)chartView
{
    return self.barChartView;
}

-(void)presentMailController{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    NSArray *toRecipients = [NSArray arrayWithObject:[[Driver currentUser] email]];
    [picker setToRecipients:toRecipients];
    NSString *thisWeek = [[NSCalendar safeCalendar] printFirstDayAndLastDayOfThisWeek];
    [picker setSubject:[NSString stringWithFormat:@"Your %@ report.",thisWeek]]; //] @"Your Deductiable App Weekly Report"];
    [picker setMessageBody:[[ConfigureManager sharedManager] emailNote] isHTML:NO];
    NSString *attchedString = [[DataProcessingManager sharedManager] generateReport:self.data isForMileage:self.isForMileage];
    NSData *textData = [attchedString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *fileName = [NSString stringWithFormat:@"%@_report.csv", thisWeek];
    [picker addAttachmentData:textData mimeType:@"text/plain" fileName:fileName];
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    __weak NSString *message = @"";
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = NSLocalizedString(@"Mail sending canceled",@"");
            break;
        case MFMailComposeResultSaved:
            message= NSLocalizedString(@"Mail saved", @"");
            break;
        case MFMailComposeResultSent:
            message =NSLocalizedString(@"Data sent", @"");
            break;
        case MFMailComposeResultFailed:
            message =NSLocalizedString(@"Mail sending failed", @"");
            break;
        default:
            message = NSLocalizedString(@"Mail not sent", @"");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        [SVProgressHUD showSuccessWithStatus:message];
    }];
}

@end
