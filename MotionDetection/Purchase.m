//
//  Purchase.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/15/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "Purchase.h"
#import "Constants.h"
#import "Driver.h"
@implementation Purchase
@dynamic productID, expirationDate,driver,productName, purchaseID;

+(NSString *)parseClassName
{
    return kPurchase;
}

+(PFQuery *)basicQuery{
    PFQuery *query = [PFQuery queryWithClassName:[self parseClassName]];
    query.cachePolicy = kPFCachePolicyNetworkOnly;
    [query whereKey:@"driver" equalTo:[Driver currentUser]];
    [query orderByDescending:@"createdAt"];
    [query setLimit:1];
    return query;
}

-(BOOL)isPlanExpired
{
    NSDate *today = [NSDate date];
    return [today compare:self.expirationDate] == NSOrderedDescending;
}
@end
