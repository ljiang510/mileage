//
//  NSNumberFormatter+ThreadSafe.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/2/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "NSNumberFormatter+ThreadSafe.h"

@implementation NSNumberFormatter (ThreadSafe)
+(NSNumberFormatter *)currencyFormatter{
    
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSNumberFormatter *numberReader = [dictionary objectForKey:@"SCNumberReader"];
    if (!numberReader)
    {
        numberReader = [[NSNumberFormatter alloc] init] ;
        [numberReader setNumberStyle: NSNumberFormatterCurrencyStyle];
        [dictionary setObject:numberReader forKey:@"SCNumberReader"];
    }
    return numberReader;
}
@end
