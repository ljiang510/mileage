//
//  JBBaseViewController.m
//  JBChartViewDemo
//
//  Created by Terry Worona on 11/7/13.
//  Copyright (c) 2013 Jawbone. All rights reserved.
//

#import "JBBaseViewController.h"
#import "Driver.h"
#import "ConfigureManager.h"
#import "SVProgressHud.h"
@import MessageUI.MFMailComposeViewController;

@interface JBBaseViewController ()

@end

@implementation JBBaseViewController

#pragma mark - View Lifecycle

- (void)loadView
{
    [super loadView];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeTop;
    }
    self.view.backgroundColor = [UIColor whiteColor];
//    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:kJBImageIconJawboneLogo]];
}

#pragma mark - Orientation

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Getters

- (UIBarButtonItem *)chartToggleButtonWithTarget:(id)target action:(SEL)action
{
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:kJBImageIconArrow] style:UIBarButtonItemStylePlain target:target action:action];
    return button;
}


//-(void)onToggle:(id)sender {
//    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Export Data"
//                                                                   message:@"Do you want to export this week's data"
//                                                            preferredStyle:UIAlertControllerStyleActionSheet];
//    UIAlertAction* noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No", @"") style:UIAlertActionStyleDefault
//                                                     handler:^(UIAlertAction * action) {}];
//    
//    [alert addAction:noAction];
//    
//    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", @"") style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * action) {
//                                                              [self presentMailController];
//                                                          }];
//    
//    [alert addAction:defaultAction];
//    
//    
//    [self presentViewController:alert animated:YES completion:nil];
//
//}

#pragma mark -
//-(void)presentMailController{
//    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
//    picker.mailComposeDelegate = self;
//    NSArray *toRecipients = [NSArray arrayWithObject:[[Driver currentUser] email]];
//    [picker setToRecipients:toRecipients];
//    [picker setSubject:@"Your Deductiable App Monthly Report"];
//    [picker setMessageBody:[[ConfigureManager sharedManager] emailNote] isHTML:NO];
//    NSString *attchedString = [[DataProcessingManager sharedManager] generateReport:self.dataSet isForMileage:self.isForMileage];
//    NSData *textData = [attchedString dataUsingEncoding:NSUTF8StringEncoding];
//    [picker addAttachmentData:textData mimeType:@"text/plain" fileName:@"report.csv"];
//    
//    [self presentViewController:picker animated:YES completion:NULL];
//}
//
//- (void)mailComposeController:(MFMailComposeViewController*)controller
//          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
//{
//    __weak NSString *message = @"";
//    switch (result)
//    {
//        case MFMailComposeResultCancelled:
//            message = NSLocalizedString(@"Mail sending canceled",@"");
//            break;
//        case MFMailComposeResultSaved:
//            message= NSLocalizedString(@"Mail saved", @"");
//            break;
//        case MFMailComposeResultSent:
//            message =NSLocalizedString(@"Data sent", @"");
//            break;
//        case MFMailComposeResultFailed:
//            message =NSLocalizedString(@"Mail sending failed", @"");
//            break;
//        default:
//            message = NSLocalizedString(@"Mail not sent", @"");
//            break;
//    }
//    
//    [self dismissViewControllerAnimated:YES completion:^{
//        [SVProgressHUD showSuccessWithStatus:message];
//    }];
//}

@end
