//
//  ProjectListTableViewController.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/18/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Project;
@class ProjectListTableViewController;
typedef void (^ProjectListControllerDidFinish)(ProjectListTableViewController *viewController, Project *project);

@interface ProjectListTableViewController : UITableViewController

@property (nonatomic, copy) ProjectListControllerDidFinish finishBlock;

@property (nonatomic, strong) Project *project;
@end
