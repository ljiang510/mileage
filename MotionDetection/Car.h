//
//  Car.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/8/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Parse/Parse.h>
@class Driver;
@interface Car : PFObject<PFSubclassing>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) Driver *driver;
@property (nonatomic, assign) BOOL isDefault;
-(void)initWithDemoData:(NSDictionary *)demo;
+(PFQuery *)basicQuery;
@end
