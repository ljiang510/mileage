#import "ViewController.h"
#import "SOMotionDetector.h"
#import "SOStepDetector.h"
#import "Trip.h"
#import "AppDelegate.h"
#import "AAPLMatchesViewController.h"
#import "Constants.h"
#import "TripSummary.h"
#define DISTANCE_THRESHOLD 100 //in meter

@interface ViewController ()<SOMotionDetectorDelegate>
{
    int stepCount;
    BOOL isStarted;
}
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (weak, nonatomic) IBOutlet UILabel *stepCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *motionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *isShakingLabel;
@property (nonatomic, strong) Trip *record;
@property (nonatomic, strong) PFGeoPoint *globalLocation;

@end

@implementation ViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(onMotionStatusChanged:) name:kMotionStatusChanged object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    isStarted = false;
   
    __weak ViewController *weakSelf = self;
    [SOMotionDetector sharedInstance].motionTypeChangedBlock = ^(SOMotionType motionType) {
        NSString *type = @"";
        switch (motionType) {
            case MotionTypeNotMoving:
            {
#if DEBUG
                [self postMotionChanged];
#endif
                type = @"Not moving";
                break;
            }
            case MotionTypeWalking:
                [self postMotionChanged];
                type = @"Walking";
                break;
            case MotionTypeRunning:
                [self postMotionChanged];
                type = @"Running";
                break;
            case MotionTypeAutomotive: {
                isStarted = YES;
                type = @"Automotive";
                break;
            }
        }
        
        weakSelf.motionTypeLabel.text = type;
        
    };
    
    
    [SOMotionDetector sharedInstance].locationChangedBlock = ^(CLLocation *location) {
        double currentSpeed = [SOMotionDetector sharedInstance].currentSpeed;
        weakSelf.speedLabel.text = [NSString stringWithFormat:@"%.2f km/h", currentSpeed * 3.6f];
        if ([weakSelf.motionTypeLabel.text isEqualToString:@"Automotive"]){
            [self trackLocation:location];
        }
    };
    
    [SOMotionDetector sharedInstance].accelerationChangedBlock = ^(CMAcceleration acceleration) {
        BOOL isShaking = [SOMotionDetector sharedInstance].isShaking;
        weakSelf.isShakingLabel.text = isShaking ? @"shaking":@"not shaking";
    };
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [SOMotionDetector sharedInstance].useM7IfAvailable = YES; //Use M7 chip if available, otherwise use lib's algorithm
    }
    
    [[SOMotionDetector sharedInstance] startDetection];
    [[SOStepDetector sharedInstance] startDetectionWithUpdateBlock:^(NSError *error) {
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            return;
        }
        
        stepCount++;
        self.stepCountLabel.text = [NSString stringWithFormat:@"Step count: %d", stepCount];
    }];
    
}

-(void)trackLocation:(CLLocation *)location
{
    self.globalLocation = [PFGeoPoint geoPointWithLocation:location];
    if (isStarted) {
        if (!self.record) {
            self.record = [Trip object];
            self.record.location = self.globalLocation;
            self.record.timestamp = [NSDate date];
            self.record.status = DRIVING_STARTED;
            [self.record saveInBackgroundWithBlock:^(BOOL success, NSError *error){
                if (!error) {
                    [self scheduleLocanotification:@"Trip started"];
                }
            }];
        }
    }
}

#pragma mark - helper
-(void)postMotionChanged {
    isStarted = NO;
    if ([self shouldRecordAsStopped]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kMotionStatusChanged object:nil];
    }
}


-(BOOL)shouldRecordAsStopped {
    if (self.record) {
        double distance = [self.record.location distanceInKilometersTo:self.globalLocation]*1000.0;
        return (distance > DISTANCE_THRESHOLD);
    }
    return NO;
}



-(void)onMotionStatusChanged:(NSNotification *)notif {
    isStarted = NO;
    if (self.record) {
        if (self.record.status == DRIVING_STARTED) {
            Trip *endPoint = [Trip object];
            endPoint.status = DRIVING_STOPPED;
            endPoint.timestamp = [NSDate date];
            endPoint.location = self.globalLocation;
            [endPoint saveInBackgroundWithBlock:^(BOOL success, NSError *error){
                if (!error) {
                    TripSummary *tripSummary = [TripSummary object];
                    [tripSummary initWithStartRecord:self.record end:endPoint];
                    [tripSummary saveInBackgroundWithBlock:^(BOOL success, NSError *error){
                        if (!error) {
                            self.record = nil;
                            [self scheduleLocanotification:@"Trip finished"];
                        }
                    }];
                    
                }
            }];
        }
    }
}


#pragma mark - IBAction
-(IBAction)onButton:(id)sender{
    AAPLMatchesViewController *matchesViewController = [[AAPLMatchesViewController alloc] init];
    [self.navigationController pushViewController:matchesViewController animated:YES];
}

#pragma mark - Notification
-(void)scheduleLocanotification:(NSString *)message{
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    localNotif.alertAction = NSLocalizedString(@"View Details", nil);
    localNotif.alertTitle = @"message";
    localNotif.alertBody = message;
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
    
}

@end
