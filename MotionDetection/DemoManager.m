//
//  DemoManager.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/12/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "DemoManager.h"
#import "Constants.h"
#import "Car.h"
#import "Project.h"
#import "Expense.h"
#import "Trip.h"
#import "TripSummary.h"
#import "Tag.h"
#import "NSCalendar+ThreadSafe.h"
@implementation DemoManager
+(instancetype)sharedManager{
    static DemoManager *manager = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        manager = [[self alloc] init];
    });
    
    return manager;
}

-(void)registerNSUserDefault{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{kShouldShowDemo:@(YES)}];
    
}
-(BOOL)isDemo{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kShouldShowDemo] && [PFAnonymousUtils isLinkedWithUser:[PFUser currentUser]];
}
-(void)setShouldDemo:(BOOL)shouldDemo{
    [[NSUserDefaults standardUserDefaults] setBool:shouldDemo forKey:kShouldShowDemo];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSArray *)arraysFromBundle:(NSString *)fileName{
    NSString *path = [[NSBundle mainBundle] pathForResource:
                  fileName ofType:@"plist"];
    return [[NSArray alloc] initWithContentsOfFile:path];
}

-(NSArray *)cars{
    NSArray *dicts = [self arraysFromBundle:@"Cars"];
    NSMutableArray *cs = [NSMutableArray arrayWithCapacity:[dicts count]];
    for (NSDictionary *dict in dicts) {
        Car *car = [Car object];
        [car initWithDemoData:dict];
        [cs addObject:car];
    }
    return [cs copy];
}
-(NSArray *)projects{
    NSArray *dicts = [self arraysFromBundle:@"Projects"];
    NSMutableArray *ps = [NSMutableArray arrayWithCapacity:[dicts count]];
    for (NSDictionary *dict in dicts) {
        Project *project = [Project object];
        [project initWithDemoData:dict];
        [ps addObject:project];
    }
    return [ps copy];
}
-(NSArray *)trips{
    NSArray *dicts = [self arraysFromBundle:@"Trips"];
    NSMutableArray *ts = [NSMutableArray arrayWithCapacity:[dicts count]];
    for (NSDictionary *dict in dicts) {
        Trip *trip = [Trip object];
        [trip initWithDemoData:dict];
        [ts addObject:trip];
    }
    return [ts copy];
}
-(NSArray *)tripSummary{
    NSArray *dicts = [self arraysFromBundle:@"TripSummary"];
    NSMutableArray *tss = [NSMutableArray arrayWithCapacity:[dicts count]];
    int i = 0;
    NSArray *trips = [self trips];
    if ([trips count] == 2*[dicts count]) {
        NSCalendar *calendar = [NSCalendar safeCalendar];
        for (NSDictionary *dict in dicts) {
            TripSummary *tSummary = [TripSummary object];
            [tSummary initWithDemoData:dict];
            
            tSummary.startTime = [calendar generateRandomDateWithinDaysBeforeToday:7];
            if (i%2==0){
                tSummary.startRecord = trips[i];
                tSummary.endRecord = trips[i+1];
            }
            tSummary.car = [self cars][[self randomNumber:[[self cars] count]] ];
            tSummary.project = [self projects][[self randomNumber:[[self projects] count]] ];
            tSummary.tripType = (i%2==0)?TRIP_PERSONAL:TRIP_BUSINESS;
            [tss addObject:tSummary];
            i++;
        }
    }
    return [tss copy];
}

-(NSArray *)expenses{
    NSArray *dicts = [self arraysFromBundle:@"Expense"];
    NSMutableArray *es = [NSMutableArray arrayWithCapacity:[dicts count]];
    NSCalendar *calendar = [NSCalendar safeCalendar];
    for (NSDictionary *dict in dicts) {
        Expense *expense = [Expense object];
        [expense initWithDemoData:dict];
        expense.tag = [self tags][[self randomNumber:[[self tags] count]] ];
        expense.when = [calendar generateRandomDateWithinDaysBeforeToday:5];
        [es addObject:expense];
    }
    return [es copy];
}

-(NSArray *)tags{
    NSArray *dicts = [self arraysFromBundle:@"Tags"];
    NSMutableArray *ts = [NSMutableArray arrayWithCapacity:[dicts count]];
    for (NSDictionary *dict in dicts) {
        Tag *tag = [Tag object];
        [tag initWithDemoData:dict];
        [ts addObject:tag];
    }
    return [ts copy];
}

-(NSInteger)randomNumber:(NSInteger)upperBound
{
    return  0 + arc4random() % (upperBound - 1);
    
}
@end
