//
//  Expense.m
//  Mileage
//
//  Created by Liangjun Jiang on 5/31/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "Expense.h"
#import  <Parse/PFObject+Subclass.h>
#import "Constants.h"
#import "Driver.h"
#import "Tag.h"
#import "Project.h"
#import "NSNumberFormatter+ThreadSafe.h"
#import "NSCalendar+ThreadSafe.h"
#import "NSDateFormatter+ThreadSafe.h"
@implementation Expense
@dynamic when, where, purpose, note, user, expense, tips, businessName, tag, thumbnail, fullsizeImage,printTotal,total, tax, project, isReimbursable;
+(NSString *)parseClassName
{
    return kExpense;
}


+(PFQuery *)basicQuery
{
    PFQuery *query = [PFQuery queryWithClassName:[self parseClassName]];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query whereKey:@"user" equalTo:[Driver currentUser]];
    [query includeKey:@"tag"];
    [query includeKey:@"project"];
    [query orderByDescending:@"createdAt"];
    return query;
    
}

+(PFQuery *)queryExpenseForWeek{
    PFQuery *query = [self basicQuery];
    
    NSCalendar *calendar = [NSCalendar safeCalendar];
    NSDate *today = [NSDate date];
    NSDate *startDateOfThisWeek = [calendar startOfDayThisWeek:[NSDate date]];
    [query whereKey:@"when" greaterThanOrEqualTo:startDateOfThisWeek];
    [query whereKey:@"when" lessThanOrEqualTo:today];
    
    return query;
}
+(PFQuery *)queryForThisMonth{
    PFQuery *query = [self basicQuery];
    
    NSCalendar *calendar = [NSCalendar safeCalendar];
    NSDate *today = [NSDate date];
    NSDate *startDateOfThisMonth = [calendar startOfDayThisMonth];
    [query whereKey:@"when" greaterThanOrEqualTo:startDateOfThisMonth];
    [query whereKey:@"when" lessThanOrEqualTo:today];
    
    return query;
}

+(PFQuery *)queryForThisYear{
    PFQuery *query = [self basicQuery];
    
    NSCalendar *calendar = [NSCalendar safeCalendar];
    NSDate *today = [NSDate date];
    NSDate *firstDateOfThisYear = [calendar firstDayOfThisYear];
    [query whereKey:@"when" greaterThanOrEqualTo:firstDateOfThisYear];
    [query whereKey:@"when" lessThanOrEqualTo:today];
    [query setLimit:2000];
    return query;
    
    
}

+(PFQuery *)queryForLastMonth{
   PFQuery *query = [self basicQuery];
    NSCalendar *calendar = [NSCalendar safeCalendar];
    [query whereKey:@"when" greaterThanOrEqualTo:[calendar startOfDayLastMonth]];
    [query whereKey:@"when" lessThanOrEqualTo:[NSDate date]];
    return query;
    
}

-(NSString *)friendlyWhen{
    NSDateFormatter *dateFormmater = [NSDateFormatter dateWriter];
    [dateFormmater setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [dateFormmater stringFromDate:self.when];
}

-(NSString *)friendlyExpense{
    NSNumberFormatter *numberFormatter = [NSNumberFormatter currencyFormatter];
    return [numberFormatter stringFromNumber:[NSDecimalNumber numberWithDouble:self.expense]];
}
-(NSString *)friendlyTax{
    NSNumberFormatter *numberFormatter = [NSNumberFormatter currencyFormatter];
    return [numberFormatter stringFromNumber:[NSDecimalNumber numberWithDouble:self.tax]];
}
-(NSString *)friendlyPrintTotal{
    NSNumberFormatter *numberFormatter = [NSNumberFormatter currencyFormatter];
    return [numberFormatter stringFromNumber:[NSDecimalNumber numberWithDouble:self.expense + self.tax]];
}

-(NSString *)friendlyTip{
    NSNumberFormatter *numberFormatter = [NSNumberFormatter currencyFormatter];
    return [numberFormatter stringFromNumber:[NSDecimalNumber numberWithDouble:self.tips]];
}

-(NSString *)friendlyTotal{
    NSNumberFormatter *numberFormatter = [NSNumberFormatter currencyFormatter];
    return [numberFormatter stringFromNumber:[NSDecimalNumber numberWithDouble:self.expense + self.tax + self.tips]];
}

-(NSString *)friendlyProject{
    return self.project?self.project.name:@"";
}

-(NSString *)reportString{
    return [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@\n",[self friendlyWhen], self.businessName, self.tag.name, [self friendlyExpense],[self friendlyTax], [self friendlyPrintTotal],[self friendlyTip], [self friendlyTotal], self.note, self.fullsizeImage.url];
}

-(void)initWithDemoData:(NSDictionary *)demo{
    if  (demo){
        self.businessName = demo[@"businessName"];
        self.tips = [demo[@"tips"] doubleValue];
        self.expense = [demo[@"expense"] doubleValue];
        self.tax = [demo[@"tax"] doubleValue];
        self.note = demo[@"note"];
        self.user = [Driver currentUser];
        self.total = self.expense + self.tax + self.tips;
    }
}




@end
