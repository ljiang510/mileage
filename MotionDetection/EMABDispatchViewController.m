//
//  EMABDispatchViewController.m
//  Chapter6
//
//  Created by Liangjun Jiang on 4/18/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "EMABDispatchViewController.h"
#import "Constants.h"
#import "Driver.h"
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import <FBSDKCoreKit/FBSDKGraphRequest.h>
#import "SVProgressHud.h"
#import "UIImageEffects.h"
#import "AppManager.h"
#import "DemoManager.h"
@interface EMABDispatchViewController ()
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic ,weak) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, weak) IBOutlet UIButton *loginButton;

@end

@implementation EMABDispatchViewController

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.activityIndicatorView.hidden = YES;
   self.backgroundImageView.image = [[AppManager sharedManager] effectImage];
   
}


-(IBAction)onFacebookLogin:(id)sender{
    // Set permissions required from the facebook user account
    [self updateIndicator:YES];
    NSArray *permissionsArray = @[@"email",@"public_profile"];
    
    // Login PFUser using Facebook
    __weak typeof(self) weakSelf = self;
//    [PFFacebookUtils linkUserInBackground:[PFUser currentUser] withReadPermissions:permissionsArray block:^(BOOL succeeded, NSError *error){
//        if(succeeded && !error) {
//            //... success
//             [weakSelf obtainFacebookUserInfo:user];
//        }
//        else {
//            if(error.code == kPFErrorFacebookAccountAlreadyLinked) {
//                [PFUser logOutInBackgroundWithBlock:^(NSError *error) {
//                    if(!error) {
//                        [PFFacebookUtils linkUserInBackground:[PFUser currentUser] withReadPermissions:permissionsArray block:^(BOOL succeeded, NSError *error) {
//                            [PFUser becomeInBackground:[[PFUser currentUser] sessionToken] block:^(PFUser *user, NSError *error) {
//                                if(user && !error) {
//                                    // ... success
//                                }
//                                else {
//                                    //... handle become error
//                                }
//                            }];
//                        }];
//                    }
//                    else {}
//                        
//                    }
//        }
//    }];
    [PFFacebookUtils logInInBackgroundWithReadPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        if (!user) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
//            NSLog(@"Uh oh. The user cancelled the Facebook login.");
        } else if (user.isNew) {
            [weakSelf obtainFacebookUserInfo:user];
//            NSLog(@"User signed up and logged in through Facebook!");
        } else {
//            NSLog(@"User logged in through Facebook!");
            [weakSelf obtainFacebookUserInfo:user];
        }
    }];
}

-(void)obtainFacebookUserInfo:(PFUser *)user{
    
    [self updateIndicator:YES];
    __block Driver *fbUser = (Driver *)user;
    
    __weak typeof(self) weakSelf = self;
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        // handle response
        if (!error) {
            // result is a dictionary with the user's Facebook data
            NSDictionary *userData = (NSDictionary *)result;
            
//            NSString *location = userData[@"location"][@"name"];
//            NSString *birthday = userData[@"birthday"];
//            NSString *relationship = userData[@"relationship_status"];
            [fbUser setEmail:userData[@"email"]];
            [fbUser setUsername:userData[@"email"]?userData[@"email"]:userData[@"name"]];
            [fbUser setName:userData[@"name"]];
            if (userData[@"gender"]) {
                [fbUser setGender:userData[@"gender"]];
            }
            NSString *facebookID = userData[@"id"];
            NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
            
            if ([pictureURL absoluteString]) {
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                dispatch_async(queue, ^{
                    NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[pictureURL absoluteString]]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        PFFile *iconFile = [PFFile fileWithName:@"avatar.jpg" data:imageData];
                        [iconFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                            if (!error) {
                                fbUser.photo = iconFile;
                            }
                        }];
                        
                    });
                });
            }
            
            [fbUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    [[DemoManager sharedManager] setShouldDemo:NO];
                    [weakSelf dismissViewControllerAnimated:YES completion:nil];
                }
            }];
        }  if ([[[[error userInfo] objectForKey:@"error"] objectForKey:@"type"]
                isEqualToString: @"OAuthException"]) {
            [weakSelf dismissViewControllerAnimated:YES completion:^{
                [weakSelf showError:@"The facebook session was invalidated"];
            }];
        } else {
            [weakSelf dismissViewControllerAnimated:YES completion:^{
                [weakSelf showError:[error localizedDescription]];
            }];
        }
    }];
}

-(IBAction)onCancel:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)updateIndicator:(BOOL)shouldEnable{
    self.navigationItem.leftBarButtonItem.enabled = !shouldEnable;
    (shouldEnable)?[self.activityIndicatorView startAnimating]:[self.activityIndicatorView stopAnimating];
    self.activityIndicatorView.hidden = !shouldEnable;
    
}

#pragma mark - helper
-(void)showError:(NSString *)message {
    [SVProgressHUD showErrorWithStatus:message];
}
@end
