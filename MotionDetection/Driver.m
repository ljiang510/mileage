//
//  EMABUser.m
//  Chapter7
//
//  Created by Liangjun Jiang on 4/19/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "Driver.h"
#import  <Parse/PFObject+Subclass.h>
@implementation Driver
@dynamic firstName, lastName, name, gender, phone, address1, address2, city, state, zipcode, photo, projects, cars;

+(Driver *)currentUser{
    return (Driver *)[PFUser currentUser];
}
-(BOOL)isShippingAddressCompleted{
    BOOL cont0 = self.address1 && [self.address1 length] > 0;
    BOOL cont1 = self.city && [self.city length] > 0;
    BOOL cont2 = self.state && self.state > 0;
    BOOL cont3 = self.zipcode && self.zipcode > 0;
    return cont0 && cont1 && cont1 && cont2 && cont3;
}
@end
