/*
 Copyright (C) 2014 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 
  The profile card view.
  
 */

#import "AAPLCardView.h"
#import "AAPLStyleUtilities.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Trip.h"

static const CGFloat AAPLCardPhotoWidth = 80.0;
static const CGFloat AAPLCardBorderWidth = 5.0;
static const CGFloat AAPLCardHorizontalPadding = 20.0;
static const CGFloat AAPLCardVerticalPadding = 20.0;
static const CGFloat AAPLCardInterItemHorizontalSpacing = 30.0;
static const CGFloat AAPLCardInterItemVerticalSpacing = 10.0;
static const CGFloat AAPLCardTitleValueSpacing = 0.0;

@interface AAPLCardView ()<GMSMapViewDelegate>

@property (nonatomic) UIView *backgroundView;
@property (nonatomic) UIView *mapHolder;
@property (nonatomic) UILabel *ageTitleLabel;
@property (nonatomic) UILabel *ageValueLabel;
@property (nonatomic) UILabel *hobbiesTitleLabel;
@property (nonatomic) UILabel *hobbiesValueLabel;
@property (nonatomic) UILabel *elevatorPitchTitleLabel;
@property (nonatomic) UILabel *elevatorPitchValueLabel;
@property (nonatomic) NSLayoutConstraint *photoAspectRatioConstraint;
@property (nonatomic) GMSMapView *startView;
@property (nonatomic) GMSMapView *endView;

@end

@implementation AAPLCardView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [AAPLStyleUtilities cardBorderColor];
        
        self.backgroundView = [[UIView alloc] init];
        self.backgroundView.backgroundColor = [AAPLStyleUtilities cardBackgroundColor];
        self.backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.backgroundView];
        
        [self addProfileViews];
        [self addAllConstraints];
    }
    return self;
}

- (void)addProfileViews {
    self.mapHolder = [[UIView alloc] init];
    self.mapHolder.isAccessibilityElement = YES;
    self.mapHolder.accessibilityLabel = NSLocalizedString(@"Trip map", @"Accessibility label for trip map");
    self.mapHolder.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.mapHolder];
    
    self.ageTitleLabel = [AAPLStyleUtilities standardLabel];
    self.ageTitleLabel.text = NSLocalizedString(@"miles", @"Distance of the trip");
    [self addSubview:self.ageTitleLabel];
    
    self.ageValueLabel = [AAPLStyleUtilities detailLabel];
    [self addSubview:self.ageValueLabel];
    
    self.hobbiesTitleLabel = [AAPLStyleUtilities standardLabel];
    self.hobbiesTitleLabel.text = NSLocalizedString(@"origin", @"Origin of this trip");
    [self addSubview:self.hobbiesTitleLabel];
    
    self.hobbiesValueLabel = [AAPLStyleUtilities detailLabel];
    [self addSubview:self.hobbiesValueLabel];
    
    self.elevatorPitchTitleLabel = [AAPLStyleUtilities standardLabel];
    self.elevatorPitchTitleLabel.text = NSLocalizedString(@"destination", @"Destination of this trip");
    [self addSubview:self.elevatorPitchTitleLabel];
    
    self.elevatorPitchValueLabel = [AAPLStyleUtilities detailLabel];
    [self addSubview:self.elevatorPitchValueLabel];
    
    self.accessibilityElements = @[self.mapHolder, self.ageTitleLabel, self.ageValueLabel, self.hobbiesTitleLabel, self.hobbiesValueLabel, self.elevatorPitchTitleLabel, self.elevatorPitchValueLabel];
}

- (void)addAllConstraints {
    NSMutableArray *constraints = [NSMutableArray array];
    
    // Fill the card with the background view (leaving a border around it)
    [constraints addObjectsFromArray:
     @[
       [NSLayoutConstraint constraintWithItem:self.backgroundView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:AAPLCardBorderWidth],
       [NSLayoutConstraint constraintWithItem:self.backgroundView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:AAPLCardBorderWidth],
       [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.backgroundView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:AAPLCardBorderWidth],
       [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.backgroundView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:AAPLCardBorderWidth]
       ]];
    
    // Position the photo
    // The constant for the aspect ratio constraint will be updated once a photo is set
    self.photoAspectRatioConstraint = [NSLayoutConstraint constraintWithItem:self.mapHolder attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:0.0 constant:0.0];
    [constraints addObjectsFromArray:
     @[
       [NSLayoutConstraint constraintWithItem:self.mapHolder attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:AAPLCardHorizontalPadding],
       [NSLayoutConstraint constraintWithItem:self.mapHolder attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:AAPLCardVerticalPadding],
       [NSLayoutConstraint constraintWithItem:self.mapHolder attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:0.0 constant:AAPLCardPhotoWidth],
       [NSLayoutConstraint constraintWithItem:self.mapHolder attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationLessThanOrEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-AAPLCardVerticalPadding],
       self.photoAspectRatioConstraint
       ]];
    
    // Position the age to the right of the photo, with some spacing
    [constraints addObjectsFromArray:
     @[
       [NSLayoutConstraint constraintWithItem:self.ageTitleLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.mapHolder attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:AAPLCardInterItemHorizontalSpacing],
       [NSLayoutConstraint constraintWithItem:self.ageTitleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.mapHolder attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
       [NSLayoutConstraint constraintWithItem:self.ageValueLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.ageTitleLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:AAPLCardTitleValueSpacing],
       [NSLayoutConstraint constraintWithItem:self.ageValueLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.ageTitleLabel attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]
       ]];
    
    [constraints addObjectsFromArray:
     @[
       [NSLayoutConstraint constraintWithItem:self.hobbiesTitleLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.ageTitleLabel attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:AAPLCardInterItemHorizontalSpacing],
       [NSLayoutConstraint constraintWithItem:self.hobbiesTitleLabel attribute:NSLayoutAttributeFirstBaseline relatedBy:NSLayoutRelationEqual toItem:self.ageTitleLabel attribute:NSLayoutAttributeFirstBaseline multiplier:1.0 constant:0.0],
       [NSLayoutConstraint constraintWithItem:self.hobbiesValueLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.ageValueLabel attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:AAPLCardInterItemHorizontalSpacing],
       [NSLayoutConstraint constraintWithItem:self.hobbiesValueLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.hobbiesTitleLabel attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
       [NSLayoutConstraint constraintWithItem:self.hobbiesValueLabel attribute:NSLayoutAttributeFirstBaseline relatedBy:NSLayoutRelationEqual toItem:self.ageValueLabel attribute:NSLayoutAttributeFirstBaseline multiplier:1.0 constant:0.0],
       [NSLayoutConstraint constraintWithItem:self.hobbiesTitleLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationLessThanOrEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-AAPLCardHorizontalPadding],
       [NSLayoutConstraint constraintWithItem:self.hobbiesValueLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationLessThanOrEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-AAPLCardHorizontalPadding]
       ]];
    
    [constraints addObjectsFromArray:
     @[
       [NSLayoutConstraint constraintWithItem:self.elevatorPitchTitleLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.ageTitleLabel attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
       [NSLayoutConstraint constraintWithItem:self.elevatorPitchTitleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.ageValueLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:AAPLCardInterItemVerticalSpacing],
       [NSLayoutConstraint constraintWithItem:self.elevatorPitchTitleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.hobbiesValueLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:AAPLCardInterItemVerticalSpacing],
       [NSLayoutConstraint constraintWithItem:self.elevatorPitchTitleLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-AAPLCardHorizontalPadding],
       [NSLayoutConstraint constraintWithItem:self.elevatorPitchValueLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.elevatorPitchTitleLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:AAPLCardTitleValueSpacing],
       [NSLayoutConstraint constraintWithItem:self.elevatorPitchValueLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.elevatorPitchTitleLabel attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
       [NSLayoutConstraint constraintWithItem:self.elevatorPitchValueLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-AAPLCardHorizontalPadding],
       [NSLayoutConstraint constraintWithItem:self.elevatorPitchValueLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-AAPLCardVerticalPadding]
       ]];
    
    [self addConstraints:constraints];
}

- (void)updateWithTripSummary:(TripSummary *)summary{
    if (summary){
        [summary tripDetailWithBlock:^(TripSummary *tSummary, NSError *error) {
            if (!error) {
                [self updatePhotoConstraint];
                self.ageValueLabel.text = [tSummary friendlyDistance];
                self.hobbiesValueLabel.text = [tSummary friendlyStartAddress];
                Trip *startRecord = tSummary.startRecord;
                
                if (startRecord) {
                    GMSCameraPosition *startPosition =  [GMSCameraPosition cameraWithLatitude:startRecord.location.latitude
                                                                                  longitude:startRecord.location.longitude
                                                                                       zoom:14];
                    if (startPosition) {
                        if (self.startView) {
                            [self.startView removeFromSuperview];
                        }
                        self.startView.camera = startPosition;
                        self.startView = [GMSMapView mapWithFrame:CGRectMake(0.0, 0.0, AAPLCardPhotoWidth, AAPLCardPhotoWidth) camera:startPosition];
                        self.startView.settings.zoomGestures = NO;
                        self.startView.settings.scrollGestures = NO;
                        self.startView.settings.rotateGestures = NO;
                        self.startView.settings.tiltGestures = NO;
                        self.startView.layer.borderColor = [UIColor lightGrayColor].CGColor;
                        self.startView.layer.borderWidth = 1.0;
                        self.startView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
                        UIViewAutoresizingFlexibleHeight |
                        UIViewAutoresizingFlexibleBottomMargin;
                        GMSMarker *dMarker = [[GMSMarker alloc] init];
                        dMarker.icon = [UIImage imageNamed:@"origin_icon"];
                        dMarker.position = CLLocationCoordinate2DMake(startRecord.location.latitude, startRecord.location.longitude);
                        dMarker.map = self.startView;
                        [self.mapHolder addSubview:self.startView];
                    }
                }
                
                Trip *endRecord = tSummary.endRecord;
                self.elevatorPitchValueLabel.text = [NSString stringWithFormat:@"%@", [tSummary friendlyEndAddress]];
                if (endRecord) {
                    GMSCameraPosition *endPosition =  [GMSCameraPosition cameraWithLatitude:endRecord.location.latitude
                                                                                  longitude:endRecord.location.longitude
                                                                                       zoom:14];
                    if (endPosition) {
                        if (self.endView) {
                            [self.endView removeFromSuperview];
                        }
                        
                        self.endView = [GMSMapView mapWithFrame:CGRectMake(0.0, AAPLCardPhotoWidth, AAPLCardPhotoWidth, AAPLCardPhotoWidth) camera:endPosition];
                        self.endView.settings.zoomGestures = NO;
                        self.endView.settings.scrollGestures = NO;
                        self.endView.settings.rotateGestures = NO;
                        self.endView.settings.tiltGestures = NO;
                        self.endView.layer.borderColor = [UIColor lightGrayColor].CGColor;
                        self.endView.layer.borderWidth = 1.0;
                        self.endView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
                        UIViewAutoresizingFlexibleHeight |
                        UIViewAutoresizingFlexibleBottomMargin;
                        GMSMarker *dMarker = [[GMSMarker alloc] init];
                        dMarker.icon = [UIImage imageNamed:@"destination_icon"];
                        dMarker.position = CLLocationCoordinate2DMake(endRecord.location.latitude, endRecord.location.longitude);
                        dMarker.map = self.endView;
                        
                        [self.mapHolder addSubview:self.endView];
                    }
                }
                
            } else {
                NSLog(@"error:%@", [error localizedDescription]);
            }
        }];
    }
}

- (void)updatePhotoConstraint {
    self.photoAspectRatioConstraint.constant = 2*AAPLCardPhotoWidth;
}


+ (GMSCameraPosition *)defaultCamera {
    return [GMSCameraPosition cameraWithLatitude:37.7847
                                       longitude:-122.41
                                            zoom:15];
}


@end
