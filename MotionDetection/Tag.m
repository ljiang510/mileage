//
//  Tag.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/1/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "Tag.h"
#import  <Parse/PFObject+Subclass.h>
#import "Constants.h"
@implementation Tag
@dynamic name;

+(NSString *)parseClassName
{
    return kTag;
}


+(PFQuery *)basicQuery{
    PFQuery *query = [PFQuery queryWithClassName:[self parseClassName]];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query orderByAscending:@"name"];
    return query;
}

-(void)initWithDemoData:(NSDictionary *)demo{
    if (demo){
        self.name = demo[@"name"];
    }
}
@end
