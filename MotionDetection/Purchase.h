//
//  Purchase.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/15/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Parse/Parse.h>
@class Driver;
@interface Purchase : PFObject<PFSubclassing>
@property (nonatomic, strong) Driver *driver;
@property (nonatomic, copy) NSString *productID;
@property (nonatomic, copy) NSString *productName;

@property (nonatomic, strong) NSDate *expirationDate;
@property (nonatomic, copy) NSString *purchaseID;


+(PFQuery *)basicQuery;
-(BOOL)isPlanExpired;
@end
