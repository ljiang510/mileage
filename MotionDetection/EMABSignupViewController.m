//
//  EMABSignupViewController.m
//  Chapter7
//
//  Created by Liangjun Jiang on 4/19/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "EMABSignupViewController.h"
#import "Constants.h"
#import "EMABEmailTextField.h"
#import "EMABPasswordTextField.h"
#import "Driver.h"
#import "SVProgressHud.h"
#import "UIImageEffects.h"
#import "AAPLStyleUtilities.h"
#import "ConfigureManager.h"
#import "AppManager.h"
#import "DemoManager.h"
@interface EMABSignupViewController ()<UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate>
@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *passwordAgainTextField;
@property (nonatomic, weak) IBOutlet UITableView *signupTableView;
@property (nonatomic ,weak) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic ,weak) IBOutlet UIButton *tosButton;

@end

@implementation EMABSignupViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.emailTextField = [[EMABEmailTextField alloc] initWithFrame:CGRectMake(kLeftMargin, kTopMargin, kTextFieldWidth, kTextFieldHeight)];
    self.emailTextField.delegate = self;
    self.passwordTextField = [[EMABPasswordTextField alloc] initWithFrame:CGRectMake(kLeftMargin, kTopMargin, kTextFieldWidth, kTextFieldHeight)];
    self.passwordTextField.delegate = self;
    self.passwordAgainTextField = [[EMABPasswordTextField alloc] initWithFrame:CGRectMake(kLeftMargin, kTopMargin, kTextFieldWidth, kTextFieldHeight)];
    self.passwordAgainTextField.delegate = self;
    [self.passwordAgainTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Password Again",@"") attributes:@{NSFontAttributeName:[AAPLStyleUtilities standardFont],NSForegroundColorAttributeName:[UIColor whiteColor]}]];
    
   
    self.backgroundImageView.image = [[AppManager sharedManager] effectImage];
    
    
    [self.signupTableView setBackgroundView:nil];
    [self.signupTableView setBackgroundColor:[UIColor clearColor]];
    self.signupTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    
    NSDictionary *dict0 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:[AAPLStyleUtilities smallFont],NSForegroundColorAttributeName:[UIColor whiteColor],
                            NSParagraphStyleAttributeName:style}; // Added line
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                            NSFontAttributeName:[AAPLStyleUtilities smallFont], NSForegroundColorAttributeName:[UIColor whiteColor],
                            NSParagraphStyleAttributeName:style}; // Added line
   
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@"By signing up, you agree to our "    attributes:dict0]];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Term of Service."      attributes:dict1]];
    [self.tosButton setAttributedTitle:attString forState:UIControlStateNormal];
    [[self.tosButton titleLabel] setNumberOfLines:0];
    [[self.tosButton titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];

   
}

-(IBAction)onToS:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[ConfigureManager sharedManager] website]]];
}


-(IBAction)onSignup:(id)sender{
    BOOL cont0 = [self.passwordTextField.text length] >= kMinTextLength;
    BOOL cont1 = [self.passwordAgainTextField.text length] >= kMinTextLength;
    BOOL cont2 = [self.passwordTextField.text isEqualToString:self.passwordAgainTextField.text];
    BOOL cont3 = [self.emailTextField.text length] >= kMinTextLength;
    BOOL cont4 = [Constants isValidEmail:self.emailTextField.text];
    
    if (!cont0) {
        [self showWarning:NSLocalizedString(@"Password at least 6 characters.", @"Password at least 6 characters.")];
    }
    if (!cont1) {
        [self showWarning:NSLocalizedString(@"Password at least 6 characters.", @"Password at least 6 characters.")];
    }
    if (!cont2) {
        [self showWarning:NSLocalizedString(@"Passwords have to match.", @"Passwords have to match.")];
    }
    if (!cont3) {
        [self showWarning:NSLocalizedString(@"Email at least 6 characters.", @"Password at least 6 characters.")];
    }
    
    if (!cont4) {
        [self showWarning:NSLocalizedString(@"Doesn't look like a valid email.", @"Doesn't look like a valid email.")];
    }
    
    if (cont0 && cont1 && cont2 && cont3 && cont4) {
        [self.emailTextField resignFirstResponder];
        [self.passwordTextField resignFirstResponder];
        [self.passwordAgainTextField resignFirstResponder];
        Driver *user = (Driver *)[PFUser currentUser];
        user.username = [self.emailTextField text];
        user.email = [self.emailTextField text];
        user.password = [self.passwordTextField text];
        __weak typeof(self) weakSelf = self;
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            if (!error) {
                [[DemoManager sharedManager] setShouldDemo:NO];
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }
        }];
    }
}

#pragma mark - tableview datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"SignupCell" forIndexPath:indexPath];
    
    UITextField *textField = nil;
    switch (indexPath.row) {
        case 0:
            textField = self.emailTextField;
            break;
        case 1:
            textField = self.passwordTextField;
            break;
        case 2:
            textField = self.passwordAgainTextField;
            break;
        default:
            break;
    }
    
    // make sure this textfield's width matches the width of the cell
    CGRect newFrame = textField.frame;
    newFrame.size.width = CGRectGetWidth(cell.contentView.frame) - kLeftMargin*2;
    textField.frame = newFrame;
    
    // if the cell is ever resized, keep the textfield's width to match the cell's width
    textField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [cell.contentView addSubview:textField];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - helper
-(void)showWarning:(NSString *)message {
    [SVProgressHUD showErrorWithStatus:message];
}

#pragma mark - UITextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
