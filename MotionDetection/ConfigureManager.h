//
//  ConfigureManager.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/2/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Parse/Parse.h>

@interface ConfigureManager : NSObject
+ (instancetype)sharedManager;

- (void)fetchConfigIfNeeded;

-(NSString *)appStoreLink;
-(NSString *)website;
-(NSString *)faqLink;

-(NSString *)helpLink;
-(NSString *)contactEmail;
-(NSString *)emailNote;

@end
