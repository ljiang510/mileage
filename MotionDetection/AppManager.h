//
//  AppManager.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/12/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Purchase;
@interface AppManager : NSObject

@property (nonatomic, strong) Purchase *purchase;

+(instancetype)sharedManager;

-(void)registerNSUserDefault;
-(void)setShouldShowTutorial:(BOOL)shouldShow;
-(BOOL)getShouldShowTutorial;
-(void)setAppBadgeNumber:(NSInteger)num;

-(UIImage *)effectImage;

-(void)setIsValidPlan:(BOOL)isValidPlan;
-(BOOL)getIsValidPlan;

-(void)setPlanName:(NSString*)planName;
-(NSString*)getPlanName;
@end
