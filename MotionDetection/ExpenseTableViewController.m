//
//  ExpenseTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 5/31/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "ExpenseTableViewController.h"
#import "Expense.h"
#import "APLExpenseTableViewCell.h"
#import "CarsTableViewCotnroller.h"
#import "ParseErrorHandlingController.h"
#import "ManualExpenseTableViewController.h"
#import "Driver.h"
@interface ExpenseTableViewController ()<UITextFieldDelegate>
@property (nonatomic) NSMutableArray *eventsArray;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *addButton;
@end

@implementation ExpenseTableViewController
-(void)loadData:(id)sender
{
    PFQuery *query = [Expense basicQuery];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if (!error) {
            self.eventsArray = [NSMutableArray arrayWithArray:objects];
            [self.tableView reloadData];
        } else {
             [ParseErrorHandlingController handleParseError:error];
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadData:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    self.eventsArray = [NSMutableArray array];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.eventsArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ExpenseTableViewCell";
    
    APLExpenseTableViewCell *cell = (APLExpenseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.delegate = self;
    
    if ([self.eventsArray count] > 0) {
        Expense *event = (Expense *)self.eventsArray[indexPath.row];
        [cell configureWithEvent:event tag:indexPath.row];
    }
    return cell;
}




#pragma mark - Editing
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Ensure that if the user is editing a name field then the change is committed before deleting a row -- this ensures that changes are made to the correct event object.
        [tableView endEditing:YES];
        
        
        // Update the array and table view.
        [self.eventsArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    self.navigationItem.rightBarButtonItem.enabled = !editing;
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    APLExpenseTableViewCell *cell = (APLExpenseTableViewCell *)sender;
    
    [cell endEditing:YES];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if ([[segue identifier] isEqualToString:@"EditTags"]) {
//        ExpenseTagTableViewCotnroller *tagSelectionController = [segue destinationViewController];
//        tagSelectionController.expense = (self.eventsArray)[indexPath.row];
    } else
        if ([[segue identifier] isEqualToString:@"Detail"]){
        ManualExpenseTableViewController *viewController = [segue destinationViewController];
        viewController.expense = (self.eventsArray)[indexPath.row];
    }
}


#pragma mark - Add an event

/*
 Add an event.
 */
- (IBAction)addEvent:(id)sender
{
    Expense *expense = [Expense object];
    expense.businessName = @"";
    expense.user = [Driver currentUser];
    expense.when = [NSDate date];
    [self.eventsArray insertObject:expense atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    [self setEditing:YES animated:YES];
    APLExpenseTableViewCell *cell = (APLExpenseTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];

    [cell makeNameFieldFirstResponder];
   
}


#pragma mark - Editing text fields

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    CGPoint point = [textField center];
    point = [self.tableView convertPoint:point fromView:textField];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    
    Expense  *event = (self.eventsArray)[indexPath.row];
    if (textField.tag == indexPath.row*100) {
        event.businessName = textField.text;
    } else if (textField.tag == (indexPath.row *100 +1)){
        event.total = [textField.text doubleValue];
    }
    
    UITableViewCell *cell= [self.tableView cellForRowAtIndexPath:indexPath];
    [cell endEditing:YES];
    
//    [event saveInBackgroundWithBlock:^(BOOL success, NSError *error){
//        if (!error) {
//            
//        }
//    }];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    CGPoint point = [textField center];
    point = [self.tableView convertPoint:point fromView:textField];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];
    Expense  *event = (self.eventsArray)[indexPath.row];

    if (textField.tag == (indexPath.row *100 +1)){
        event.total = [textField.text doubleValue];
    }
    
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    textField.enabled = self.editing;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;	
}


@end
