//
//  CustomMapView.h
//  MotionDetection
//
//  Created by Liangjun Jiang on 5/28/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@interface CustomMapView : GMSMapView
@end
