//
//  BusinessCategoryTableViewController.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/3/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ParseUI/PFQueryTableViewController.h>
@class Expense;
@class Tag;
@class BusinessCategoryTableViewController;

typedef void (^ViewControllerDidFinish)(BusinessCategoryTableViewController *viewController, Tag *note);
typedef void (^ViewControllerDidCancel)(BusinessCategoryTableViewController *viewController);
@interface BusinessCategoryTableViewController : PFQueryTableViewController
@property (nonatomic, copy) ViewControllerDidCancel cancelBlock;
@property (nonatomic, copy) ViewControllerDidFinish finishBlock;

@property (nonatomic, strong) Tag *tag;

@end
