//
//  EMABNoteViewController.h
//  Chapter19
//
//  Created by Liangjun Jiang on 4/23/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class EMABNoteViewController;

typedef void (^NoteViewControllerDidFinish)(EMABNoteViewController *viewController, NSString *note);
typedef void (^NoteViewControllerDidCancel)(EMABNoteViewController *viewController);

@interface EMABNoteViewController : UIViewController
@property (nonatomic, copy) NoteViewControllerDidCancel cancelBlock;
@property (nonatomic, copy) NoteViewControllerDidFinish finishBlock;

@property (nonatomic, copy) NSString *note;
@end
