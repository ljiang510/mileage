//
//  TripReviewTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/10/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "TripReviewTableViewController.h"
#import "TripSummary.h"
#import "TripReviewTableViewCell.h"
#import "DemoManager.h"
#import "SVProgressHud.h"
#import "MeTableViewController.h"
@interface TripReviewTableViewController ()
@property (nonatomic, strong) NSMutableArray *trips;
@end

@implementation TripReviewTableViewController

-(void)loadDemoData{
    self.trips = [NSMutableArray arrayWithArray:[[DemoManager sharedManager] tripSummary]];
    [self.tableView reloadData];
}

-(IBAction)loadData:(id)sender
{
    self.trips = [NSMutableArray arrayWithCapacity:0];
    
    __weak typeof(self) weakSelf = self;
    PFQuery *query = [TripSummary queryForTripReview];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if ([weakSelf.refreshControl isRefreshing]) {
            [weakSelf.refreshControl endRefreshing];
        }

        if (!error) {
            weakSelf.trips = [NSMutableArray arrayWithArray:objects];
            [weakSelf.tableView reloadData];
        }
    }];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Trip Review", @"");
    
    if ([[DemoManager sharedManager] isDemo]) {
        [self loadDemoData];
    } else
        [self loadData:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onDone:(id)sender
{
    if ([[DemoManager sharedManager] isDemo]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        __weak typeof(self) weakSelf = self;
    [TripSummary saveAllInBackground:[self.trips copy] block:^(BOOL success, NSError *error){
        if (!error) {
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"", @"Saved")];
            [weakSelf.navigationController popViewControllerAnimated:YES];

        }
    }];
    }
    
}

-(IBAction)onCancel:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.trips count];;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0;
}

- (TripReviewTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TripReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TripReviewCell" forIndexPath:indexPath];
    
    // Configure the cell...
    if ([self.trips count]> 0) {
        TripSummary *tSummary = self.trips[indexPath.row];
        [cell setItem:tSummary];
        cell.tripTypeButton.tag = indexPath.row + 100;

    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    __block TripSummary *tripSummary = self.trips[indexPath.row];
    UIStoryboard *storyboard = [self storyboard];
    MeTableViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MeTableViewController"];
    [viewController setTSummary:tripSummary];
    __weak typeof(self) weakSelf = self;
    viewController.finishBlock = ^(MeTableViewController *viewController, Car *car, Project *project){
        tripSummary.car = car;
        tripSummary.project = project;
        [weakSelf.trips replaceObjectAtIndex:indexPath.row withObject:tripSummary];
        [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
      
    };
    [self.navigationController pushViewController:viewController animated:YES];
}

-(IBAction)onButtonTapped:(id)sender
{
    
    UIButton *button = (UIButton *)sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:button.tag - 100 inSection:0];
    if (indexPath) {
        TripSummary *updatedTrip = self.trips[indexPath.row];
        if (updatedTrip) {
            if (updatedTrip.tripType == TRIP_BUSINESS) {
                updatedTrip.tripType = TRIP_PERSONAL;
            } else if (updatedTrip.tripType == TRIP_PERSONAL)
                updatedTrip.tripType = TRIP_BUSINESS;
            [self.trips replaceObjectAtIndex:indexPath.row withObject:updatedTrip];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
        }
    }
    
}
@end
