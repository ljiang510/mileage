//
//  ExpenseListTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/3/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "ExpenseListTableViewController.h"
#import "Expense.h"
#import "Constants.h"
#import "ExpenseTableViewCell.h"
#import  "ManualExpenseTableViewController.h"
#import "ParseErrorHandlingController.h"
#import "Driver.h"
#import "DemoManager.h"
#import "AAPLStyleUtilities.h"
#import "ExpenseReportCategoryTableViewController.h"
#import "NSDateFormatter+ThreadSafe.h"
@interface ExpenseListTableViewController()
@property (nonatomic, strong) NSArray *objects;
@property (nonatomic, weak) IBOutlet UILabel *hintLabel;
@end

@implementation ExpenseListTableViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([[DemoManager sharedManager] isDemo]) {
        [self loadDemoData];
    } else {
        self.hintLabel.text = [[NSDateFormatter dateWriter] monthSymbolForToday];;
        [self loadData:nil];
    }
}

-(void)loadDemoData{
    self.hintLabel.text = NSLocalizedString(@"Demo data. Please sign up or log in to add your data", @"");
    
    self.navigationItem.title = NSLocalizedString(@"Demo Expense", @"");
    self.objects = [[DemoManager sharedManager] expenses];
    [self.tableView reloadData];
}


-(IBAction)loadData:(id)sender
{
    self.navigationItem.title = NSLocalizedString(@"Expense", @"");
    __weak typeof(self) weakSelf = self;
    PFQuery *query = [self queryForTable];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if ([weakSelf.refreshControl isRefreshing]) {
            [weakSelf.refreshControl endRefreshing];
        }

        if (!error) {
            weakSelf.objects = objects;
            [weakSelf.tableView reloadData];
        } else {
//            NSLog(@"error :%@",[error localizedDescription]);
        }
        
    }];
}


-(void)viewDidLoad{
    [super viewDidLoad];
    self.hintLabel.backgroundColor = [AAPLStyleUtilities foregroundColor];
    self.hintLabel.textColor = [UIColor whiteColor];
    self.hintLabel.font = [AAPLStyleUtilities largeFont];
}

-(PFQuery *)queryForTable
{
    PFQuery *query = [Expense queryForThisMonth];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    return query;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.objects count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(ExpenseTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ExpenseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExpenseListCell"];
    
    [cell setItem:self.objects[indexPath.row]];
    return cell;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowReportCategory"]) {
        ExpenseReportCategoryTableViewController *viewController = [segue destinationViewController];
        
        viewController.expenses = self.objects;
    } else {
    
        Expense *expense;
        if ([segue.identifier isEqualToString:@"Add"]) {
            expense = [Expense object];
            expense.user = [Driver currentUser];
        } else {
            ExpenseTableViewCell *cell = (ExpenseTableViewCell *)sender;
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            expense = (self.objects)[indexPath.row];
            
        }
        ManualExpenseTableViewController *viewController = [segue destinationViewController];
        viewController.expense = expense;
    }
}



@end
