//
//  CustomButton.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/8/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "CustomButton.h"

#import "AAPLStyleUtilities.h"
#import "Constants.h"

static const CGFloat AAPLOverlayCornerRadius = 10.0;
static const CGFloat AAPLButtonVerticalContentInset = 10.0;
static const CGFloat AAPLButtonHorizontalContentInset = 10.0;
//static const CGFloat AAPLOverlayMargin = 20.0;
//static const CGFloat AAPLContentVerticalMargin = 50.0;
//static const CGFloat AAPLContentHorizontalMargin = 30.0;
@implementation CustomButton


-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self  = [super initWithCoder:aDecoder];
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self setTitleColor:[self foregroundColor] forState:UIControlStateNormal];
    self.titleLabel.font = [self largeFont];
    [self setBackgroundImage:[self overlayRoundedRectImage] forState:UIControlStateNormal];
    [self setContentEdgeInsets:UIEdgeInsetsMake(AAPLButtonVerticalContentInset, AAPLButtonHorizontalContentInset, AAPLButtonVerticalContentInset, AAPLButtonHorizontalContentInset)];
    return self;
    
}

-(NSString *)fontName {
    if (UIAccessibilityIsBoldTextEnabled()) {
        return @"Avenir-Medium";
    }
    return @"Avenir-Light";
}

- (UIFont *)standardFont {
    return [UIFont fontWithName:[self fontName] size:14.0];
}

- (UIFont *)largeFont {
    return [UIFont fontWithName:[self fontName] size:18.0];
}

- (UIColor *)foregroundColor {
    return [UIColor colorWithRed:75.0/255 green:35.0/255 blue:106.0/255 alpha:1.0];
}

- (UIColor *)overlayColor {
    if (UIAccessibilityIsReduceTransparencyEnabled()) {
        return [UIColor whiteColor];
    }
    return [UIColor colorWithWhite:1.0 alpha:0.8];
}

-(UIImage *)overlayRoundedRectImage {
    static UIImage *roundedRectImage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        CGSize imageSize = CGSizeMake(2 * AAPLOverlayCornerRadius, 2 * AAPLOverlayCornerRadius);
        UIGraphicsBeginImageContextWithOptions(imageSize, NO, [[UIScreen mainScreen] scale]);
        
        UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0.0, 0.0, imageSize.width, imageSize.height) cornerRadius:AAPLOverlayCornerRadius];
        [[self overlayColor] set];
        [roundedRect fill];
        
        roundedRectImage = UIGraphicsGetImageFromCurrentImageContext();
        roundedRectImage = [roundedRectImage resizableImageWithCapInsets:UIEdgeInsetsMake(AAPLOverlayCornerRadius, AAPLOverlayCornerRadius, AAPLOverlayCornerRadius, AAPLOverlayCornerRadius)];
        UIGraphicsEndImageContext();
    });
    return roundedRectImage;
}

@end
