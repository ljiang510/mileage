//
//  APIClient.m
//  MotionDetection
//
//  Created by Liangjun Jiang on 5/27/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "APIClient.h"
#import "Constants.h"
@implementation APIClient

+(instancetype)sharedClient{
    static APIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *baseUrl = [NSURL URLWithString:kBaseURL];
        _sharedClient = [[APIClient alloc] initWithBaseURL:baseUrl];
        
    });
    
    return _sharedClient;
}
-(instancetype)initWithBaseURL:(NSURL *)url{
    self = [super initWithBaseURL:url];
    if (self) {
        self.securityPolicy.allowInvalidCertificates = YES;
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }
    return self;
}

@end
