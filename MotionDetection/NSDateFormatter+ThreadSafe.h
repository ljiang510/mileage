//
//  NSDateFormatter+ThreadSafe.h
//  MotionDetection
//
//  Created by Liangjun Jiang on 5/22/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (ThreadSafe)
+ (NSDateFormatter *)dateWriter;
+ (NSDateFormatter *)dateReader;
+ (NSDateFormatter *)shortStyle;
-(NSString *)weekday:(NSDate *)date;
-(NSString *)monthSymbolForToday;
-(NSString *)yearSymbolForToday;
@end
