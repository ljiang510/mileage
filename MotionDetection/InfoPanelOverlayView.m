//
//  InfoPanelOverlayView.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/15/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "InfoPanelOverlayView.h"
#import "AAPLStyleUtilities.h"
@implementation InfoPanelOverlayView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.backgroundColor = [AAPLStyleUtilities foregroundColor]; //[AAPLStyleUtilities overlayColor];
    self.layer.cornerRadius = [AAPLStyleUtilities overlayCornerRadius];
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.alpha = 0.0;
    
    UILabel *label = [AAPLStyleUtilities standardLabel];
    label.textColor = [AAPLStyleUtilities overlayColor];
    label.frame = frame;
    label.font = [AAPLStyleUtilities largeFont];
    label.text = NSLocalizedString(@"All caught up and start to drive!", @"Shown when all trips have been classified");
    [self addSubview:label];

    return self;
    
}

@end
