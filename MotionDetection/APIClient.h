//
//  APIClient.h
//  MotionDetection
//
//  Created by Liangjun Jiang on 5/27/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface APIClient : AFHTTPSessionManager
+(instancetype)sharedClient;
-(instancetype)initWithBaseURL:(NSURL *)url;

@end
