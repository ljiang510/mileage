//
//  OverviewTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/1/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "OverviewTableViewController.h"
#import "OverviewTableViewCell.h"
#import "JBBarChartViewController.h"
#import "TripSummary.h"
#import "Expense.h"
#import "ParseErrorHandlingController.h"
#import "NSNumberFormatter+ThreadSafe.h"
#import "DataProcessingManager.h"
#import "NSCalendar+ThreadSafe.h"
#import "NSDateFormatter+ThreadSafe.h"
#import "JBLineChartViewController.h"
#import "DGActivityIndicatorView.h"
#import "AppManager.h"
#import "DemoManager.h"
#import "MonthlyTableViewController.h"
typedef NS_OPTIONS(NSInteger, SECTION_TITLE){
    SECTION_LAST_7_DAYS,
    SECTION_LAST_MONTH,
    SECTION_YEAR_TO_DATE_BY_MONTH
};


@interface OverviewTableViewController (){
    NSInteger currentIndex;
    double totalMilesForThisWeek;
    double totalMilesForThisMonth;
    double totalExpenseForThisWeek;
    double totalExpenseForThisMonth;
    
    double totalMilesForThisYear;
    double totalExpenseForThisYear;
    
}
@property (nonatomic, strong) NSArray *thisWeekMilesArray;
@property (nonatomic, strong) NSArray *thisMonthMilesArray;
@property (nonatomic, strong) NSArray *thisWeekExpenseArray;
@property (nonatomic, strong) NSArray *thisMonthExpenseArray;

@property (nonatomic, strong) NSArray *thisYearMilesArray;
@property (nonatomic, strong) NSArray *thisYearExpenseArray;

@property (nonatomic,strong) DataProcessingManager *manager;
//@property (nonatomic,strong) DGActivityIndicatorView *activityView;

@end

@implementation OverviewTableViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
   
}

-(IBAction)loadData:(id)sender
{
    [self fetchMilesInThisWeek];
    [self fetchExpenseInThisWeek];
    [self fetchMilesInThisMonth];
    [self fetchExpenseInThisMonth];
    
    [self fetchMilesInThisYear];
    [self fetchExpenseInThisYear];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentIndex = 0;
    
    self.manager = [DataProcessingManager sharedManager];
    NSArray *segmentTextContent = @[
                                    NSLocalizedString(@"Mileage", @""),
                                    NSLocalizedString(@"Expense", @"")
                                    
                                    ];
    
    // Segmented control as the custom title view
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:segmentTextContent];
    segmentedControl.selectedSegmentIndex = currentIndex;
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    segmentedControl.frame = CGRectMake(0, 0, 400.0f, 30.0f);
    [segmentedControl addTarget:self action:@selector(action:) forControlEvents:UIControlEventValueChanged];
    
    self.navigationItem.titleView = segmentedControl;
    
    totalMilesForThisWeek = 0;
    totalMilesForThisMonth = 0;
    totalExpenseForThisWeek = 0;
    totalExpenseForThisMonth = 0;
    //    self.activityView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor redColor] size:20.0f];
    //    self.activityView.frame = CGRectMake(0.0f, 0.0f, 50.0f, 50.0f);
    //    self.activityView.center = self.tableView.center;
    //
    if ([[DemoManager sharedManager] isDemo]) {
        [self testDemo];
    } else {
        [self loadData:nil];
        
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (IBAction)action:(id)sender
{
    currentIndex = [sender selectedSegmentIndex];
    [self.tableView reloadData];
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 180.0;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[DemoManager sharedManager] isDemo]?NSLocalizedString(@"Demo Data", @""):@"";
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (OverviewTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OverviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OverviewCell" forIndexPath:indexPath];
    NSString *title = @"";
    NSString *formatString = @"";
    NSString *unitString = @"";
    
    switch (indexPath.section) {
        case SECTION_LAST_7_DAYS:
        {
            formatString = [[NSCalendar safeCalendar] printFirstDayAndLastDayOfThisWeek];
            
            switch (currentIndex) {
                case 0:{
                    title = [NSString stringWithFormat:@"%.1f",totalMilesForThisWeek];
                    unitString = NSLocalizedString(@"miles", @"miles");
                    break;
                }
                case 1:{
                    NSNumberFormatter *currencyNumberFormatter = [NSNumberFormatter currencyFormatter];
                    title = [NSString stringWithFormat:@"%@", [currencyNumberFormatter stringFromNumber:[NSNumber numberWithDouble:totalExpenseForThisWeek]]];
                    break;
                }
                case 2:{
                    //todo: to track hours
                    title = NSLocalizedString(@"Total: $12", @"");
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case SECTION_LAST_MONTH:
        {
            formatString = [[NSDateFormatter dateWriter] monthSymbolForToday];
            switch (currentIndex) {
                case 0:
                    title = [NSString stringWithFormat:@"%.1f",totalMilesForThisMonth];
                    unitString = NSLocalizedString(@"miles", @"miles");
                    break;
                case 1: {
                    NSNumberFormatter *currencyNumberFormatter = [NSNumberFormatter currencyFormatter];
                    title = [NSString stringWithFormat:@"%@",[currencyNumberFormatter stringFromNumber:[NSNumber numberWithDouble:totalExpenseForThisMonth]]];
                    break;
                }
                case 2:
                    title = NSLocalizedString(@"Total: $0", @"");
                    
                    break;
                default:
                    break;
            }
            break;
        }
        case SECTION_YEAR_TO_DATE_BY_MONTH:
        {
            formatString = [[NSDateFormatter dateWriter] yearSymbolForToday];
            switch (currentIndex) {
                case 0:
                    title = [NSString stringWithFormat:@"%.1f",totalMilesForThisYear];
                    unitString = NSLocalizedString(@"miles", @"miles");
                    break;
                case 1:{
                    NSNumberFormatter *currencyNumberFormatter = [NSNumberFormatter currencyFormatter];
                    title = [NSString stringWithFormat:@"%@",[currencyNumberFormatter stringFromNumber:[NSNumber numberWithDouble:totalExpenseForThisYear]]];
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
    [cell setContent:formatString num:title unit:unitString];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *data;
    if (currentIndex == 0) {
        if (indexPath.section == 0){
            data = self.thisWeekMilesArray;
            JBBarChartViewController *barChartController = [[JBBarChartViewController alloc] initWithRawData:data isFor:(currentIndex == 0)?YES:NO];
            [self.navigationController pushViewController:barChartController animated:YES];

        } else if (indexPath.section == 1){
            data = self.thisMonthMilesArray;
            JBLineChartViewController *lineChartController = [[JBLineChartViewController alloc] initWithRawData:data isForMileage:YES month:[[NSDateFormatter dateWriter] monthSymbolForToday]];
            [self.navigationController pushViewController:lineChartController animated:YES];
        } else {
            MonthlyTableViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MonthlyTableViewController"];
            [viewController setData:self.thisYearMilesArray];
            [viewController setIsForMileage:YES];
            [self.navigationController pushViewController:viewController animated:YES];
            
        }
        
    } else {
        if (indexPath.section == 0) {
            data = self.thisWeekExpenseArray;
            JBBarChartViewController *barChartController = [[JBBarChartViewController alloc] initWithRawData:data isFor:(currentIndex == 0)?YES:NO];
            [self.navigationController pushViewController:barChartController animated:YES];
        } else if (indexPath.section == 1){
            data = self.thisMonthExpenseArray;
            JBLineChartViewController *lineChartController = [[JBLineChartViewController alloc] initWithRawData:data isForMileage:NO month:[[NSDateFormatter dateWriter] monthSymbolForToday]];
            [self.navigationController pushViewController:lineChartController animated:YES];
        } else {
            MonthlyTableViewController  *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MonthlyTableViewController"];
            [viewController setData:self.thisYearExpenseArray];
            [viewController setIsForMileage:NO];
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

#pragma mark - APIs

-(void)fetchMilesInThisWeek{
//    [self addActivityView];
    PFQuery *thisWeekQuery = [TripSummary queryForThisWeek];
    __weak typeof(self) weakSelf = self;
    [thisWeekQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
//        [weakSelf removeActivityView];
        if ([weakSelf.refreshControl isRefreshing]) {
            [weakSelf.refreshControl endRefreshing];
        }
        if (!error) {
            weakSelf.thisWeekMilesArray = objects;
            totalMilesForThisWeek = [weakSelf.manager calculateTotalMiles:weakSelf.thisWeekMilesArray];
             [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];
}

-(void)fetchMilesInThisMonth{
//    [self addActivityView];
    PFQuery *thisWeekQuery = [TripSummary queryForThisMonth];
    __weak typeof(self) weakSelf = self;
    [thisWeekQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if ([weakSelf.refreshControl isRefreshing]) {
            [weakSelf.refreshControl endRefreshing];
        }
//        [weakSelf removeActivityView];
        if (!error) {
            weakSelf.thisMonthMilesArray = objects;
            totalMilesForThisMonth = [weakSelf.manager calculateTotalMiles:weakSelf.thisMonthMilesArray];
            [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];
}



-(void)fetchExpenseInThisWeek{
//    [self addActivityView];
    PFQuery *expenseQuery = [Expense queryExpenseForWeek];
    __weak typeof(self) weakSelf = self;
    [expenseQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if ([weakSelf.refreshControl isRefreshing]) {
            [weakSelf.refreshControl endRefreshing];
        }
//        [weakSelf removeActivityView];
        if (!error) {
            weakSelf.thisWeekExpenseArray = objects;
            totalExpenseForThisWeek = [weakSelf.manager calculateTotalExpense:weakSelf.thisWeekExpenseArray];
            [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];
    
}

-(void)fetchExpenseInThisMonth{
//    [self addActivityView];
    PFQuery *expenseQuery = [Expense queryForThisMonth];
    __weak typeof(self) weakSelf = self;
    [expenseQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if ([weakSelf.refreshControl isRefreshing]) {
            [weakSelf.refreshControl endRefreshing];
        }
//        [self removeActivityView];
        if (!error) {
            weakSelf.thisMonthExpenseArray = objects;
            totalExpenseForThisMonth = [weakSelf.manager calculateTotalExpense:weakSelf.thisMonthExpenseArray];
            [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];
    
}

-(void)fetchMilesInThisYear{
    PFQuery *thisWeekQuery = [TripSummary queryForThisYear];
    __weak typeof(self) weakSelf = self;
    [thisWeekQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if ([weakSelf.refreshControl isRefreshing]) {
            [weakSelf.refreshControl endRefreshing];
        }
        //        [weakSelf removeActivityView];
        if (!error) {
            weakSelf.thisYearMilesArray = objects;
            totalMilesForThisYear = [weakSelf.manager calculateTotalMiles:weakSelf.thisYearMilesArray];
            [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:2]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];
}
-(void)fetchExpenseInThisYear{
    PFQuery *expenseQuery = [Expense queryForThisYear];
    __weak typeof(self) weakSelf = self;
    [expenseQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if ([weakSelf.refreshControl isRefreshing]) {
            [weakSelf.refreshControl endRefreshing];
        }
        //        [self removeActivityView];
        if (!error) {
            weakSelf.thisYearExpenseArray = objects;
            totalExpenseForThisYear = [weakSelf.manager calculateTotalExpense:weakSelf.thisYearExpenseArray];
            [weakSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:2]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];

}



#pragma mark - test Demo
-(void)testDemo{
    self.thisWeekMilesArray = [[DemoManager sharedManager] tripSummary];
    totalMilesForThisWeek = [self.manager calculateTotalMiles:self.thisWeekMilesArray];
    self.thisMonthMilesArray = self.thisWeekMilesArray;
    totalMilesForThisMonth = totalMilesForThisWeek;
    self.thisWeekExpenseArray = [[DemoManager sharedManager] expenses];
    totalExpenseForThisWeek = [self.manager calculateTotalExpense:self.thisWeekExpenseArray];
    self.thisMonthExpenseArray = self.thisWeekExpenseArray;
    totalExpenseForThisMonth = totalExpenseForThisWeek;
    [self.tableView reloadData];
}

//-(void)addActivityView{
//    if (self.activityView) {
//        [self.view addSubview:self.activityView];
//    }
//
//    [self.activityView startAnimating];
//
//}
//
//-(void)removeActivityView{
//    [self.activityView stopAnimating];
//    [self.activityView removeFromSuperview];
//}

@end
