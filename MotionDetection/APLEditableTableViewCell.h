


@interface APLEditableTableViewCell : UITableViewCell

@property (nonatomic, weak, readonly) UITextField *textField;

@end
