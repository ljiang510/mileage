//
//  NSCalendar+ThreadSafe.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/4/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "NSCalendar+ThreadSafe.h"
#import "NSDateFormatter+ThreadSafe.h"
@implementation NSCalendar (ThreadSafe)
+(NSCalendar *)safeCalendar{
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSCalendar *calendar = [dictionary objectForKey:@"SCCalendarReader"];
    if (!calendar)
    {
        calendar = [NSCalendar currentCalendar];
        [dictionary setObject:calendar forKey:@"SCCalendarReader"];
    }
    return calendar;
    
}

-(NSDate *)startOfDayThisWeek:(NSDate *)date{
    NSDate *now = [NSDate date];
    NSDate *startOfTheWeek;
    NSTimeInterval interval;
    [self rangeOfUnit:NSCalendarUnitWeekOfYear startDate:&startOfTheWeek interval:&interval forDate:now];
    
    return startOfTheWeek;
    
}

-(NSDate *)startOfDayThisMonth{
    NSDate *now = [NSDate date];
    NSDateComponents * currentDateComponents = [self components: NSCalendarUnitYear | NSCalendarUnitMonth fromDate:now];
    NSDate * startOfMonth = [self dateFromComponents: currentDateComponents];
    return startOfMonth;
}

-(NSDate *)startOfDayLastMonth{
    NSDate *now = [NSDate date];
    NSDateComponents * currentDateComponents = [self components: NSCalendarUnitYear | NSCalendarUnitMonth fromDate:now];
    currentDateComponents.month = currentDateComponents.month - 1;
    currentDateComponents.day = 1;
    NSDate * startOfMonth = [self dateFromComponents: currentDateComponents];
    return startOfMonth;
}


-(NSInteger)weekday:(NSDate *)date{
    NSDateComponents *comps = [self components:(NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:date];
    NSInteger weekday = [comps weekday];
    
    return weekday-1;
}

-(NSInteger)totalDaysInThisMonth{
    NSRange range = [self rangeOfUnit:NSCalendarUnitDay
                           inUnit:NSCalendarUnitMonth
                          forDate:[NSDate date]];
    return range.length;
}

-(NSInteger)monthForToday{
    NSDateComponents *comps = [self components:(NSCalendarUnitDay | NSCalendarUnitMonth) fromDate:[NSDate date]];
    NSInteger m = [comps month];
    
    return m;
    
}
-(NSInteger)monthForDay:(NSDate *)date{
    NSDateComponents *comps = [self components:(NSCalendarUnitDay | NSCalendarUnitMonth) fromDate:date];
    NSInteger m = [comps month];
    
    return m;

}

-(NSDate *)firstDayOfThisYear{
    NSDate *now = [NSDate date];
    NSDateComponents * currentDateComponents = [self components: NSCalendarUnitYear fromDate:now];
    NSDate * startOfYear = [self dateFromComponents: currentDateComponents];
    return startOfYear;
}

-(NSInteger)dayOfMonth:(NSDate *)date{
    NSDateComponents *comps = [self components:(NSCalendarUnitDay | NSCalendarUnitMonth) fromDate:date];
    NSInteger d = [comps day];
    
    return d;
}

-(NSString *)printFirstDayAndLastDayOfThisWeek{
    NSDate *now = [NSDate date];
    
    NSInteger weekNumber =  [[self components: NSCalendarUnitWeekOfYear fromDate:now] weekOfYear];
    NSDateComponents *comp = [self components:NSCalendarUnitYear fromDate:now];
    [comp setWeekOfYear:weekNumber];  //Week number.
    [comp setWeekday:1]; //First day of the week. Change it to 7 to get the last date of the week
    
    NSDate *resultDate = [self dateFromComponents:comp];
    
    NSDateFormatter *formatter = [NSDateFormatter dateWriter];
    [formatter setDateFormat:@"MMM d, yyyy"];
    
    NSString *myDateString = [formatter stringFromDate:resultDate];
    
    [comp setWeekday:7];
    resultDate  = [self dateFromComponents:comp];
    
    return [NSString stringWithFormat:@"%@ - %@", myDateString,[formatter stringFromDate:resultDate]];
}

// Generate a random date sometime between now and n days before day.
// Also, generate a random time to go with the day while we are at it.
- (NSDate *) generateRandomDateWithinDaysBeforeToday:(u_int32_t)days
{
    u_int32_t r1 = arc4random_uniform(days);
    u_int32_t r2 = arc4random_uniform(23);
    u_int32_t r3 = arc4random_uniform(59);
    
    NSDate *today = [NSDate new];
    
    NSDateComponents *offsetComponents = [NSDateComponents new];
    [offsetComponents setDay:(r1*-1)];
    [offsetComponents setHour:r2];
    [offsetComponents setMinute:r3];
    
    NSDate *rndDate1 = [self dateByAddingComponents:offsetComponents
                                                  toDate:today options:0];
    
    return rndDate1;
}

-(NSDate *)oneMonthLater{
    
    NSDate *today = [NSDate new];
    NSDateComponents *offsetComponents = [NSDateComponents new];
    [offsetComponents setMonth:1];
    
    NSDate *oneMonth = [self dateByAddingComponents:offsetComponents
                                             toDate:today options:0];
    return oneMonth;
}

-(NSDate *)oneYearLater{
    NSDate *today = [NSDate new];
    NSDateComponents *offsetComponents = [NSDateComponents new];
    [offsetComponents setYear:1];
    NSDate *nextYear = [self dateByAddingComponents:offsetComponents toDate:today options:0];    return nextYear;
}



@end
