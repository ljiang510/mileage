//
//  ExpenseTableViewCell.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/3/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Expense;
@interface ExpenseTableViewCell : UITableViewCell


-(void)setItem:(Expense *)expense;
@end
