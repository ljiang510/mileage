//
//  NSDateFormatter+ThreadSafe.m
//  MotionDetection
//
//  Created by Liangjun Jiang on 5/22/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "NSDateFormatter+ThreadSafe.h"

@implementation NSDateFormatter (ThreadSafe)
+ (NSDateFormatter *)dateReader
{
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateReader = [dictionary objectForKey:@"SCDateReader"];
    if (!dateReader)
    {
        dateReader = [[NSDateFormatter alloc] init] ;
        dateReader.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        dateReader.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        dateReader.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        [dictionary setObject:dateReader forKey:@"SCDateReader"];
    }
    return dateReader;
}

+ (NSDateFormatter *)dateWriter
{
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateWriter = [dictionary objectForKey:@"SCDateWriter"];
    if (!dateWriter)
    {
        dateWriter = [[NSDateFormatter alloc] init];
        dateWriter.locale = [NSLocale currentLocale];
        dateWriter.timeZone = [NSTimeZone defaultTimeZone];
        dateWriter.dateStyle = NSDateFormatterLongStyle;
        dateWriter.timeStyle = NSDateFormatterLongStyle;
        [dictionary setObject:dateWriter forKey:@"SCDateWriter"];
    }
    return dateWriter;
}

+ (NSDateFormatter *)shortStyle{
    NSMutableDictionary *dictionary = [[NSThread currentThread] threadDictionary];
    NSDateFormatter *dateWriter = [dictionary objectForKey:@"SCDateShortWriter"];
    if (!dateWriter)
    {
        dateWriter = [[NSDateFormatter alloc] init];
        dateWriter.locale = [NSLocale currentLocale];
        dateWriter.timeZone = [NSTimeZone defaultTimeZone];
        dateWriter.timeStyle = NSDateFormatterShortStyle;
        [dictionary setObject:dateWriter forKey:@"SCDateShortWriter"];
    }
    return dateWriter;
}

-(NSString *)weekday:(NSDate *)date{
    [self setDateFormat:@"EEEE, MMMM dd"];
    return [self stringFromDate:date];
}
-(NSString *)monthSymbolForToday{
    NSDate *today = [NSDate date];
    [self setDateFormat:@"MMMM"];
    return [self stringFromDate:today];
}

-(NSString *)yearSymbolForToday{
    NSDate *today = [NSDate date];
    [self setDateFormat:@"YYYY"];
    return [self stringFromDate:today];
}

@end
