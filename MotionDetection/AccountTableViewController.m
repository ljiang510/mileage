//
//  AccountTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 5/31/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "AccountTableViewController.h"
#import "Constants.h"
#import "SVProgressHud.h"
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "Driver.h"
#import "ConfigureManager.h"
#import "AAPLStyleUtilities.h"
#import "MeTableViewController.h"
#import "CarsTableViewCotnroller.h"
#import "ParentViewController.h"
#import "UserVoice.h"
#import "AppManager.h"
#import "Purchase.h"
@import MessageUI.MFMailComposeViewController;
typedef NS_OPTIONS(NSInteger, SECTIONS) {
    SECTION_DRIVER,
    SECTION_PLAN,
    SECTION_ABOUT
};

typedef NS_OPTIONS(NSInteger, DRIVER_ROW){
    SIGNED_IN = 0,
    CAR = 1,
    PROJECT = 2,
    SIGN_OUT = 3
};


typedef NS_OPTIONS(NSInteger, ABOUT_ROW){
    HELP = 0,
    RATE_US = 1,
    SHARE_US = 2,
    CONTACT_US= 3,
    TOS = 4
};

@interface AccountTableViewController ()<MFMailComposeViewControllerDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) Driver *driver;
@end

@implementation AccountTableViewController
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.driver = [Driver currentUser];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"My Account", @"");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    int num = 0;
    switch (section) {
        case SECTION_DRIVER:
            num = ([PFAnonymousUtils isLinkedWithUser:[PFUser currentUser]])?1:4;
            break;
        case SECTION_PLAN:
            num = 1;
            break;
        case SECTION_ABOUT:
            num = 5;
            break;
        default:
            break;
    }
    
    return num;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return (section == SECTION_ABOUT)?30:20;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 30.0)];
    UILabel *label;
    label = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 0.0, 310.0, 30.0)];
    label.textColor = [AAPLStyleUtilities foregroundColor];
    label.backgroundColor = [UIColor clearColor];
    label.font = [AAPLStyleUtilities smallFont];
    label.text = (section == SECTION_ABOUT)? [self appVersionString]:@"";
    [view addSubview:label];
    return view;
 
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == SECTION_ABOUT) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 30.0)];
        UILabel *label;
        label = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 0.0, 310.0, 30.0)];
        label.textColor = [AAPLStyleUtilities foregroundColor];
        label.backgroundColor = [UIColor clearColor];
        label.font = [AAPLStyleUtilities smallFont];
        label.text = @"Copyright \u00A9 2015 Deductible Inc.";
        [view addSubview:label];
        return view;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AccountCell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSString *title = @"";
    NSString *subtitle = @"";
    
    switch (indexPath.section) {
        case SECTION_DRIVER:
        {
            switch (indexPath.row) {
                case SIGNED_IN:
                    if ([PFAnonymousUtils isLinkedWithUser:self.driver]) {
                        title = NSLocalizedString(@"Sign Up or Sign In", @"");
     } else {
                        title = NSLocalizedString(@"Signed In", @"");
                        subtitle = self.driver.email;
                    }
                    break;
                case CAR:
                    title = NSLocalizedString(@"Cars", @"");
                     cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    break;
                case PROJECT:
                    title = NSLocalizedString(@"Projects", @"");
                     cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    break;
                case SIGN_OUT:
                    title = NSLocalizedString(@"Sign Out", @"");
                    break;

                default:
                    break;
            }
            break;
        }
        case SECTION_PLAN:
        {
            title = NSLocalizedString(@"Plan", @"");
            if ([[AppManager sharedManager] getIsValidPlan]) {
                Purchase *purchase = [[AppManager sharedManager] purchase];
                subtitle = purchase.productName;
            }
            break;
        }
            
        case SECTION_ABOUT:
        {
            switch (indexPath.row) {
                case RATE_US:
                    title = NSLocalizedString(@"Rate Us", @"");
                    break;
                case SHARE_US:
                    title = NSLocalizedString(@"Share Us", @"");
                    break;
                case HELP:
                    title = NSLocalizedString(@"Help & FAQ", @"");
                    break;
                case CONTACT_US:
                    title = NSLocalizedString(@"Contact Us", @"");
                    break;
                case TOS:
                    title = NSLocalizedString(@"Terms of Service", @"");
                    break;
                    
                default:
                    break;
            }
        }
        default:
            break;
    }
    cell.textLabel.text = title;
    cell.detailTextLabel.text = subtitle;
    cell.textLabel.font = [AAPLStyleUtilities standardFont];
    cell.textLabel.textColor = [AAPLStyleUtilities foregroundColor];
    cell.detailTextLabel.textColor  = [AAPLStyleUtilities foregroundColor];
    cell.detailTextLabel.font = [AAPLStyleUtilities standardFont];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case SECTION_DRIVER:
        {
            if ([PFAnonymousUtils isLinkedWithUser:[PFUser currentUser]]) {
                [self showLogin];
            } else {
                if (indexPath.row == SIGNED_IN) {
                    
                    
                } else if (indexPath.row == SIGN_OUT) {
                    [self signOut];
                } else if (indexPath.row == CAR){
                    UIStoryboard *storyboard = [self storyboard];
                    CarsTableViewCotnroller *viewController = [storyboard instantiateViewControllerWithIdentifier:@"CarsTableViewCotnroller"];
                    [self.navigationController pushViewController:viewController animated:YES];
                } else if (indexPath.row == PROJECT){
                    UIStoryboard *storyboard = [self storyboard];
                    CarsTableViewCotnroller *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ProjectsTableViewCotnroller"];
                    [self.navigationController pushViewController:viewController animated:YES];
                }
                
            }
            break;
        }
        case SECTION_PLAN:{
            if ([PFAnonymousUtils isLinkedWithUser:[Driver currentUser]]) {
                
                [self askForLogin];
            } else {
                if ([[AppManager sharedManager] getIsValidPlan]) {
                    [tableView deselectRowAtIndexPath:indexPath animated:YES];
                } else {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"iOSInAppPurchases" bundle:nil];
                    ParentViewController *viewController = [storyboard instantiateInitialViewController];
                    [self.navigationController presentViewController:viewController animated:YES completion:nil];
                }
            }
            break;
        }
        case SECTION_ABOUT:
            switch (indexPath.row) {
                case RATE_US:
                    [self rateUs];
                    break;
                case SHARE_US:
                    [self shareUs];
                    break;
                case HELP:
                    [UserVoice presentUserVoiceInterfaceForParentViewController:self];
                    break;
                case CONTACT_US:
                    [self contactUs];
                    break;
                case TOS:
                    [self showWeb:[[ConfigureManager sharedManager] website]];
                    break;
                default:
                    break;
            }
            
        default:
            break;
    }
    
}


-(void)shareUs
{
    NSString *textToShare = NSLocalizedString(@"Deducitable - log your mileage for tax deduction", @"");
    NSURL *appStoreLinnk = [NSURL URLWithString:[[ConfigureManager sharedManager] appStoreLink]];
    NSArray *objectsToShare = @[textToShare, appStoreLinnk];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
}

- (void)contactUs
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    
    // Set up recipients
    NSArray *toRecipients = @[[[ConfigureManager sharedManager] contactEmail]];
    
    [picker setToRecipients:toRecipients];
    
    
    // Fill out the email body text
    NSString *iOSVersion = [[UIDevice currentDevice] systemVersion];
    NSString *model = [[UIDevice currentDevice] model];
    
    NSString *emailBody = [NSString stringWithFormat:@"\n\n\n\n\n\n---Please don't delete the following content---\n%@, %@\n%@", iOSVersion, model, [self appVersionString]];
    [picker setMessageBody:emailBody isHTML:NO];
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    __block NSString *message = @"";
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = NSLocalizedString(@"Mail sending canceled",@"");
            break;
        case MFMailComposeResultSaved:
            message= NSLocalizedString(@"Mail saved", @"");
            break;
        case MFMailComposeResultSent:
            message =NSLocalizedString(@"Mail sent", @"");
            break;
        case MFMailComposeResultFailed:
            message =NSLocalizedString(@"Mail sending failed", @"");
            break;
        default:
            message = NSLocalizedString(@"Mail not sent", @"");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        [SVProgressHUD showSuccessWithStatus:message];
    }];
}


- (void)showAppstore
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[ConfigureManager sharedManager] appStoreLink]]];
}


- (void)showWeb:(NSString *)link
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[ConfigureManager sharedManager] website]]];
    
}


-(NSString *)appVersionString{
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString * versionBuildString = [NSString stringWithFormat:@"%@ (%@)", appVersionString, appBuildString];
    return versionBuildString;
}


-(void)signOut {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", @"")
                                                                   message:NSLocalizedString(@"Please remember logging back in so we can track your trips for you.", @"")
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    __weak typeof(self) weakSelf = self;
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [PFUser logOut];
                                                              [PFUser enableAutomaticUser];
                                                              [[PFUser currentUser] saveInBackground];
                                                              [weakSelf.tableView reloadData];
                                                              
                                                              [weakSelf showLogin];
                                                          }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
   
}

-(void)showLogin{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate presentLoginSignUp];
}

-(void)rateUs {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Rate Us", @"")
                                                                   message:NSLocalizedString(@"Have you enjoyed this app?", @"Have you enjoyed this app?")
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    __weak typeof(self) weakSelf = self;
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", @"") style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [weakSelf showAppstore];
                                                          }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Not really", @"") style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [weakSelf contactUs];
                                                         }];
    [alert addAction:cancelAction];
    
    [alert addAction:defaultAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)askForLogin{
    
   
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Not Logged In", @"")
                                                                   message:NSLocalizedString(@"Please sign up or log in",@"")
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    __weak typeof(self) weakSelf = self;
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [weakSelf showLogin];
                                                          }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Later", @"") style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             [weakSelf contactUs];
                                                         }];
    [alert addAction:cancelAction];
    
    [alert addAction:defaultAction];
    
    [self presentViewController:alert animated:YES completion:nil];

    
}

@end
