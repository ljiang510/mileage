//
//  Tag.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/1/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Parse/Parse.h>

@interface Tag : PFObject<PFSubclassing>

@property (nonatomic, copy) NSString *name;

-(void)initWithDemoData:(NSDictionary *)demo;
+(PFQuery *)basicQuery;
@end
