//
//  BusinessCategoryTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/3/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "BusinessCategoryTableViewController.h"
#import "Tag.h"
#import "Constants.h"
#import "Expense.h"
#import "AAPLStyleUtilities.h"
@interface BusinessCategoryTableViewController ()
@end

@implementation BusinessCategoryTableViewController

-(void)awakeFromNib{
    [super awakeFromNib];
    self.parseClassName = kTag;
    self.objectsPerPage = 20;
    self.paginationEnabled = YES;
    self.pullToRefreshEnabled = YES;
}

-(void)setTag:(Tag *)tag{
    _tag = tag;
    
}

-(IBAction)onCancel:(id)sender{
    self.cancelBlock(self);
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)onDone:(id)sender{
    self.finishBlock(self,self.tag);
    [self.navigationController popViewControllerAnimated:YES];
}

- (PFQuery *)queryForTable {
    PFQuery *query = [Tag basicQuery];
    
    if ([self.objects count] == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    return query;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Category", @"Category");
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell" forIndexPath:indexPath];
    Tag *tTag = self.objects[indexPath.row];
    cell.textLabel.text = tTag.name;
    if ([self.tag.objectId isEqualToString:tTag.objectId]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    cell.textLabel.font = [AAPLStyleUtilities standardFont];
    cell.textLabel.textColor = [AAPLStyleUtilities foregroundColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger num = [self.objects indexOfObject:self.tag];
    UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:num inSection:0]];
    if  (oldCell.accessoryType == UITableViewCellAccessoryCheckmark){
        oldCell.accessoryType = UITableViewCellAccessoryNone;
    }
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    self.tag = (Tag *)[self objectAtIndexPath:indexPath];
    [self.tableView reloadData];
}
@end
