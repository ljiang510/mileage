//
//  OverviewTableViewCell.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/3/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "OverviewTableViewCell.h"
#import "AAPLStyleUtilities.h"
#import "JBFontConstants.h"
@interface OverviewTableViewCell()

@property (nonatomic,weak) IBOutlet UILabel *overviewLabel;
@property (nonatomic,weak) IBOutlet UILabel *numberLabel;
@property (nonatomic,weak) IBOutlet UILabel *unitLabel;

@end


@implementation OverviewTableViewCell



-(void)setContent:(NSString *)content num:(NSString *)numString unit:(NSString *)unitString;
{
    self.overviewLabel.font = [AAPLStyleUtilities largeFont];
    self.numberLabel.font = kJBFontInformationUnit;// [AAPLStyleUtilities largeFont];
    self.unitLabel.font = [AAPLStyleUtilities standardFont];
    
    self.overviewLabel.textColor = [AAPLStyleUtilities foregroundColor];
    
    self.numberLabel.textColor = [AAPLStyleUtilities foregroundColor];
    self.unitLabel.textColor = [AAPLStyleUtilities foregroundColor];
    self.overviewLabel.text = content;
    self.numberLabel.text = numString;
    self.unitLabel.text = unitString;
}
@end
