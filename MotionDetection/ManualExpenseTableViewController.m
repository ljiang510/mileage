//
//  ManualExpenseTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/2/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "ManualExpenseTableViewController.h"
#import "Expense.h"
#import <ParseUI/PFImageView.h>
#import "NSDateFormatter+ThreadSafe.h"
#import "Tag.h"
#import "Constants.h"
#import "AAPLStyleUtilities.h"
#import "ReciptImageViewController.h"
#import "ManualMileageTableViewCell.h"
#import "UIImage+Resize.h"
#import "SVProgressHud.h"
#import "BusinessCategoryTableViewController.h"
#import "EMABNoteViewController.h"
#import "AppDelegate.h"
#import "DemoManager.h"
#import "ProjectListTableViewController.h"
static NSString *kTitleKey = @"titleKey";
static NSString *kPlaceholderKey = @"placeholderKey";
static NSString *kKeyboardKey = @"keyboardTypeKey";

typedef NS_OPTIONS(NSInteger, sections) {
    SECTION_BUSINESS = 0,
    SECTION_TOTAL = 1,
    SECTION_MORE
};

@interface ManualExpenseTableViewController ()<UITextFieldDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    UIImagePickerController * imagePickerController;
}

@property (nonatomic, weak) IBOutlet PFImageView *receiptThumbnail;
@property (nonatomic, strong) UITextField *businessNameTextField;
@property (nonatomic, strong) UITextField *whenTextField;
@property (nonatomic, strong) UITextField *categoryTextField;
@property (nonatomic, strong) UITextField *expenseTextField;
@property (nonatomic, strong) UITextField *taxTextField;
@property (nonatomic, strong) UITextField *printTotalTextField;

@property (nonatomic, strong) UITextField *tipTextField;

@property (nonatomic, strong) UITextField *totalTextField;

@property (nonatomic, strong) NSArray *businessArray;
@property (nonatomic, strong) NSArray *totalDataArray;
@property (nonatomic, strong) NSArray *otherInfoArray;

@end

@implementation ManualExpenseTableViewController

-(void)setExpense:(Expense *)expense
{
    if (_expense != expense) {
        _expense = expense;
        
        [self updateUI];
    }
    
}

-(IBAction)onCancel:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onDone:(id)sender {
    if ([self isCompletedInput]) {
        if ([[DemoManager sharedManager] isDemo]) {
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Demo data, not Saved", @"Saved")];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
        __weak typeof(self) weakSelf = self;
            [self.expense saveInBackgroundWithBlock:^(BOOL success, NSError *error){
                if (!error) {
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Saved", @"Saved")];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    } else {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please make sure you have entered merchan name, date, category and expense amount", @"")];
    }
    
}

-(void)updateUI{
    if (self.expense.thumbnail) {
        self.receiptThumbnail.file = self.expense.thumbnail;
        [self.receiptThumbnail loadInBackground];
    } else if ([[DemoManager sharedManager] isDemo]){
        self.receiptThumbnail.image = [UIImage imageNamed:@"receipt"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onThumbnail:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.tableView.tableHeaderView addGestureRecognizer:tapGesture];
    self.businessArray = @[
                           @{
                               kTitleKey:NSLocalizedString(@"Merchant Name", @"Merchant Name"),
                               kPlaceholderKey:NSLocalizedString(@"Merchant Name", @"Merchant Name"),
                               kKeyboardKey:@(UIKeyboardTypeDefault)},
                           @{
                               kTitleKey:NSLocalizedString(@"Date",@"Date"),
                               kPlaceholderKey:NSLocalizedString(@"Date",@""),
                               kKeyboardKey:@(UIKeyboardTypeNamePhonePad)},
                           @{
                               kTitleKey:NSLocalizedString(@"Category", @"Category"),
                               kPlaceholderKey:NSLocalizedString(@"Category", @"Category"),
                               kKeyboardKey:@(UIKeyboardTypeNamePhonePad)},
                           ];

    
    self.totalDataArray = @[
                                NSLocalizedString(@"Subtotal", @"Subtotal"),
                                NSLocalizedString(@"Tax",@"Tax"),
                                NSLocalizedString(@"Printed Total", @"End Time"),
                                NSLocalizedString(@"Tip",@"Tip"),
                                NSLocalizedString(@"Grand Total",@"")
    ];
    
    self.otherInfoArray = @[NSLocalizedString(@"Project",@""),@"Notes"];
    
    [self updateUI];
}

-(void)onThumbnail:(UITapGestureRecognizer *)recognizer{
    if (self.expense.fullsizeImage) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ReciptImageViewController *viewController = (ReciptImageViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ReciptImageViewController"];
        [viewController setExpense:self.expense];
        [self.navigationController pushViewController:viewController animated:YES];
    } else {
        //ask to add new receipt
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Photo Source", @"Photo Source")
                                                                       message:NSLocalizedString(@"Choose One", @"Choose One")
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                                   handler:^(UIAlertAction * action) {
                                                                       
                                                                   }];
        
        UIAlertAction* photoLibraryAction = [UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                              [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                                              }];
        UIAlertAction* cameraAction = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
                                                              }];
        
        [alert addAction:cancelAction];
        [alert addAction:photoLibraryAction];
        [alert addAction:cameraAction];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

- (IBAction)showImagePickerForCamera:(id)sender
{
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
}


- (IBAction)showImagePickerForPhotoPicker:(id)sender
{
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    NSInteger num = 0;
    switch (section) {
        case SECTION_BUSINESS:
            num = [self.businessArray count];
            break;
        case SECTION_TOTAL:
            num = [self.totalDataArray count];
            break;
        case SECTION_MORE:
            num = [self.otherInfoArray count];
            break;
        default:
            break;
    }
    return num;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_MORE) {
        return 66.00;
    }
    return 44.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    NSString *title = @"";
    if (indexPath.section == SECTION_BUSINESS) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"BusinessCell" forIndexPath:indexPath];
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(150, kTopMargin, 150, kTextFieldHeight)];
        textField.delegate = self;
        textField.textAlignment = NSTextAlignmentRight;
        textField.font = [AAPLStyleUtilities standardFont];
        textField.textColor = [AAPLStyleUtilities foregroundColor];
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0f, textField.frame.size.height - 1, textField.frame.size.width, 1.0f);
        bottomBorder.backgroundColor = [UIColor grayColor].CGColor;
        [textField.layer addSublayer:bottomBorder];
        

        textField.tag = SECTION_BUSINESS*100+indexPath.row;
        switch (indexPath.row) {
            case 0:
            {
                textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                self.businessNameTextField = textField;
                self.businessNameTextField.text = self.expense.businessName;
                title = NSLocalizedString(@"Merchant Name", @"");
                cell.accessoryView = self.businessNameTextField;
                break;
            }
            case 1:{
                self.whenTextField = textField;
                self.whenTextField.text =[self.expense friendlyWhen];
                title = NSLocalizedString(@"Date", @"");
                cell.accessoryView = self.whenTextField;
                break;
            }
            case 2:
                self.categoryTextField = textField;
                self.categoryTextField.enabled = NO;
                self.categoryTextField.text = self.expense.tag.name;
                title = NSLocalizedString(@"Category", @"");
                cell.accessoryView = self.categoryTextField;
                break;
            default:
                break;
        }
        
        cell.textLabel.text = title;
        
    } else if (indexPath.section == SECTION_TOTAL) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"TotalCell" forIndexPath:indexPath];
        UITextField *textField = [self decimalTextField];
        textField.tag = SECTION_TOTAL*100+indexPath.row;
        switch (indexPath.row) {
            case 0:
                self.expenseTextField = textField;
                self.expenseTextField.text = [self.expense friendlyExpense];
                cell.accessoryView = self.expenseTextField;
                break;
            case 1:
                self.taxTextField = textField;
                self.taxTextField.text = [self.expense friendlyTax];
                cell.accessoryView = self.taxTextField;
                break;
            case 2:
            {
                UITextField *standardTextField = [self standardTextField];
                standardTextField.tag = SECTION_TOTAL*100+indexPath.row;
                self.printTotalTextField = standardTextField;
                self.printTotalTextField.text = [self.expense friendlyPrintTotal];
                self.printTotalTextField.enabled = NO;
                cell.accessoryView = self.printTotalTextField;
                break;
            }
            case 3:
                self.tipTextField = textField;
                self.tipTextField.text = [self.expense friendlyTip];
                cell.accessoryView = self.tipTextField;
                break;
            case 4:
            {
                UITextField *standardTextField = [self standardTextField];
                standardTextField.tag = SECTION_TOTAL*100+indexPath.row;
                self.totalTextField = standardTextField;
                self.totalTextField.text = [self.expense friendlyTotal];
                cell.accessoryView = self.totalTextField;
                break;
            }
            default:
                break;
        }
        cell.textLabel.text = self.totalDataArray[indexPath.row];
       
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"InfoCell" forIndexPath:indexPath];
      
        cell.textLabel.text = self.otherInfoArray[indexPath.row];
        
        if (indexPath.row == 0) {
            cell.detailTextLabel.text = [self.expense friendlyProject];
        } else {
            cell.detailTextLabel.text = self.expense.note;
        }
        cell.detailTextLabel.font = [AAPLStyleUtilities standardFont];
        cell.detailTextLabel.textColor = [AAPLStyleUtilities foregroundColor];
    }
    cell.textLabel.font = [AAPLStyleUtilities standardFont];
    cell.textLabel.textColor = [AAPLStyleUtilities foregroundColor];
    return cell;
}


#pragma mark - Navigation
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_BUSINESS){
        if (indexPath.row == 2) {
            UIStoryboard *storyboard = [self storyboard];
            BusinessCategoryTableViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"BusinessCategoryTableViewController"];
            viewController.tag = self.expense.tag;
            
            viewController.cancelBlock = ^(BusinessCategoryTableViewController *viewController){
                
            };
            viewController.finishBlock = ^(BusinessCategoryTableViewController *viewController, Tag *tag)
            {
                self.expense.tag = tag;
                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:SECTION_BUSINESS]] withRowAnimation:UITableViewRowAnimationAutomatic];
            };
            [self.navigationController pushViewController:viewController animated:YES];
        }
    } else if (indexPath.section == SECTION_MORE){
        UIStoryboard *storyboard = [self storyboard];
        if (indexPath.row == 0) {
            ProjectListTableViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ProjectListTableViewController"];
            if (self.expense.project) {
                viewController.project = self.expense.project;
            }
            viewController.finishBlock = ^(ProjectListTableViewController *viewController, Project *project)
            {
                self.expense.project = project;
                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:SECTION_MORE]] withRowAnimation:UITableViewRowAnimationAutomatic];
            };
            [self.navigationController pushViewController:viewController animated:YES];
            
        }
        else if (indexPath.row == 1) {
            EMABNoteViewController *noteViewController = [storyboard instantiateViewControllerWithIdentifier:@"EMABNoteViewController"];
            noteViewController.note = self.expense.note;
            noteViewController.cancelBlock = ^(EMABNoteViewController *viewController){
                
            };
            noteViewController.finishBlock = ^(EMABNoteViewController *viewController, NSString *note)
            {
                self.expense.note = note;
                [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:SECTION_MORE]] withRowAnimation:UITableViewRowAnimationAutomatic];
            };
            [self.navigationController pushViewController:noteViewController animated:YES];
        }
        
        
    } else {
        [tableView endEditing:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

-(UITextField *)standardTextField
{
    CGRect frame = CGRectMake(230, kTopMargin, 70.0, kTextFieldHeight);
    UITextField *textField= [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleNone;
    textField.textAlignment = NSTextAlignmentRight;
    textField.font = [AAPLStyleUtilities standardFont];
    textField.textColor = [AAPLStyleUtilities foregroundColor];
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.enabled = NO;
    textField.backgroundColor = [UIColor clearColor];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.keyboardType = UIKeyboardTypeDecimalPad;
    textField.delegate = self;
    return textField;
}


-(UITextField *)decimalTextField{
    
    CGRect frame = CGRectMake(230, kTopMargin, 70.0, kTextFieldHeight);
    UITextField *textField= [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleNone;
    textField.textAlignment = NSTextAlignmentRight;
    textField.font = [AAPLStyleUtilities standardFont];
    textField.textColor = [AAPLStyleUtilities foregroundColor];
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.enabled = YES;
    textField.backgroundColor = [UIColor clearColor];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.keyboardType = UIKeyboardTypeDecimalPad;
    textField.delegate = self;
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, textField.frame.size.height - 1, textField.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor grayColor].CGColor;
    [textField.layer addSublayer:bottomBorder];
 
    return textField;

}

#pragma mark - UITextField delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if ([textField isEqual:self.whenTextField]) {
        UIDatePicker *datePickerView = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        datePickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        datePickerView.datePickerMode = UIDatePickerModeDateAndTime;
        datePickerView.tag = textField.tag + 1000;
        textField.inputView = datePickerView;
        [datePickerView addTarget:self
                           action:@selector(onDatePicker:)
                 forControlEvents:UIControlEventValueChanged];
        
        // this animiation was from Apple Sample Code: DateCell
        CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
        CGSize pickerSize = [datePickerView sizeThatFits:CGSizeZero];
        CGRect startRect = CGRectMake(0.0,
                                      screenRect.origin.y + screenRect.size.height,
                                      pickerSize.width, pickerSize.height);
        datePickerView.frame = startRect;
        
        // compute the end frame
        CGRect pickerRect = CGRectMake(0.0,
                                       screenRect.origin.y + screenRect.size.height - pickerSize.height,
                                       pickerSize.width,
                                       pickerSize.height);
        
        datePickerView.frame = pickerRect;

    } else {
        if ([textField isEqual:self.expenseTextField] || [textField isEqual:self.tipTextField] || [textField isEqual:self.taxTextField]) {
            textField.text = @"";
        }
    }
    return YES;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
     [textField resignFirstResponder];
    if ([textField isEqual:self.expenseTextField] || [textField isEqual:self.tipTextField] || [textField isEqual:self.taxTextField]) {
        if ([textField isEqual:self.expenseTextField]){
            self.expense.expense = [textField.text doubleValue];
            textField.text = [self.expense friendlyExpense];
        }
        if (([textField isEqual:self.tipTextField])) {
            self.expense.tips = [textField.text doubleValue];
            textField.text = [self.expense friendlyTip];
        }
        if ([textField isEqual:self.taxTextField]) {
            self.expense.tax = [textField.text doubleValue];
            textField.text = [self.expense friendlyTax];
        }
        self.expense.printTotal = self.expense.expense + self.expense.tax;
        self.printTotalTextField.text = [self.expense friendlyPrintTotal];
        self.expense.total = self.expense.printTotal + self.expense.tips;
        self.totalTextField.text = [self.expense friendlyTotal];
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:SECTION_TOTAL], [NSIndexPath indexPathForRow:4 inSection:SECTION_TOTAL]] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else if ([textField isEqual:self.businessNameTextField]){
        self.expense.businessName = self.businessNameTextField.text;
    }
   
    return YES;
}


#pragma mark - ImagePickerController
-(IBAction)cancelPicture:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)shootPicture:(id)sender{
     [imagePickerController takePicture];
}

- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        CGRect frame =  [UIScreen mainScreen].applicationFrame;
        UIToolbar *toolBar=[[UIToolbar alloc] initWithFrame:CGRectMake(0, frame.size.height-44, self.view.frame.size.width, 44)];
        
        toolBar.barStyle =  UIBarStyleBlackOpaque;
        NSArray *items=@[
                        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel  target:self action:@selector(cancelPicture:)],
                        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace  target:nil action:nil],
                        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera  target:self action:@selector(shootPicture:)]
                        ];
        [toolBar setItems:items];
        
        // create the overlay view
        UIView *overlayView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 64.0, frame.size.width, frame.size.height-44.0)];
        overlayView.opaque=NO;
        overlayView.backgroundColor=[UIColor clearColor];
        
        // parent view for our overlay
        UIView *cameraView=[[UIView alloc] initWithFrame:frame];
        [cameraView addSubview:overlayView];
        [cameraView addSubview:toolBar];

        
        imagePickerController.showsCameraControls = NO;
        imagePickerController.extendedLayoutIncludesOpaqueBars = YES;
        [imagePickerController setCameraOverlayView:cameraView];
        AppDelegate* appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDel.tabBarController presentViewController:imagePickerController animated:YES completion:^{

        }];
    } else
        [self.parentViewController presentViewController:imagePickerController animated:YES completion:nil];
}

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    UIImage *fullsize = [UIImage imageWithImage:image scaledToWidth:1632];
    UIImage *thumbnail = [UIImage imageWithImage:fullsize scaledToWidth:408];
    
    self.receiptThumbnail.image = image;
    
    __weak typeof(self) weakSelf = self;
    
    PFFile *fullsizeFile = [PFFile fileWithName:@"fullszie.jpg" data:UIImageJPEGRepresentation(fullsize, 0.75)];
    
    [fullsizeFile saveInBackgroundWithBlock:^(BOOL success, NSError *error){
        if (!error) {
            weakSelf.expense.fullsizeImage = fullsizeFile;
        }else {
            NSLog(@"error:%@",[error localizedDescription]);
        }
    }];
    PFFile *thumbnailFile = [PFFile fileWithName:@"thumbnail.jpg" data:UIImageJPEGRepresentation(thumbnail, 0.75)];
    [thumbnailFile saveInBackgroundWithBlock:^(BOOL success, NSError *error){
        if (!error) {
            weakSelf.expense.thumbnail = thumbnailFile;
        } else {
            NSLog(@"error:%@",[error localizedDescription]);
        }
    }];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)finishAndUpdate
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma picker Delegate
-(IBAction)onDatePicker:(id)sender {
    
    UIDatePicker *datePicker = (UIDatePicker *)sender;
    NSDateFormatter *dateFormmater = [NSDateFormatter dateWriter];
    [dateFormmater setTimeStyle:NSDateFormatterShortStyle];
    [dateFormmater setDateStyle:NSDateFormatterShortStyle];
    self.expense.when = datePicker.date;
    self.whenTextField.text = [dateFormmater stringFromDate:datePicker.date];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:SECTION_BUSINESS]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(BOOL)isCompletedInput{
    return [self.expense.businessName length] > 0 && self.expense.when && self.expense.expense > 0 && self.expense.tag;
}
@end
