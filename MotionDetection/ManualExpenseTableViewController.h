//
//  ManualExpenseTableViewController.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/2/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Expense;
@interface ManualExpenseTableViewController : UITableViewController

@property (nonatomic, strong) Expense *expense;
@end
