//
//  DataProcessingManager.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/4/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "DataProcessingManager.h"
#import "TripSummary.h"
#import "Expense.h"
#import "NSCalendar+ThreadSafe.h"
#import "Tag.h"
#import "Project.h"

@implementation DataProcessingManager
+(instancetype)sharedManager{
    static DataProcessingManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        manager = [[self alloc] init];
    });
    
    return manager;
    
}

-(double)calculateTotalMiles:(NSArray *)data{
    double totalMiles = 0.0;
    if ([data count] > 0) {
        for (TripSummary *summary in data) {
            totalMiles +=summary.distance;
        }
    }
    return totalMiles;
    
}
-(double)calculateTotalExpense:(NSArray *)data{
    double totalExpense= 0.0;
    if ([data count] > 0) {
        for (Expense *expense in data) {
            totalExpense +=expense.total;
        }
    }
    return totalExpense;
}

-(NSArray *)processWeeklyMileageData:(NSArray *)data{
    if ([data count] > 0) {
        NSMutableArray *sus = [NSMutableArray array];
        NSMutableArray *ms = [NSMutableArray array];
        NSMutableArray *ts = [NSMutableArray array];
        NSMutableArray *ws = [NSMutableArray array];
        NSMutableArray *ths = [NSMutableArray array];
        NSMutableArray *fs = [NSMutableArray array];
        NSMutableArray *ss = [NSMutableArray array];
        
        for (TripSummary *summary in data) {
            NSCalendar *calendar = [NSCalendar safeCalendar];
            NSDate *date = (summary.startTime)?summary.startTime:summary.createdAt;
            NSInteger dayOfWeek = [calendar weekday:date];
            switch (dayOfWeek) {
                case 0:
                    [sus addObject:summary];
                    break;
                case 1:
                    [ms addObject:summary];
                    break;
                case 2:
                    [ts addObject:summary];
                    break;
                case 3:
                    [ws addObject:summary];
                    break;
                case 4:
                    [ths addObject:summary];
                    break;
                case 5:
                    [fs addObject:summary];
                    break;
                case 6:
                    [ss addObject:summary];
                    break;
                default:
                    break;
            }
        }
        NSArray *sets = [NSArray arrayWithObjects:sus,ms,ts,ws,ths,fs,ss,nil];
        NSMutableArray *dataset = [NSMutableArray arrayWithCapacity:7];
        for (NSMutableArray *singleSet in sets) {
            double total = [self calculateTotalMiles:[singleSet copy]];
            [dataset addObject:[NSNumber numberWithDouble:total]];
        }
        
        return [dataset copy];
        
    } else {
        return nil;
    }
}

-(NSArray *)processWeeklyExpenseData:(NSArray *)data{
    if ([data count] > 0) {
        NSMutableArray *sus = [NSMutableArray array];
        NSMutableArray *ms = [NSMutableArray array];
        NSMutableArray *ts = [NSMutableArray array];
        NSMutableArray *ws = [NSMutableArray array];
        NSMutableArray *ths = [NSMutableArray array];
        NSMutableArray *fs = [NSMutableArray array];
        NSMutableArray *ss = [NSMutableArray array];
        
        for (Expense *expense in data) {
            NSCalendar *calendar = [NSCalendar safeCalendar];
            NSInteger dayOfWeek = [calendar weekday:expense.when];
            switch (dayOfWeek) {
                case 0:
                    [sus addObject:expense];
                    break;
                case 1:
                    [ms addObject:expense];
                    break;
                case 2:
                    [ts addObject:expense];
                    break;
                case 3:
                    [ws addObject:expense];
                    break;
                case 4:
                    [ths addObject:expense];
                    break;
                case 5:
                    [fs addObject:expense];
                    break;
                case 6:
                    [ss addObject:expense];
                    break;
                default:
                    break;
            }
        }
        NSArray *sets = [NSArray arrayWithObjects:sus,ms,ts,ws,ths,fs,ss,nil];
        NSMutableArray *dataset = [NSMutableArray arrayWithCapacity:7];
        for (NSMutableArray *singleSet in sets) {
            double total = [self calculateTotalExpense:[singleSet copy]];
            [dataset addObject:[NSNumber numberWithDouble:total]];
        }
        
        return [dataset copy];
        
    } else {
        return [NSArray array];
    }

}

-(NSArray *)processMonthlyData:(NSArray *)data isForMileage:(BOOL)forMileage{
    
    if ([data count] > 0) {
        NSMutableArray *day1 = [NSMutableArray array];
        NSMutableArray *day2 = [NSMutableArray array];
        NSMutableArray *day3 = [NSMutableArray array];
        NSMutableArray *day4 = [NSMutableArray array];
        NSMutableArray *day5 = [NSMutableArray array];
        NSMutableArray *day6 = [NSMutableArray array];
        NSMutableArray *day7 = [NSMutableArray array];
        NSMutableArray *day8 = [NSMutableArray array];
        NSMutableArray *day9 = [NSMutableArray array];
        NSMutableArray *day10 = [NSMutableArray array];
        NSMutableArray *day11 = [NSMutableArray array];
        NSMutableArray *day12 = [NSMutableArray array];
        NSMutableArray *day13 = [NSMutableArray array];
        NSMutableArray *day14 = [NSMutableArray array];
        NSMutableArray *day15 = [NSMutableArray array];
        NSMutableArray *day16 = [NSMutableArray array];
        NSMutableArray *day17 = [NSMutableArray array];
        NSMutableArray *day18 = [NSMutableArray array];
        NSMutableArray *day19 = [NSMutableArray array];
        NSMutableArray *day20 = [NSMutableArray array];
        NSMutableArray *day21 = [NSMutableArray array];
        NSMutableArray *day22 = [NSMutableArray array];
        NSMutableArray *day23 = [NSMutableArray array];
        NSMutableArray *day24 = [NSMutableArray array];
        NSMutableArray *day25 = [NSMutableArray array];
        NSMutableArray *day26 = [NSMutableArray array];
        NSMutableArray *day27 = [NSMutableArray array];
        NSMutableArray *day28 = [NSMutableArray array];
        NSMutableArray *day29 = [NSMutableArray array];
        NSMutableArray *day30 = [NSMutableArray array];
        NSMutableArray *day31 = [NSMutableArray array];
        
        for (PFObject *object in data) {
            NSCalendar *calendar = [NSCalendar safeCalendar];
            NSDate *date;
            if (forMileage) {
                date = ((TripSummary *)object).startTime?((TripSummary *)object).startTime:((TripSummary *)object).createdAt;
            } else {
                date = ((Expense *)object).when?((Expense *)object).when:((Expense *)object).createdAt;
            }
            NSInteger dayOfWeek = [calendar dayOfMonth:date];
            switch (dayOfWeek) {
                case 1:
                    [day1 addObject:object];
                    break;
                case 2:
                    [day2 addObject:object];
                    break;
                case 3:
                    [day3 addObject:object];
                    break;
                case 4:
                    [day4 addObject:object];
                    break;
                case 5:
                    [day5 addObject:object];
                    break;
                case 6:
                    [day6 addObject:object];
                    break;
                case 7:
                    [day7 addObject:object];
                    break;
                case 8:
                    [day8 addObject:object];
                    break;
                case 9:
                    [day9 addObject:object];
                    break;
                case 10:
                    [day10 addObject:object];
                    break;
                case 11:
                    [day11 addObject:object];
                    break;
                case 12:
                    [day12 addObject:object];
                    break;
                case 13:
                    [day13 addObject:object];
                    break;
                case 14:
                    [day14 addObject:object];
                    break;
                case 15:
                    [day15 addObject:object];
                    break;
                case 16:
                    [day16 addObject:object];
                    break;
                case 17:
                    [day17 addObject:object];
                    break;
                case 18:
                    [day18 addObject:object];
                    break;
                case 19:
                    [day19 addObject:object];
                    break;
                case 20:
                    [day20 addObject:object];
                    break;
                case 21:
                    [day21 addObject:object];
                    break;
                case 22:
                    [day22 addObject:object];
                    break;
                case 23:
                    [day23 addObject:object];
                    break;
                case 24:
                    [day24 addObject:object];
                    break;
                case 25:
                    [day25 addObject:object];
                    break;
                case 26:
                    [day26 addObject:object];
                    break;
                case 27:
                    [day27 addObject:object];
                    break;
                case 28:
                    [day28 addObject:object];
                    break;
                case 29:
                    [day29 addObject:object];
                    break;
                case 30:
                    [day30 addObject:object];
                    break;
                case 31:
                    [day31 addObject:object];
                    break;
                default:
                    break;
            }
        }
        NSArray *sets = [NSArray arrayWithObjects:day1,day2,day3,day4,day5,day6,day7,day8,day9,day10,day11,day12,day13,day14,day15,day16,day17,day18, day19,day20,day21,day22,day24, day25,day26, day27,day28,day29, day30,day31,nil];
        NSMutableArray *dataset = [NSMutableArray arrayWithCapacity:31];
        for (NSMutableArray *singleSet in sets) {
            double total = 0.0;
            if (forMileage) {
                total = [self calculateTotalMiles:[singleSet copy]];
            } else
                total = [self calculateTotalExpense:[singleSet copy]];
            
            [dataset addObject:[NSNumber numberWithDouble:total]];
        }
        
        return [dataset copy];
    } else {
        
        return [NSArray array];
    }
    
}
-(NSInteger)daysCountInThisMonth:(NSDate *)date{
    NSCalendar* cal = [NSCalendar safeCalendar];
    NSDateComponents* comps = [[NSDateComponents alloc] init];

    // Set your month here
    [comps setMonth:1];

    NSRange range = [cal rangeOfUnit:NSCalendarUnitDay
                              inUnit:NSCalendarUnitMonth
                             forDate:[cal dateFromComponents:comps]];
    return range.length;
}

-(NSString *)generateReport:(NSArray *)data isForMileage:(BOOL)forMileage{
    NSString *exportString = @"";
    if (data && [data count] > 0) {
        if (forMileage) {
            exportString = @"date,distance(mile),car,project,origin street, origin city, origin state & zip, origin country, dest. street, dest. city, dest. state & zip, dest. country\n";
        }else
            exportString = @"date,merchant,category,expense,tax,print total,tip,total,note,fileUrl\n";
        for(PFObject *object in data){
            if (forMileage) {
                TripSummary *summary = (TripSummary *)object;
                exportString = [exportString stringByAppendingString:[summary reportString]];
            } else {
                Expense *expense = (Expense *)object;
                exportString = [exportString stringByAppendingString:[expense reportString]];
            }
            
        }
    }
    
    
    return exportString;
}

-(NSSet *)CategoriesInMonthlyExpense:(NSArray *)expenses{
    if (!expenses)return [NSSet set];
    NSMutableSet *categories = [NSMutableSet set];
    
    for (Expense *e in expenses) {
        if (e.tag) {
           [categories addObject:e.tag];
        }
        
    }
    
    return categories;
        
}
-(NSSet *)ProjectsInMonthlyExpense:(NSArray *)expenses{
    if (!expenses)return [NSSet set];
    NSMutableSet *projects = [NSMutableSet set];
    
    for (Expense *e in expenses) {
        if (e.project) {
            [projects addObject:e.project];
        }
    }
    
    return projects;

}

-(NSArray *)expensesForCategory:(Tag *)tag data:(NSArray *)data{
    if (!tag || !data) return [NSArray array];
    NSMutableArray *result = [NSMutableArray array];
    for (Expense *e in data) {
        if ([e.tag.objectId isEqualToString:tag.objectId]) {
            [result addObject:e];
        }
    }
    return [result copy];
    
}

-(NSArray *)expensesForProject:(Project *)project data:(NSArray *)data{
    if (!project || !data) return [NSArray array];
    NSMutableArray *result = [NSMutableArray array];
    for (Expense *e in data) {
        if ([e.project.objectId isEqualToString:project.objectId]) {
            [result addObject:e];
        }
    }
    return [result copy];
}


-(NSDictionary *)expensesByCategories:(NSArray *)data{
    if (!data) return [NSDictionary dictionary];
    NSSet *categories = [self CategoriesInMonthlyExpense:data];
    NSMutableDictionary *results = [NSMutableDictionary dictionary];
    for (Tag *tag in categories){
        NSArray *expenses = [self expensesForCategory:tag data:data];
        [results setObject:expenses forKey:tag.name];
    }
    return [results copy];
}
-(NSDictionary *)expensesByProjects:(NSArray *)data{
    if (!data) return [NSDictionary dictionary];
    NSSet *projects = [self ProjectsInMonthlyExpense:data];
    NSMutableDictionary *results = [NSMutableDictionary dictionary];
    for (Project *project in projects){
        NSArray *expenses = [self expensesForProject:project data:data];
        [results setObject:expenses forKey:project.name];
    }
    return [results copy];
}

-(NSArray *)expensesPerMonth:(NSArray *)data{
    if (!data) {
        return [NSArray array];
    }
   
    NSMutableArray *jans = [NSMutableArray array];
    NSMutableArray *febs = [NSMutableArray array];
    NSMutableArray *mars = [NSMutableArray array];
    NSMutableArray *apis = [NSMutableArray array];
    NSMutableArray *mays = [NSMutableArray array];
    NSMutableArray *juns = [NSMutableArray array];
    NSMutableArray *julys = [NSMutableArray array];
    NSMutableArray *augs = [NSMutableArray array];
    NSMutableArray *septs = [NSMutableArray array];
    NSMutableArray *octs = [NSMutableArray array];
    NSMutableArray *novs = [NSMutableArray array];
    NSMutableArray *decs = [NSMutableArray array];
    for (Expense *e in data){
        NSDate *targetDate = (e.when)?e.when:e.updatedAt;
        NSInteger monthForADay = [[NSCalendar safeCalendar] monthForDay:targetDate];
        switch (monthForADay) {
            case 1:
                [jans addObject:e];
                break;
            case 2:
                [febs addObject:e];
                break;
            case 3:
                [mars addObject:e];
                break;
            case 4:
                [apis addObject:e];
                break;
            case 5:
                [mays addObject:e];
                break;
            case 6:
                [juns addObject:e];
                break;
            case 7:
                [julys addObject:e];
                break;
            case 8:
                [augs addObject:e];
                break;
            case 9:
                [septs addObject:e];
                break;
            case 10:
                [octs addObject:e];
                break;
            case 11:
                [novs addObject:e];
                break;
            case 12:
                [decs addObject:e];
                break;
            default:
                break;
        }
        
    }
    return @[jans, febs, mars, apis, mays,juns, julys, augs, septs, octs, novs, decs];
}
-(NSArray *)milesPerMonth:(NSArray *)data{
    if (!data) {
        return [NSArray array];
    }
    NSMutableArray *jans = [NSMutableArray array];
    NSMutableArray *febs = [NSMutableArray array];
    NSMutableArray *mars = [NSMutableArray array];
    NSMutableArray *apis = [NSMutableArray array];
    NSMutableArray *mays = [NSMutableArray array];
    NSMutableArray *juns = [NSMutableArray array];
    NSMutableArray *julys = [NSMutableArray array];
    NSMutableArray *augs = [NSMutableArray array];
    NSMutableArray *septs = [NSMutableArray array];
    NSMutableArray *octs = [NSMutableArray array];
    NSMutableArray *novs = [NSMutableArray array];
    NSMutableArray *decs = [NSMutableArray array];
    for (TripSummary *t in data){
        NSDate *targetDate = (t.startTime)?t.startTime:t.updatedAt;
        NSInteger monthForADay = [[NSCalendar safeCalendar] monthForDay:targetDate];
        switch (monthForADay) {
            case 1:
                [jans addObject:t];
                break;
            case 2:
                [febs addObject:t];
                break;
            case 3:
                [mars addObject:t];
                break;
            case 4:
                [apis addObject:t];
                break;
            case 5:
                [mays addObject:t];
                break;
            case 6:
                [juns addObject:t];
                break;
            case 7:
                [julys addObject:t];
                break;
            case 8:
                [augs addObject:t];
                break;
            case 9:
                [septs addObject:t];
                break;
            case 10:
                [octs addObject:t];
                break;
            case 11:
                [novs addObject:t];
                break;
            case 12:
                [decs addObject:t];
                break;
            default:
                break;
        }
        
    }
    
    return @[jans, febs, mars, apis, mays,juns, julys, augs, septs, octs, novs, decs];
}

@end
