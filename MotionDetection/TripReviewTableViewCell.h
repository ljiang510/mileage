//
//  TripReviewTableViewCell.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/10/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TripSummary;
@interface TripReviewTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIButton *tripTypeButton;

-(void)setItem:(TripSummary *)tSummary;
@end
