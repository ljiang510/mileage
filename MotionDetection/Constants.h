//
//  Constants.h
//  MotionDetection
//
//  Created by Liangjun Jiang on 5/23/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Foundation/Foundation.h>

extern  NSString *const kAPIURL; // = @"https://maps.googleapis.com/maps/api/distancematrix/output?json";
extern NSString *const kAPIKey;// = @"AIzaSyBwyiMK1JjSFLrWi5AuniydHYv2H84JVKs";
extern NSString *const kBaseURL;
extern NSString *const testAPIURL;// = @"http://maps.googleapis.com/maps/api/distancematrix/json?origins=Vancouver+BC%7CSeattle&destinations=San+Francisco%7CVictoria+BC&mode=bicycling&language=fr-FR&sensor=false";

extern NSString *const kMotionStatusChanged;
extern NSString *const kFinishLoading;
extern NSString *const kParseApplicationId;
extern NSString *const KParseClientKey;
extern NSString *const kTrip;
extern NSString *const kTripSummary;
extern NSString *const kExpense;
extern NSString *const kTag;
extern NSString *const kProject;
extern NSString *const kCar;
extern NSString *const kPurchase;

extern NSString *const kMonthlySubscriptionProductId;
extern NSString *const kAnnualSubscriptionProductId;

extern NSString *const kShouldShowDemo;
extern NSString *const kShouldShowTutorial;
extern NSString *const kIsValidPlan;
extern NSString *const kPlanName;




extern NSString *const kBackgroundImageName;

extern double const DOLLAR_PER_MILEAGE;
extern int const DISTANCE_THRESHOLD; //in meter
extern int const kMinTextLength;
extern float const kLeftMargin;
extern float const kTextFieldWidth;
extern float const kTextFieldHeight;
extern float const kTopMargin;



@interface Constants : NSObject

+(BOOL)isValidEmail:(NSString *)emailAdress;
@end
