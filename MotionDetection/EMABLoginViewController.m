//
//  EMABLoginViewController.m
//  Chapter7
//
//  Created by Liangjun Jiang on 4/19/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "EMABLoginViewController.h"
#import "Constants.h"
#import "Driver.h"
#import "EMABEmailTextField.h"
#import "EMABPasswordTextField.h"
#import "SVProgressHud.h"
#import "UIImageEffects.h"
#import "AAPLStyleUtilities.h"
#import "AppManager.h"
#import "DemoManager.h"
@interface EMABLoginViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, weak) IBOutlet UITableView *loginTableView;
@property (nonatomic ,weak) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, weak) IBOutlet UIButton *forgotPasswordButton;
@end

@implementation EMABLoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.backgroundImageView.image = [[AppManager sharedManager] effectImage];
    
    self.emailTextField = [[EMABEmailTextField alloc] initWithFrame:CGRectMake(kLeftMargin, kTopMargin, kTextFieldWidth, kTextFieldHeight)];
    self.emailTextField.delegate = self;
    self.passwordTextField = [[EMABPasswordTextField alloc] initWithFrame:CGRectMake(kLeftMargin, kTopMargin, kTextFieldWidth, kTextFieldHeight)];
    self.passwordTextField.delegate = self;
    
    [self.loginTableView setBackgroundView:nil];
    [self.loginTableView setBackgroundColor:[UIColor clearColor]];
    self.loginTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [self.forgotPasswordButton setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Forgot your password?", @"") attributes:@{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle), NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[AAPLStyleUtilities smallFont]}] forState:UIControlStateNormal];
    
//    [self.forgotPasswordButton addTarget:self action:@selector(onForgotPassword:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(IBAction)onLogin:(id)sender{
    
    BOOL cont0 = [self.passwordTextField.text length] >= kMinTextLength;
    BOOL cont1 = [self.emailTextField.text length] >= kMinTextLength;
    BOOL cont2 = [Constants isValidEmail:self.emailTextField.text];
    
    if (!cont0) {
        [self showWarning:NSLocalizedString(@"Password at least 6 characters.", @"Password at least 6 characters.")];
    }
   
    if (!cont1) {
        [self showWarning:NSLocalizedString(@"Email at least 6 characters.", @"Password at least 6 characters.")];
    }
    
    if (!cont2) {
        [self showWarning:NSLocalizedString(@"Doesn't look like a valid email.", @"Doesn't look like a valid email.")];
    }
    
    if (cont0 && cont1 && cont2) {
        [self.emailTextField resignFirstResponder];
        [self.passwordTextField resignFirstResponder];
       
        __weak typeof(self) weakSelf = self;
        [PFUser logInWithUsernameInBackground:self.emailTextField.text password:self.passwordTextField.text
                                            block:^(PFUser *user, NSError *error) {
                                                if (!error) {
                                                    [[DemoManager sharedManager] setShouldDemo:NO];
                                                    [weakSelf dismissViewControllerAnimated:YES completion:nil];
                                                } else {
                                                    [weakSelf showWarning:[error localizedDescription]];
                                                }
                                            }];
    }
    
}

-(IBAction)onForgotPassword:(id)sender{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Password Reset",@"") message:NSLocalizedString(@"Enter your email to reset password",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles:NSLocalizedString(@"Reset",@""), nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *emailField = (UITextField*)[alertView textFieldAtIndex:0];
    emailField.placeholder = NSLocalizedString(@"Email",@"");
    [alertView show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex) {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    } else {
        UITextField *emailField = (UITextField*)[alertView textFieldAtIndex:0];
        [PFUser requestPasswordResetForEmailInBackground:emailField.text block:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Password reset instruction sent", @"Password reset instruction sent")];
            }
            
        }];
    }
    
}

#pragma mark - tableview datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"LoginCell" forIndexPath:indexPath];
    
    UITextField *textField = nil;
    switch (indexPath.row) {
        case 0:
            textField = self.emailTextField;
            break;
        case 1:
            textField = self.passwordTextField;
            break;
        default:
            break;
    }
    
    // make sure this textfield's width matches the width of the cell
    CGRect newFrame = textField.frame;
    newFrame.size.width = CGRectGetWidth(cell.contentView.frame) - kLeftMargin*2;
    textField.frame = newFrame;
    
    // if the cell is ever resized, keep the textfield's width to match the cell's width
    textField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [cell.contentView addSubview:textField];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - helper
-(void)showWarning:(NSString *)message {
    [SVProgressHUD showErrorWithStatus:message];
}


#pragma mark - UITextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
