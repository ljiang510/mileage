//
//  NSCalendar+ThreadSafe.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/4/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCalendar (ThreadSafe)

+(NSCalendar *)safeCalendar;

-(NSDate *)startOfDayThisWeek:(NSDate *)date;
-(NSDate *)startOfDayThisMonth;
-(NSDate *)startOfDayLastMonth;

-(NSInteger)weekday:(NSDate *)date;
-(NSInteger)totalDaysInThisMonth;
-(NSInteger)dayOfMonth:(NSDate *)date;
-(NSInteger)monthForToday;
-(NSInteger)monthForDay:(NSDate *)date;

-(NSString *)printFirstDayAndLastDayOfThisWeek;

//http://stackoverflow.com/questions/10092468/how-do-you-generate-a-random-date-in-objective-c
- (NSDate *) generateRandomDateWithinDaysBeforeToday:(u_int32_t)days;


-(NSDate *)oneMonthLater;

-(NSDate *)oneYearLater;
-(NSDate *)firstDayOfThisYear;
@end
