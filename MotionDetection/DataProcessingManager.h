//
//  DataProcessingManager.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/4/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Tag;
@class Project;
@interface DataProcessingManager : NSObject
+(instancetype)sharedManager;

//just borrow this singleton class
-(double)calculateTotalMiles:(NSArray *)data;
-(double)calculateTotalExpense:(NSArray *)data;

-(NSArray *)processWeeklyMileageData:(NSArray *)data;
-(NSArray *)processWeeklyExpenseData:(NSArray *)data;

-(NSArray *)processMonthlyData:(NSArray *)data isForMileage:(BOOL)forMileage;

-(NSString *)generateReport:(NSArray *)data isForMileage:(BOOL)forMileage;

-(NSSet *)CategoriesInMonthlyExpense:(NSArray *)expenses;
-(NSSet *)ProjectsInMonthlyExpense:(NSArray *)expenses;

-(NSArray *)expensesForCategory:(Tag *)tag data:(NSArray *)data;
-(NSArray *)expensesForProject:(Project *)project data:(NSArray *)data;

-(NSDictionary *)expensesByCategories:(NSArray *)data;
-(NSDictionary *)expensesByProjects:(NSArray *)data;

//this is only for this year
-(NSArray *)expensesPerMonth:(NSArray *)data;
//this is only for this year
-(NSArray *)milesPerMonth:(NSArray *)data;



@end
