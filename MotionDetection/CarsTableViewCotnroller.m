//
//  ExpenseTagTableViewCotnrollerTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/1/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "CarsTableViewCotnroller.h"
#import "APLEditableTableViewCell.h"
#import "Car.h"
#import "Project.h"
#import "Driver.h"
#import "ParseErrorHandlingController.h"
#import "SVProgressHud.h"
#import "AAPLStyleUtilities.h"
#import "DGActivityIndicatorView.h"
@interface CarsTableViewCotnroller ()

@property (nonatomic) NSMutableArray *tagsArray;
@property (nonatomic) Car *selectedTag;

@end

@implementation CarsTableViewCotnroller

-(void)loadData:(id)sender
{
    PFQuery *query = [Car basicQuery];
    __weak typeof(self) weakSelf = self;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if (!error) {
            weakSelf.tagsArray = [NSMutableArray arrayWithArray:objects];
            [weakSelf.tableView reloadData];
            if ([weakSelf.tagsArray count] == 0) {
                [weakSelf setEditing:YES animated:NO];
                [weakSelf insertTagAnimated:NO];
            }
        }
    }];
    
}

-(IBAction)onCancel:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     [self loadData:nil];
    
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.allowsSelectionDuringEditing = YES;
    
    self.title = NSLocalizedString(@"Cars", @"");
    // Set up the navigation bar.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.tagsArray = [NSMutableArray array];
    
//    self.tableView.backgroundColor = [UIColor colorWithRed:237/255.0f green:85/255.0f blue:101/255.0f alpha:1.0f];
//    
//    DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor whiteColor] size:20.0f];
//    activityIndicatorView.frame = CGRectMake(0.0f, 0.0f, 50.0f, 50.0f);
//    activityIndicatorView.center = self.tableView.center;
//    [self.view addSubview:activityIndicatorView];
//    [activityIndicatorView startAnimating];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of tags in the array, adding one if editing (for the Add Tag row).
    NSUInteger count = [self.tagsArray count];
    if (self.editing) {
        count++;
    }
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = indexPath.row;
    
    if (row == [self.tagsArray count]) {
        static NSString *InsertionCellIdentifier = @"InsertionCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:InsertionCellIdentifier];
        cell.textLabel.font = [AAPLStyleUtilities standardFont];
        cell.textLabel.textColor = [AAPLStyleUtilities foregroundColor];
        return cell;
    }
    
    static NSString *TagCellIdentifier = @"EditableTableViewCell";
    
    APLEditableTableViewCell *cell = (APLEditableTableViewCell *)[tableView dequeueReusableCellWithIdentifier:TagCellIdentifier];
   
    cell.textField.tag = row;
    
    
    Car *car = (self.tagsArray)[row];
    cell.textField.text = car.name;
//    if (car.isDefault) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    }
    
    return cell;
}


#pragma mark - Editing rows

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The Add Tag row gets an insertion marker, the others a delete marker.
    if (indexPath.row == [self.tagsArray count]) {
        return UITableViewCellEditingStyleInsert;
    }
    return UITableViewCellEditingStyleDelete;
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    // Don't show the Back button while editing.
    [self.navigationItem setHidesBackButton:editing animated:YES];
    
    
    [self.tableView beginUpdates];
    
    NSUInteger count = [self.tagsArray count];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:count inSection:0];
    
    // Add or remove the Add row as appropriate.
    UITableViewRowAnimation animationStyle = UITableViewRowAnimationNone;
    if (animated) {
        animationStyle = UITableViewRowAnimationAutomatic;
    }
    
    if (editing) {
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:animationStyle];
    }
    else {
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:animationStyle];
    }
    
    [self.tableView endUpdates];
    
    if (!editing) {
        [Car saveAllInBackground:self.tagsArray block:^(BOOL success, NSError *error){
            if (!error) {
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Saved", @"Saved")];
            } else {
                [ParseErrorHandlingController handleParseError:error];
                NSLog(@"error :%@",[error localizedDescription]);
            }
        }];
    }
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        APLEditableTableViewCell *cell = (APLEditableTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        [cell.textField resignFirstResponder];
        // Find the tag to delete.
        Car *car = (self.tagsArray)[indexPath.row];
        
        [self.tagsArray removeObject:car];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [car deleteInBackground];
        
    }
    
    if (editingStyle == UITableViewCellEditingStyleInsert) {
        [self insertTagAnimated:YES];
    }
}


- (void)insertTagAnimated:(BOOL)animated
{
    Car *car = [Car object];
    car.name = @"";
    car.driver = [Driver currentUser];
    [self.tagsArray addObject:car];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.tagsArray count]-1 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    // Start editing the tag's name.
    APLEditableTableViewCell *cell = (APLEditableTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    [cell.textField becomeFirstResponder];
}



- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Disable selection of the Add row.
    if (indexPath.row == [self.tagsArray count]) {
        return nil;
    }
    return indexPath;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Editing text fields

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    // Update the tag's name with the text in the text field.
    Car *car = (self.tagsArray)[textField.tag];
    car.name = textField.text;
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


@end
