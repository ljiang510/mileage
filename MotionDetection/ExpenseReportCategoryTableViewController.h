//
//  ExpenseReportCategoryTableViewController.h
//  Mileage
//
//  Created by Liangjun Jiang on 6/30/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpenseReportCategoryTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *expenses;
@end
