//
//  RecordInfoTableViewCell.h
//  MotionDetection
//
//  Created by Liangjun Jiang on 5/23/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ParseUI/PFTableViewCell.h>
@class TripSummary;
@interface RecordInfoTableViewCell : PFTableViewCell
@property (nonatomic, weak) IBOutlet UIButton *trashButton;
@property (nonatomic,strong) TripSummary *summary;
@end
