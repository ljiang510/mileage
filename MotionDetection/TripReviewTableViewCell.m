//
//  TripReviewTableViewCell.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/10/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//


#import "TripReviewTableViewCell.h"
#import "TripSummary.h"
#import "Car.h"
#import "Project.h"
#import "AAPLStyleUtilities.h"

@interface TripReviewTableViewCell()
@property (nonatomic, weak) IBOutlet UILabel *startLabel;
@property (nonatomic, weak) IBOutlet UILabel *endLabel;
@property (nonatomic, weak) IBOutlet UILabel *carProjectLabel;
@property (nonatomic, weak) IBOutlet UILabel *distanceLabel;


@end

@implementation TripReviewTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.startLabel.font = [AAPLStyleUtilities smallFont];
    self.endLabel.font = [AAPLStyleUtilities smallFont];
    self.carProjectLabel.font = [AAPLStyleUtilities smallFont];
    self.distanceLabel.font = [AAPLStyleUtilities smallFont];
    self.startLabel.textColor = [AAPLStyleUtilities foregroundColor];
    self.distanceLabel.textColor = [AAPLStyleUtilities foregroundColor];
    self.endLabel.textColor = [AAPLStyleUtilities foregroundColor];
    self.carProjectLabel.textColor = [AAPLStyleUtilities foregroundColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setItem:(TripSummary *)tSummary{
    if (tSummary) {
        self.startLabel.text = [tSummary friendlyStartInfo];
        self.endLabel.text = [tSummary friendlyEndInfo];
        self.distanceLabel.text = [tSummary friendlyDistance];
        self.carProjectLabel.text = [tSummary friendlyCarProject];
        [self.tripTypeButton setTitle:[tSummary friendlyTripType] forState:UIControlStateNormal];
        [self.tripTypeButton.titleLabel setFont:[AAPLStyleUtilities smallFont]];
        if (tSummary.tripType == TRIP_BUSINESS) {
            [self.tripTypeButton setBackgroundColor:[AAPLStyleUtilities foregroundColor]];
            [self.tripTypeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        } else {
            [self.tripTypeButton setBackgroundColor:[UIColor lightGrayColor]];
            [self.tripTypeButton setTitleColor:[AAPLStyleUtilities foregroundColor] forState:UIControlStateNormal];
        }
    }
}
@end
