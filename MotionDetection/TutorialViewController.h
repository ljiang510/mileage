//
//  ViewController.h
//  PageViewDemo
//
//  Created by Simon on 24/11/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"
@class TutorialViewController;
typedef void (^TuorialViewControllerShouldFinish) (TutorialViewController *viewController);

@interface TutorialViewController : UIViewController <UIPageViewControllerDataSource>
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (nonatomic, copy) TuorialViewControllerShouldFinish shoulFinishBlock;

@end
