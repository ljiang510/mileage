//
//  Expense.h
//  Mileage
//
//  Created by Liangjun Jiang on 5/31/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <Parse/Parse.h>
@class Driver;
@class Tag;
@class Project;
@interface Expense : PFObject<PFSubclassing>
@property (nonatomic, strong) NSDate *when;
@property (nonatomic, copy) NSString *where;
@property (nonatomic, copy) NSString *purpose;
@property (nonatomic, copy) NSString *note;
@property (nonatomic, copy) NSString *businessName;
@property (nonatomic, assign) double expense;
@property (nonatomic, assign) double printTotal;
@property (nonatomic, assign) double tips;
@property (nonatomic, assign) double tax;
@property (nonatomic, assign) double total;
@property (nonatomic, assign) BOOL isReimbursable;

@property (nonatomic, strong) Driver *user;
@property (nonatomic, strong) Tag *tag;
@property (nonatomic, strong) Project *project;
@property (nonatomic, strong) PFFile *thumbnail;
@property (nonatomic, strong) PFFile *fullsizeImage;



+(PFQuery *)basicQuery;
+(PFQuery *)queryExpenseForWeek;
+(PFQuery *)queryForThisMonth;

+(PFQuery *)queryForThisYear;
+(PFQuery *)queryForLastMonth;

-(NSString *)friendlyExpense;
-(NSString *)friendlyTip;
-(NSString *)friendlyPrintTotal;
-(NSString *)friendlyWhen;
-(NSString *)friendlyTax;
-(NSString *)friendlyTotal;
-(NSString *)friendlyProject;
-(NSString *)reportString;

-(void)initWithDemoData:(NSDictionary *)demo;

@end
