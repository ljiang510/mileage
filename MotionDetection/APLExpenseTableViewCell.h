


@class Expense;
@interface APLExpenseTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIViewController *delegate;

- (void)configureWithEvent:(Expense *)event tag:(NSInteger)rowNum;
- (BOOL)makeNameFieldFirstResponder;

@end

