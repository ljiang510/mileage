//
//  ProjectListTableViewController.m
//  Mileage
//
//  Created by Liangjun Jiang on 6/18/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import "ProjectListTableViewController.h"
#import "Project.h"
#import "AAPLStyleUtilities.h"
@interface ProjectListTableViewController ()
@property (nonatomic, strong) NSArray *projects;
@property (nonatomic, strong) NSIndexPath *oldPath;
@end

@implementation ProjectListTableViewController

-(void)setProject:(Project *)project
{
    _project = project;
    
}


-(void)loadProjects:(id)sender
{
    __weak typeof(self)weakSelf = self;
    PFQuery *projectQuery = [Project basicQuery];
    [projectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if(!error) {
            weakSelf.projects = objects;
            if ([objects count] > 0) {
                [self.tableView reloadData];
            }
            
        }
    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Select a project", @"");
    
    [self loadProjects:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onCancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onDone:(id)sender{
    self.finishBlock(self,self.project);
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.projects count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProjectCell" forIndexPath:indexPath];
    
    Project *aProject = self.projects[indexPath.row];
    cell.textLabel.text = aProject.name;
    
    if ([aProject.objectId isEqualToString:self.project.objectId]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.oldPath = indexPath;
    } else if (aProject.isDefault) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    cell.textLabel.textColor = [AAPLStyleUtilities foregroundColor];
    cell.textLabel.font = [AAPLStyleUtilities standardFont];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:self.oldPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    UITableViewCell *nCell = [tableView cellForRowAtIndexPath:indexPath];
    nCell.accessoryType = UITableViewCellAccessoryCheckmark;
    self.project = self.projects[indexPath.row];
    self.oldPath = indexPath;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
