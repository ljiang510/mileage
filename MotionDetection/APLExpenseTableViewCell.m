

#import "APLExpenseTableViewCell.h"
#import "Expense.h"
#import "Tag.h"

@interface APLExpenseTableViewCell ()

@property (nonatomic, weak, readwrite) IBOutlet UITextField *nameField;
@property (nonatomic, weak, readwrite) IBOutlet UITextField *totalField;
@property (nonatomic, weak) IBOutlet UILabel *creationDateLabel;
@property (nonatomic, weak) IBOutlet UILabel *locationLabel;
@property (nonatomic, weak) IBOutlet UITextField *tagsField;
@property (nonatomic, weak) IBOutlet UIButton *tagsButton;

@end


@implementation APLExpenseTableViewCell


- (IBAction)editTags:(id)sender
{
    [self.delegate performSegueWithIdentifier:@"EditTags" sender:self];
}


- (void)configureWithEvent:(Expense *)expense tag:(NSInteger)rowNum
{
    self.nameField.tag = rowNum*100;
	self.nameField.text = expense.businessName;
    self.creationDateLabel.text = [self.dateFormatter stringFromDate:expense.when];
    self.tagsField.text = expense.tag.name;
    self.locationLabel.text = expense.where;
    self.totalField.text = [expense friendlyTotal];
    self.totalField.tag = rowNum*100 +1;
}


- (BOOL)makeNameFieldFirstResponder
{
	return [self.nameField becomeFirstResponder];
}

- (void)willTransitionToState:(UITableViewCellStateMask)state
{
	[super willTransitionToState:state];
	
	if (state & UITableViewCellStateEditingMask) {
		self.locationLabel.hidden = YES;
		self.nameField.enabled = YES;
		self.tagsButton.hidden = NO;
		self.tagsField.placeholder = NSLocalizedString(@"Tap to edit tags", @"Text for tags field in main table view cell");
	}
}


- (void)didTransitionToState:(UITableViewCellStateMask)state
{
	[super didTransitionToState:state];
	
	if (!(state & UITableViewCellStateEditingMask)) {
		self.locationLabel.hidden = NO;
		self.nameField.enabled = NO;
		self.tagsButton.hidden = YES;
		self.tagsField.placeholder = @"";
	}
}

#pragma mark - Internationalization

/*
 Reset the date and number formatters if the locale changes.
 */

+(void)initialize
{
    [[NSNotificationCenter defaultCenter] addObserverForName:NSCurrentLocaleDidChangeNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        
        sDateFormatter = nil;
        sNumberFormatter = nil;
    }];
}


// A date formatter for the creation date.
- (NSDateFormatter *)dateFormatter
{
    return [[self class] dateFormatter];
}


static NSDateFormatter *sDateFormatter = nil;

+ (NSDateFormatter *)dateFormatter
{
	if (sDateFormatter == nil) {
		sDateFormatter = [[NSDateFormatter alloc] init];
		[sDateFormatter setTimeStyle:NSDateFormatterShortStyle];
		[sDateFormatter setDateStyle:NSDateFormatterMediumStyle];
	}
    return sDateFormatter;
}

// A number formatter for the latitude and longitude.
- (NSNumberFormatter *)numberFormatter
{
    return [[self class] numberFormatter];
}


static NSNumberFormatter *sNumberFormatter = nil;

+ (NSNumberFormatter *)numberFormatter
{
	if (sNumberFormatter == nil) {
		sNumberFormatter = [[NSNumberFormatter alloc] init];
		[sNumberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
		[sNumberFormatter setMaximumFractionDigits:3];
	}
    return sNumberFormatter;
}

#pragma mark - Layout

- (void)awakeFromNib
{
    NSArray *constraints = [self.constraints copy];
    
    for (NSLayoutConstraint *constraint in constraints) {
        
        id firstItem = constraint.firstItem;
        if (firstItem == self) {
            firstItem = self.contentView;
        }
        id secondItem = constraint.secondItem;
        if (secondItem == self) {
            secondItem = self.contentView;
        }
        
        NSLayoutConstraint *fixedConstraint = [NSLayoutConstraint constraintWithItem:firstItem attribute:constraint.firstAttribute relatedBy:constraint.relation toItem:secondItem attribute:constraint.secondAttribute multiplier:constraint.multiplier constant:constraint.constant];
        
        [self removeConstraint:constraint];
        [self.contentView addConstraint:fixedConstraint];
    }
}


@end
