//
//  MonthlyTableViewController.h
//  Mileage
//
//  Created by Liangjun Jiang on 7/2/15.
//  Copyright (c) 2015 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonthlyTableViewController : UITableViewController
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, assign) BOOL isForMileage;

@end
